package com.example.kolkatametroapp;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.kolkatametroapp.viewpager.find_route_viewpager;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.nativead.NativeAd;

public class homeMainFragment extends Fragment {
    public static final String MyPREFERENCES = "LangaugePref";
    CardView find_route_card, route_map_card, vastral_gam_card, request_status_card, about_us_card, find_nearest_station, kolkata_local, kolkata_bus, express_trains_card;
    SharedPreferences preferences;
    View view;
    CardView native_ad_small;
    FrameLayout google_ads;

    private void showNativeAd(final View v) {
        MobileAds.initialize(getActivity());
        AdLoader adLoader = new AdLoader.Builder(getActivity(), getResources().getString(R.string.native_ad_unit1))
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        google_ads.setVisibility(View.VISIBLE);
                        NativeTemplateStyle styles = new
                                NativeTemplateStyle.Builder().build();
                        TemplateView template = v.findViewById(R.id.my_template);
                        template.setStyles(styles);
                        template.setNativeAd(nativeAd);
                    }
                })
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_main_layout, container, false);

        view = getActivity().getLayoutInflater().inflate(R.layout.change_lang_dialog_layout, null);

        AdView bannerView = v.findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        showNativeAd(v);

        google_ads = v.findViewById(R.id.google_native_ads);
        find_route_card = v.findViewById(R.id.find_route_card);
        route_map_card = v.findViewById(R.id.route_map_card);
        vastral_gam_card = v.findViewById(R.id.vastral_gam_card);
        request_status_card = v.findViewById(R.id.request_feature_card);
        find_nearest_station = v.findViewById(R.id.find_nearest_station);
        kolkata_local = v.findViewById(R.id.kolkata_local);
        kolkata_bus = v.findViewById(R.id.kolkata_bus);
        express_trains_card = v.findViewById(R.id.express_trains_card);

        about_us_card = v.findViewById(R.id.about_us_card);

        preferences = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        find_route_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity().getApplicationContext(), find_route_viewpager.class);
                startActivity(i);
            }
        });

        route_map_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity().getApplicationContext(), routeMapWebview.class);
                startActivity(i);
            }
        });

        vastral_gam_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity().getApplicationContext(), stationList.class);
                startActivity(i);
            }
        });

        kolkata_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), kolkata_local_find_route_class.class);
                startActivity(i);
            }
        });

        kolkata_bus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), kolkata_bus_find_route_class.class);
                startActivity(i);
            }
        });

        express_trains_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), express_trains.class);
                startActivity(i);
            }
        });

        request_status_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = "Request Feature";
                String bodyText = "";
                String mailto = "mailto:appspundit2014@gmail.com" +
                        "?cc=" + "" +
                        "&subject=" + Uri.encode(subject) +
                        "&body=" + Uri.encode(bodyText);
                Intent EmailIntent = new Intent(Intent.ACTION_SENDTO);
                Uri data = Uri.parse(mailto);
                EmailIntent.setData(data);
                try {
                    startActivity(EmailIntent);
                } catch (ActivityNotFoundException e) {
                    //TODO: Handle case where no email app is available
                    Toast.makeText(getActivity(), "No app available to send email.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        find_nearest_station.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity().getApplicationContext(), findNearestStation.class);
                startActivity(i);
            }
        });

        about_us_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent PlayStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/dev?id=4744559207594823968"));
                startActivity(PlayStoreIntent);
            }
        });

        return v;
    }

    public void showDialog(View vi) {
        Dialog dialog = new Dialog(getActivity());
        ViewParent viewParent = vi.getParent();
        if (viewParent != null && viewParent instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) viewParent;
            viewGroup.removeView(vi);
        }
        dialog.setContentView(vi);
        dialog.setCancelable(true);

        RadioGroup selectLangaugeGroup;
        Button save;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        selectLangaugeGroup = vi.findViewById(R.id.RadioSelectLangauge);
        save = vi.findViewById(R.id.save_lang);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedID = selectLangaugeGroup.getCheckedRadioButtonId();
                System.out.println("seleid: " + selectedID);

                if (selectedID == R.id.english) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("langauge", "en");
                    editor.commit();
                    getActivity().finish();
                    Intent intent = new Intent(getActivity().getApplicationContext(), MainDashboard.class);
                    startActivity(intent);
                }
                if (selectedID == R.id.kannada) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("langauge", "bn");
                    editor.commit();
                    getActivity().finish();
                    Intent intent = new Intent(getActivity().getApplicationContext(), MainDashboard.class);
                    startActivity(intent);
                }

                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setLayout((6 * width) / 7, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.dialog_bg));
    }
}