package com.example.kolkatametroapp;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.searchedListBusRecyclerAdapterClass;
import com.example.kolkatametroapp.database.DataAdapterBus;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Locale;

public class searchedKolkataBusList extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    public static final int ITEMS_PER_AD = 3;
    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView src_display_txtview, dst_display_txtview;
    ImageView img_back;
    String src_from_main, dst_from_main, src_id_from_main, dst_id_from_main, src_code_from_main, dst_code_from_main;
    SharedPreferences preferences;
    int src_id, dst_id;
    searchedListBusRecyclerAdapterClass Adapter;
    String lang;
    DataAdapterBus dbAdapter;
    LinearLayout no_trains_available;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }

        setContentView(R.layout.searched_kolkata_bus_layout);

        toolbar = findViewById(R.id.toolbar_searched_list_bus);
        setSupportActionBar(toolbar);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        src_from_main = getIntent().getStringExtra("src");
        src_id_from_main = getIntent().getStringExtra("srcid");
        src_code_from_main = getIntent().getStringExtra("src_code");
        dst_from_main = getIntent().getStringExtra("dst");
        dst_id_from_main = getIntent().getStringExtra("dstid");
        dst_code_from_main = getIntent().getStringExtra("dst_code");

        src_display_txtview = findViewById(R.id.searched_src_display_searched_list_bus);
        dst_display_txtview = findViewById(R.id.searched_dst_display_searched_list_bus);
        img_back = findViewById(R.id.img_back_searched_list_bus);
        recyclerView = findViewById(R.id.recycler_view_searched_list_bus);
        no_trains_available = findViewById(R.id.no_trains_available_layout_searched_list_bus);

        src_display_txtview.setText(src_from_main);
        dst_display_txtview.setText(dst_from_main);


        src_id = Integer.valueOf(src_id_from_main);
        dst_id = Integer.valueOf(dst_id_from_main);

        System.out.println("src " + src_from_main);
        System.out.println("src code " + src_code_from_main);
        System.out.println("dst code " + dst_code_from_main);
        System.out.println("srcid " + src_id_from_main);
        System.out.println("dst " + dst_from_main);
        System.out.println("dstid " + dst_id_from_main);

        dbAdapter = new DataAdapterBus(searchedKolkataBusList.this);
        dbAdapter.createDatabase();
        dbAdapter.open();

        ArrayList<ArrayList<String>> list = dbAdapter.GetDataBusList(src_id, dst_id);
//        System.out.println(" actual size is :::::::::::: " + list.size());

        dbAdapter.close();

        for (int i = ITEMS_PER_AD; i <= list.size(); i += ITEMS_PER_AD) {

            ArrayList<String> twelve_hr_frmt_time_array_for_ads = new ArrayList<>();
            twelve_hr_frmt_time_array_for_ads.add(null);
            twelve_hr_frmt_time_array_for_ads.add(null);
            twelve_hr_frmt_time_array_for_ads.add(null);

            list.add(i, twelve_hr_frmt_time_array_for_ads);
        }

        System.out.println(" actual size after ads is :::::::::::: " + list.size());


        if (list.size() == 0) {
            no_trains_available.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            no_trains_available.setVisibility(View.GONE);
            Adapter = new searchedListBusRecyclerAdapterClass(list, lang, src_from_main, dst_from_main, src_id, dst_id, searchedKolkataBusList.this);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(searchedKolkataBusList.this);
            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setAdapter(Adapter);
        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}