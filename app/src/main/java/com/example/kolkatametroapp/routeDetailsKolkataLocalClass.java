package com.example.kolkatametroapp;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.kolkatametroapp.adapters.routeDetailsLocalAdapterClass;
import com.example.kolkatametroapp.database.DataAdapterTrains;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class routeDetailsKolkataLocalClass extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    SharedPreferences preferences;
    String lang;
    RecyclerView recyclerView;
    //    Button report_time_table_issue;
    Toolbar toolbar;
    routeDetailsLocalAdapterClass adapter;
    int trip_id_int, cal_id_int;
    String time_from, time_to;
    int rt_id_src, rt_id_dst;
    ArrayList<ArrayList<String>> final_route_details_local_array;
    DataAdapterTrains dbAdapter;
    String short_name, long_name, trip_id, cal_id, source, destination;
    TextView long_name_txt, status;

    LinearLayout liveStatusView;

    Handler handler = new Handler();
    Runnable runnable;
    int delay = 15000;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onResume() {
        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                handler.postDelayed(runnable, delay);
                getLiveStatus(false);
            }
        }, delay);
        super.onResume();

        System.gc();
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            handler.removeCallbacks(runnable);
        } catch (Exception ex) {
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        lang = preferences.getString("langauge", "");

        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }

        setContentView(R.layout.route_details_kolkata_local_layout);

        toolbar = findViewById(R.id.toolbar_route_details_local);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        AdRequest adRequest1 = new AdRequest.Builder().build();

        InterstitialAd.load(routeDetailsKolkataLocalClass.this, getResources().getString(R.string.inter), adRequest1,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        mInterstitialAd = null;
                    }
                });

        short_name = getIntent().getStringExtra("short_name");
        long_name = getIntent().getStringExtra("long_name");
        trip_id = getIntent().getStringExtra("trip_id");
        cal_id = getIntent().getStringExtra("cal_id");
        source = getIntent().getStringExtra("src");
        destination = getIntent().getStringExtra("dst");

        getSupportActionBar().setTitle("Train no. " + short_name);

        liveStatusView = findViewById(R.id.liveStatusView);
        liveStatusView.setVisibility(View.GONE);

//        System.out.println("short " + short_name);
//        System.out.println("long " + long_name);
//        System.out.println("trip_id " + trip_id);
//        System.out.println("cal_id " + cal_id);

        recyclerView = findViewById(R.id.route_stepper_recycler_view_route_details_local);
//        report_time_table_issue = findViewById(R.id.report_time_table_issue_route_route_details_local);
        long_name_txt = findViewById(R.id.long_name_route_details_local);
        status = findViewById(R.id.status);
        status.setVisibility(View.GONE);

        long_name_txt.setText(long_name);

        trip_id_int = Integer.valueOf(trip_id);
        cal_id_int = Integer.valueOf(cal_id);

        final_route_details_local_array = new ArrayList<>();

        dbAdapter = new DataAdapterTrains(routeDetailsKolkataLocalClass.this);
        dbAdapter.createDatabase();
        dbAdapter.open();

        ArrayList<ArrayList<String>> routeStationListArray = dbAdapter.GetRouteDetailsKolkataLocal(trip_id_int);

//        System.out.println("route array size : " + routeStationListArray.size());

        dbAdapter.close();

        for (int j = 0; j < routeStationListArray.size(); j++) {
            try {
                SimpleDateFormat sdf_frm = new SimpleDateFormat("HH:mm");
                Date dateObj_frm = sdf_frm.parse(routeStationListArray.get(j).get(0));
                time_from = new SimpleDateFormat("hh:mm a").format(dateObj_frm);

                SimpleDateFormat sdf_to = new SimpleDateFormat("HH:mm");
                Date dateObj_to = sdf_to.parse(routeStationListArray.get(j).get(1));
                time_to = new SimpleDateFormat("hh:mm a").format(dateObj_to);

                ArrayList<String> twelve_hr_frmt_time_array = new ArrayList<>();
                twelve_hr_frmt_time_array.add(time_from);
                twelve_hr_frmt_time_array.add(time_to);
                twelve_hr_frmt_time_array.add(routeStationListArray.get(j).get(2));
                twelve_hr_frmt_time_array.add(routeStationListArray.get(j).get(3));
                twelve_hr_frmt_time_array.add(routeStationListArray.get(j).get(4));
                twelve_hr_frmt_time_array.add(routeStationListArray.get(j).get(5));
                twelve_hr_frmt_time_array.add(routeStationListArray.get(j).get(6));

                final_route_details_local_array.add(twelve_hr_frmt_time_array);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < final_route_details_local_array.size(); i++) {
            if (final_route_details_local_array.get(i).get(3).equals(source)) {
                rt_id_src = Integer.valueOf(final_route_details_local_array.get(i).get(6));
            }
            if (final_route_details_local_array.get(i).get(3).equals(destination)) {
                rt_id_dst = Integer.valueOf(final_route_details_local_array.get(i).get(6));
            }
        }

//        System.out.println("route array updated size : " + final_route_details_local_array.toString());

        adapter = new routeDetailsLocalAdapterClass(
                final_route_details_local_array,
                lang,
                source,
                destination,
                rt_id_src,
                rt_id_dst,
                routeDetailsKolkataLocalClass.this, "");
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(routeDetailsKolkataLocalClass.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        getLiveStatus(true);

//        report_time_table_issue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String subject = "Report Timetable Issue";
//                String bodyText = " Train no " + short_name + " and Train route is " + long_name;
//                String mailto = "mailto:appspundit2014@gmail.com" +
//                        "?cc=" + "" +
//                        "&subject=" + Uri.encode(subject) +
//                        "&body=" + Uri.encode(bodyText);
//                Intent EmailIntent = new Intent(Intent.ACTION_SENDTO);
//                Uri data = Uri.parse(mailto);
//                EmailIntent.setData(data);
//                try {
//                    startActivity(EmailIntent);
//                } catch (ActivityNotFoundException e) {
//                    //TODO: Handle case where no email app is available
//                }
//            }
//        });
    }

    private void getLiveStatus(boolean firstTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar c = Calendar.getInstance();
        String today = sdf.format(c.getTime());
        String URL = "https://api.confirmtkt.com/api/trains/livestatusall" + "?trainno=" + short_name.trim() + "&doj=" + today;
        AndroidNetworking.get(URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        System.out.println("Resp:> " + response.toString());
//                        System.out.println("Resp:> fetched");

                        if (response != null) {
                            if (response.has("curStn")) {
                                try {
                                    int curStnIndex = -1;
                                    String curStn = response.getString("curStn");
System.out.println("XXXXXXXXXXXXXXXXXXXX");

                                    adapter.setCurrentStation(curStn);

                                    for (int i = 0; i < final_route_details_local_array.size(); i++) {
                                        boolean found = false;

                                        if (final_route_details_local_array.get(i).get(2).trim().equalsIgnoreCase(curStn)) {
                                            curStnIndex = i;
                                            found = true;
                                        }

                                        if (found) {
                                            break;
                                        }
                                    }

                                    int finalCurStnIndex = curStnIndex;
                                    System.out.println(curStn);
                                    System.out.println(finalCurStnIndex);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if (finalCurStnIndex >= 0 && firstTime) {
                                                    recyclerView.smoothScrollToPosition(finalCurStnIndex);
                                                }
                                            } catch (Exception ex) {
                                            }
                                        }
                                    }, 1000);
                                } catch (JSONException e) {
                                } catch (Exception e) {
                                }
                            }

                            if (response.has("curStnName")) {
                                try {
                                    String curStnName = "Last location " + response.getString("curStnName");

                                    if (response.has("lastUpdated")) {
                                        String LastUpdated = response.getString("lastUpdated");
                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault());

                                        Date date1 = simpleDateFormat.parse(LastUpdated);
                                        Date date2 = simpleDateFormat.parse(simpleDateFormat.format(Calendar.getInstance().getTime()));

                                        String diff = printDifference(date1, date2);

                                        if (!diff.trim().isEmpty()) {
                                            LastUpdated = ", updated " + diff + " ago";
                                        } else {
                                            LastUpdated = ", updated just now";
                                        }

                                        curStnName = curStnName + LastUpdated;
                                    }

                                    status.setText(curStnName);
                                    status.setVisibility(View.VISIBLE);
                                    liveStatusView.setVisibility(View.VISIBLE);
                                } catch (JSONException e) {
                                    status.setVisibility(View.GONE);
                                    liveStatusView.setVisibility(View.GONE);
                                } catch (Exception e) {
                                    status.setVisibility(View.GONE);
                                    liveStatusView.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            status.setVisibility(View.GONE);
                            liveStatusView.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        status.setVisibility(View.GONE);
                        liveStatusView.setVisibility(View.GONE);
                    }
                });
    }

    private String printDifference(Date startDate, Date endDate) {
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        ArrayList<String> text = new ArrayList<String>();

        if (elapsedDays > 0) {
            text.add(elapsedDays > 1 ? elapsedDays + " days" : elapsedDays + " day");
        }
        if (elapsedHours > 0) {
            text.add(elapsedHours > 1 ? elapsedHours + " hours" : elapsedHours + " hour");
        }
        if (elapsedMinutes > 0) {
            text.add(elapsedMinutes > 1 ? elapsedMinutes + " mins" : elapsedMinutes + " min");
        }
        if (elapsedSeconds > 0) {
            text.add(elapsedSeconds > 1 ? elapsedSeconds + " secs" : elapsedSeconds + " sec");
        }

        return TextUtils.join(", ", text);
//        return String.join(", ", text);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }


    @Override
    public void onBackPressed() {
        if (mInterstitialAd != null) {
            mInterstitialAd.show(routeDetailsKolkataLocalClass.this);
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(routeDetailsKolkataLocalClass.this);
                }
                this.finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}