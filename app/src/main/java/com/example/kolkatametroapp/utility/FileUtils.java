package com.example.kolkatametroapp.utility;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {
    public static File getDataDir(Context context) {
        String path = context.getFilesDir().getAbsolutePath();
//        String path = context.getApplicationInfo().dataDir + "/databases/";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }

        return file;
    }

    public static File getDataDir(Context context, String folder) {
        String path = context.getFilesDir().getAbsolutePath() + "/" + folder;
//        System.out.println("path data:::::" + path);

        File file = new File(path);

        if (!file.exists()) {
            file.mkdirs();
        }

        return file;
    }

    public static void checkIfFileExistsInDBFolder(Context context) {
        String path = context.getFilesDir().getAbsolutePath() + "/kolkataMetroDB.db";
        System.out.println("DATA====> checkIfFileExistsInDBFolder: " + path);
        File file = new File(path);
        if (file.exists()) {
//            System.out.println("DATA====> checkIfFileExistsInDBFolder: FILE EXISTS");
            moveFile(context.getFilesDir().getAbsolutePath(), "/kolkataMetroDB.db", context.getApplicationInfo().dataDir + "/databases/");
        }
    }

    private static void moveFile(String inputPath, String inputFile, String outputPath) {
        InputStream in = null;
        OutputStream out = null;

        try {
            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            new File(inputPath + inputFile).delete();
        } catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    public static boolean downloadZipFile(String urlStr, String destinationFilePath) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlStr);

            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.d("x-x: downloadZipFile", "Server ResponseCode=" + connection.getResponseCode() + " ResponseMessage=" + connection.getResponseMessage());
            }

            // download the file
            input = connection.getInputStream();

            Log.d("x-x: downloadZipFile", "destinationFilePath=" + destinationFilePath);
            new File(destinationFilePath).createNewFile();
            output = new FileOutputStream(destinationFilePath);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
            File f = new File(destinationFilePath);

            Log.d("x-x: downloadZipFile", "f.getParentFile().getPath()=" + f.getParentFile().getPath());
            Log.d("x-x: downloadZipFile", "f.getName()=" + f.getName().replace(".zip", ""));

            return true;
        } catch (Exception e) {
            System.out.println("x-x: error download zip::::" + e.toString());
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (output != null) output.close();
                if (input != null) input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (connection != null) connection.disconnect();
        }
    }

    public static boolean unpackZip(String filePath) {
        InputStream is;
        ZipInputStream zis;

        try {
            File zipfile = new File(filePath);
            String parentFolder = zipfile.getParentFile().getPath();
            String filename;

            is = new FileInputStream(filePath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();

                if (ze.isDirectory()) {
                    File fmd = new File(parentFolder + "/" + filename);

                    try {
                        FileUtils.ensureZipPathSafety(fmd, parentFolder + "/" + filename);
                    } catch (Exception e) {
                        // SecurityException
                        Log.d("x-x: extract zip file", "SecurityException while unziping: " + e);
                        break;
                    }

                    // --- OLD SecurityException checking Code --- //
//                    String canonicalPath = fmd.getCanonicalPath();
//                    if (!canonicalPath.startsWith(parentFolder + "/" + filename)) {
//                        // SecurityException
//                        Log.d("x-x: extract zip file", "SecurityException while unziping");
//                        break;
//                    }
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(parentFolder + "/" + filename);

                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
            zipfile.delete();
            Log.d("x-x: extract zip file", "completed extracted file");
            return true;
        } catch (Exception e) {
            System.out.println("x-x: extract zip file err ::: " + e.toString());
            return false;
        }
    }

    private static void ensureZipPathSafety(final File outputFile, final String destDirectory) throws Exception {
        String destDirCanonicalPath = (new File(destDirectory)).getCanonicalPath();
        String outputFileCanonicalPath = outputFile.getCanonicalPath();
        if (!outputFileCanonicalPath.startsWith(destDirCanonicalPath)) {
            throw new Exception(String.format("Found Zip Path Traversal Vulnerability"));
        }
    }

    public static String readFile(File f) {
        if (f.exists()) {
            System.out.println("file successfully uploaded");
            FileReader fileReader = null;
            try {
                fileReader = new FileReader(f);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                StringBuilder stringBuilder = new StringBuilder();
                String line = bufferedReader.readLine();
                while (line != null) {
                    stringBuilder.append(line).append("\n");
                    line = bufferedReader.readLine();
                }
                bufferedReader.close();
                String responce = stringBuilder.toString();

                return responce;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public static String readJSONFromAsset(Context ctx, String fileName) {
        String json = null;
        try {
            InputStream is = ctx.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}