package com.example.kolkatametroapp.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpHandler {

    private static final String TAG = HttpHandler.class.getSimpleName();
    private static final MediaType MEDIA_JSON = MediaType.parse("application/json; charset=utf-8");
    private static final MediaType MEDIA_IMAGE = MediaType.parse("image/png");
    private static final int TIMEOUT_IN_SECOND = 15;
    private static OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    private static Headers.Builder headersBuilder = new Headers.Builder();

    public HttpHandler() {

    }

    public static boolean isOnline(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        // if running on emulator return true always.
        return Build.MODEL.equals("google_sdk");
    }

    private static OkHttpClient configureClient() {
        httpClientBuilder.readTimeout(TIMEOUT_IN_SECOND, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(TIMEOUT_IN_SECOND, TimeUnit.SECONDS);
        httpClientBuilder.connectTimeout(TIMEOUT_IN_SECOND, TimeUnit.SECONDS);
        httpClientBuilder.retryOnConnectionFailure(true);
        return httpClientBuilder.build();
    }

    private static Headers getHeaders(@NonNull Context mContext) {
        headersBuilder.set("Accept", "application/json");
        headersBuilder.set("Content-Type", "application/json");
        headersBuilder.set("X-Requested-With", "XMLHttpRequest");
        try {
            headersBuilder.set("Authorization", mContext.getSharedPreferences("com.indian.railways.pnr", Context.MODE_PRIVATE).getString("auth_header", ""));
        } catch (Exception e) {
            headersBuilder.set("Authorization", "bsk58rv6aw219y4qnpmechtxfg30z7dj::4.3.7");
        }
        return headersBuilder.build();
    }

    private static RequestBody getparam(HashMap<String, String> param) {
        FormBody.Builder requestBody = new FormBody.Builder();
        for (String key : param.keySet()) {
            String value = param.get(key);
            requestBody.add(key, value);
        }
        return requestBody.build();
    }

    @NonNull
    public static String post(@NonNull final Context mContext, @NonNull final String endPoint, HashMap<String, String> param) {
        Request request = new Request.Builder().headers(getHeaders(mContext)).url(endPoint).post(getparam(param)).build();
        return execute(mContext, request);
    }

    @NonNull
    public static String get(@NonNull final Context mContext, @NonNull final String endPoint) {
        Request request = new Request.Builder().headers(getHeaders(mContext)).url(endPoint).get().build();
        return execute(mContext, request);
    }

    @NonNull
    public static String upload(@NonNull Context mContext, @NonNull String uri, String filePath, String fileName) {
        String FILE_TYPE = "photo";
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(FILE_TYPE, fileName, RequestBody.create(MEDIA_IMAGE, new File(filePath + fileName)))
                .build();
        Request request = new Request.Builder().headers(getHeaders(mContext)).url(uri).post(requestBody).build();
        return execute(mContext, request);
    }

    @NonNull
    private static String execute(@NonNull final Context mContext, @NonNull final Request request) {
        String status = "";
        try {
            OkHttpClient client = configureClient();
            // Execute the request and retrieve the response.
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                status = response.body().string().trim();
            }
        } catch (@NonNull UnknownHostException | SocketTimeoutException e) {
            e.printStackTrace();
            new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message message) {
                    // Toast.makeText(mContext, "Internet Not Working", Toast.LENGTH_SHORT).show();
                }
            }.sendEmptyMessage(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return status;
    }

    private static String getSHA(String queryString, String millisec) {
        try {

            char[] auth_temp = queryString.concat("5.2.3::33::android").concat(millisec).concat("4_9?jtd@8?pp5]<").toLowerCase().toCharArray();

            //System.out.println("DATA:::::::x:: :auth_temp1: " + "trainNum=".concat(trainNum).concat("5.2.3::33::android").concat(paramArrayMap).concat("4_9?jtd,@8?pp5]<").toLowerCase());
            //System.out.println("DATA:::::::x:: :auth_temp_1_char: " + auth_temp.toString());

            Arrays.sort(auth_temp);

            //System.out.println("DATA:::::::x:: :auth_temp_2_char: " + auth_temp.toString());

            String auth = new String(auth_temp);

            MessageDigest md1 = MessageDigest.getInstance("SHA-256");
            byte[] b;

            if (Build.VERSION.SDK_INT >= 19) {
                md1.update(auth.getBytes(StandardCharsets.UTF_8));
                b = md1.digest();
            } else {
                md1.update(auth.getBytes());
                b = md1.digest();
            }

            System.out.println("DATA:::::::x:: :b: " + b.toString());

            String sha = String.format("%064x", new Object[]{new BigInteger(1, b)});

            return sha;
        } catch (NoSuchAlgorithmException err) {
            System.out.println("XXXXXXXX::::::::::::: :err: " + err);
            return "";
        }
    }

    private static String getUniqueID(String millsec) {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("35");
        localStringBuilder.append(Build.BOARD.length() % 10);
        localStringBuilder.append(Build.BRAND.length() % 10);
        localStringBuilder.append(Build.CPU_ABI.length() % 10);
        localStringBuilder.append(Build.DEVICE.length() % 10);
        localStringBuilder.append(Build.DISPLAY.length() % 10);
        localStringBuilder.append(Build.HOST.length() % 10);
        localStringBuilder.append(Build.ID.length() % 10);
        localStringBuilder.append(Build.MANUFACTURER.length() % 10);
        localStringBuilder.append(Build.MODEL.length() % 10);
        localStringBuilder.append(Build.PRODUCT.length() % 10);
        localStringBuilder.append(Build.TAGS.length() % 10);
        localStringBuilder.append(Build.TYPE.length() % 10);
        localStringBuilder.append(Build.USER.length() % 10);

        StringBuilder paramContext = new StringBuilder();
        paramContext.append(localStringBuilder.toString());
        paramContext.append("::");
        paramContext.append(millsec);
        return paramContext.toString();
    }

    @NonNull
    public static String newget(@NonNull final Context mContext, @NonNull final String endPoint) {
        String millsec = String.valueOf(System.currentTimeMillis());

        headersBuilder.set("Authorization", getSHA(endPoint.split("\\?")[1], millsec));
        headersBuilder.set("Content-Type", "application/json");
        headersBuilder.set("device-id", getUniqueID(millsec));
        headersBuilder.set("Version", "5.2.3::33::android");


        Request request = new Request.Builder().headers(headersBuilder.build()).url(endPoint).get().build();
        return execute(mContext, request);
    }

    public String makeServiceCall(String reqUrl) {
        String response = null;
        try {
            URL url = new URL(reqUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return response;
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}