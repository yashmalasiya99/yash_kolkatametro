package com.example.kolkatametroapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

public class DataAdapterTrains {
    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelperLocal mDbHelper;

    public DataAdapterTrains(Context context) {
        this.mContext = context;
        mDbHelper = new DatabaseHelperLocal(mContext, "kolktalocal_3.db", null, 1);
    }

    public DataAdapterTrains createDatabase() throws SQLException {
        try {
            mDbHelper.createDataBase();
        } catch (IOException mIOException) {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public DataAdapterTrains open() throws SQLException {
        try {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        } catch (SQLException mSQLException) {
            Log.e(TAG, "open >>" + mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public ArrayList<ArrayList<String>> GetAllStationsList(String search) {
        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from stops WHERE name LIKE '%" + search + "%' OR code LIKE '%" + search + "%' ORDER BY name ASC";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(String.valueOf(c.getInt(0)));      // station id
            listdata.add(c.getString(1));                   // domain_id
            listdata.add(c.getString(2));                   // station_code
            listdata.add(c.getString(3));                   // station name
            listdata.add(c.getString(4));                   // lat
            listdata.add(c.getString(5));                   // long
            listdata.add(c.getString(6));                   // sound dex

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> GetDataStationList() {
        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from stops \n" +
                "where code IN (\"HWH\",\"TPKR\",\"DSNR\",\"RMJ\",\"SRC\",\"ADL\",\"SEL\",\"ABB\",\"NALR\",\"BVA\",\"CGA\",\"FLR\",\"ULB\",\"BSBP\",\"KGY\",\"BZN\",\"GGTA\",\"DTE\",\"KIG\",\"MCA\",\"NDGJ\",\"BOP\",\"NPMR\",\n" +
                "            \"PKU\",\"GEDE\",\"HRSR\",\"BPN\",\"MIJ\",\"TNX\",\"MYHT\",\"BGL\",\"BHNA\",\"BHGH\",\"AG\",\"PNCB\",\"BNKA\",\"RHA\",\"PDX\",\"CDH\",\"PXR\",\"SMX\",\"MPJ\",\"KYI\",\"KPA\",\"HLR\",\"NH\",\"SNR\",\"IP\",\n" +
                "            \"PTF\",\"BP\",\"AGP\",\"DDJ\",\"BNXR\",\"SDAH\",\"BLH\",\"SEP\",\"KDH\",\"TGH\",\"JGDL\",\"KNR\",\"NMKA\",\"UKLR\",\"KWDP\",\"KHGR\",\"NCP\",\"NCPM\",\"KFD\",\"KLW\",\"URP\",\"LKPR\",\"MDBP\",\"MPRD\",\"JNM\",\"BARU\",\n" +
                "            \"DBT\",\"HGA\",\"GCN\",\"SJPR\",\"DPDP\",\"KRXM\",\"SSRD\",\"BRP\",\"SPR\",\"BLN\",\"PQS\",\"DHK\",\"JDP\",\"BGJT\",\"GIA\",\"NRPR\",\"SBGR\",\"MAK\",\"KNJI\",\"BNBA\",\"DDC\",\"PTKR\",\"KOAA\",\"TALA\",\n" +
                "            \"BBR\",\"SOLA\",\"BZB\",\"BBDB\",\"EDG\",\"PPGT\",\"KIRP\",\"RMTR\",\"MJT\",\"NACC\",\"LKF\",\"DGNR\",\"BBT\",\"BRPK\",\"NBE\",\"MMG\",\"HHR\",\"BT\",\"BMG\",\"DTK\",\"BIRA\",\"GUMA\",\"ASKR\",\"HB\",\"SNHT\",\"MSL\",\"GBG\",\n" +
                "            \"TKNR\",\"CDP\",\"BNAA\",\"BNJ\",\"BGB\",\"NAI\",\"AKRA\",\"SSP\",\"BRJ\",\"KYP\",\"DKDP\",\"HT\",\"DMU\",\"UTN\",\"MGT\",\"BHPA\",\"SNU\",\"D\",\"NTA\",\"BSD\",\"GURN\",\"DH\",\"BDYP\",\"KLKR\",\"CHT\",\"PLF\",\"GQD\",\n" +
                "            \"GOF\",\"BTPG\",\"TLX\",\"CG\",\"LLH\",\"BEQ\",\"BLY\",\"UPA\",\"HMZ\",\"KOG\",\"RIS\",\"SRP\",\"SHE\",\"DEA\",\"NSF\",\"SIU\",\"NKL\",\"MLYA\",\"HPL\",\"KKAE\",\"BAHW\",\"LOK\",\"TAK\",\"MAYP\",\"AMBG\",\"NGRI\",\n" +
                "            \"GFAE\",\"HYG\",\"BDC\",\"ADST\",\"MUG\",\"TLO\",\"KHN\",\"PDA\",\"SLG\",\"BCGM\",\"BOI\",\"DBP\",\"BGF\",\"MYM\",\"NMF\",\"RSLR\",\"PLAE\",\"SKG\",\"GRP\",\"BWN\",\"BSAE\",\"TBAE\",\"KJU\",\"DMLE\",\"KMAE\",\n" +
                "            \"JIT\",\"BGAE\",\"SOAE\",\"BHLA\",\"GPAE\",\"ABKA\",\"BGRA\",\"DTAE\",\"SMAE\",\"KLNT\",\"NDAE\",\"BFZ\",\"PSAE\",\"MTFA\",\"LKX\",\"BQY\",\"PTAE\",\"AGAE\",\"DHAE\",\"KWAE\",\"SHBA\",\"BZL\",\"DKAE\",\"GBRA\",\n" +
                "            \"JOX\",\"BPAE\",\"BRPA\",\"MBE\",\"BLAE\",\"KQU\",\"MDSE\",\"CDAE\",\"PBZ\",\"BMAE\",\"DNHL\",\"SHBC\",\"HIH\",\"GRAE\",\"JPQ\",\"JRAE\",\"NBAE\",\"MSAE\",\"CHC\",\"PRAE\",\"BBAE\",\"BHR\",\"MUU\",\"CGR\",\"CNS\",\n" +
                "            \"HGY\",\"SGBA\",\"STBB\",\"GN\",\"AKIP\",\"MAJ\",\"GGP\",\"NBRN\",\"CPHT\",\"KLYM\",\"KLYG\",\"KLYS\",\"STB\",\"FLU\",\"HBE\",\"KLNP\",\"BTKB\",\"BLYG\",\"DAKE\",\"BARN\",\"KNJ\",\"JKL\",\"BDZ\",\"THP\",\"BIJ\",\n" +
                "            \"HNB\",\"TKF\",\"NMDR\",\"MPN\",\"MTAP\",\"BSHT\",\"BBLA\",\"CQR\",\"GGV\",\"MPE\",\"KMZA\",\"HRO\",\"BSLA\",\"LBTL\",\"BGRD\",\"SXC\",\"BHKA\",\"KBGH\",\"KZPR\",\"TLG\",\"BRMH\",\"CRAE\",\"MRGM\",\"KGP\",\"JPR\",\n" +
                "            \"MPD\",\"SMCK\",\"BCK\",\"DUAN\",\"RDU\",\"HAUR\",\"KHAI\",\"BIR\",\"KZPB\",\"GMDN\",\"GKL\",\"MDN\",\"RGX\",\"RGA\",\"SMTG\",\"TMZ\",\"KSBP\",\"SSPH\",\"MSDL\",\"BRDB\",\"BYSA\",\"DZK\",\"DZKT\",\"BAAR\",\"HLZ\",\n" +
                "            \"BKNM\",\"KONA\",\"DNI\",\"JLBR\",\"MDC\",\"DMJR\",\"DJR\",\"DKB\",\"BAC\",\"PTHL\",\"MNH\",\"MHLN\",\"MJH\",\"JLI\",\"HDC\",\"AMZ\",\"JSOR\",\"RCD\",\"BLYH\",\"SHM\",\"PDPK\",\"KMRA\",\"KSHT\",\"CMDG\",\"KJRA\",\n" +
                "            \"KJRM\",\"ARNB\",\"BTRH\",\"BGNA\",\"DTG\",\"TLPH\",\"TKPH\",\"BALT\",\"KPAW\",\"VSU\",\"BSMH\",\"GNJP\",\"MYPR\",\"NDKR\",\"LSGS\",\"DSPN\",\"HEN\",\"NCN\",\"KATI\",\"STLB\",\"SJPA\",\"APRD\",\"BOPA\",\"RMRB\",\"TKRA\",\n" +
                "            \"DGHA\",\"TIT\",\"KAN\",\"GLI\",\"PAJ\",\"MNAE\",\"PAN\",\"RBH\",\"DGR\",\"OYR\",\"UDL\",\"RNG\",\"KPK\",\"ASN\",\"BQT\",\"INCI\",\"HIJ\",\"MDNR\",\"VSPR\",\"SNTR\",\"GOGT\",\"GMMG\",\"GMMR\",\"HPUR\",\"GMDP\",\n" +
                "            \"BUGM\",\"MTIP\",\"BOKA\",\"RNGR\",\"SHMR\",\"GOR\",\"SRBZ\",\"KYB\",\"GSQ\",\"BWCN\",\"SHJ\",\"SHKL\",\"INS\",\"KMRL\",\"BTRB\",\"PSF\",\"DGF\",\"DIM\",\"SONA\",\"HAM\",\"SIMR\",\"BDPR\",\"CNRF\",\"BZC\",\n" +
                "            \"BEX\",\"NOB\",\"BKNO\",\"BQA\",\"SOF\",\"NGX\",\"KCY\",\"BCF\",\"SIZ\", \"SPS\") ORDER BY name ASC";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(String.valueOf(c.getInt(0)));      // station id
            listdata.add(c.getString(1));                   // domain_id
            listdata.add(c.getString(2));                   // station_code
            listdata.add(c.getString(3));                   // station name
            listdata.add(c.getString(4));                   // lat
            listdata.add(c.getString(5));                   // long
            listdata.add(c.getString(6));                   // sound dex

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> GetFilterDataList(String filter) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from stops \n" +
                "where code IN (\"HWH\",\"TPKR\",\"DSNR\",\"RMJ\",\"SRC\",\"ADL\",\"SEL\",\"ABB\",\"NALR\",\"BVA\",\"CGA\",\"FLR\",\"ULB\",\"BSBP\",\"KGY\",\"BZN\",\"GGTA\",\"DTE\",\"KIG\",\"MCA\",\"NDGJ\",\"BOP\",\"NPMR\",\n" +
                "            \"PKU\",\"GEDE\",\"HRSR\",\"BPN\",\"MIJ\",\"TNX\",\"MYHT\",\"BGL\",\"BHNA\",\"BHGH\",\"AG\",\"PNCB\",\"BNKA\",\"RHA\",\"PDX\",\"CDH\",\"PXR\",\"SMX\",\"MPJ\",\"KYI\",\"KPA\",\"HLR\",\"NH\",\"SNR\",\"IP\",\n" +
                "            \"PTF\",\"BP\",\"AGP\",\"DDJ\",\"BNXR\",\"SDAH\",\"BLH\",\"SEP\",\"KDH\",\"TGH\",\"JGDL\",\"KNR\",\"NMKA\",\"UKLR\",\"KWDP\",\"KHGR\",\"NCP\",\"NCPM\",\"KFD\",\"KLW\",\"URP\",\"LKPR\",\"MDBP\",\"MPRD\",\"JNM\",\"BARU\",\n" +
                "            \"DBT\",\"HGA\",\"GCN\",\"SJPR\",\"DPDP\",\"KRXM\",\"SSRD\",\"BRP\",\"SPR\",\"BLN\",\"PQS\",\"DHK\",\"JDP\",\"BGJT\",\"GIA\",\"NRPR\",\"SBGR\",\"MAK\",\"KNJI\",\"BNBA\",\"DDC\",\"PTKR\",\"KOAA\",\"TALA\",\n" +
                "            \"BBR\",\"SOLA\",\"BZB\",\"BBDB\",\"EDG\",\"PPGT\",\"KIRP\",\"RMTR\",\"MJT\",\"NACC\",\"LKF\",\"DGNR\",\"BBT\",\"BRPK\",\"NBE\",\"MMG\",\"HHR\",\"BT\",\"BMG\",\"DTK\",\"BIRA\",\"GUMA\",\"ASKR\",\"HB\",\"SNHT\",\"MSL\",\"GBG\",\n" +
                "            \"TKNR\",\"CDP\",\"BNAA\",\"BNJ\",\"BGB\",\"NAI\",\"AKRA\",\"SSP\",\"BRJ\",\"KYP\",\"DKDP\",\"HT\",\"DMU\",\"UTN\",\"MGT\",\"BHPA\",\"SNU\",\"D\",\"NTA\",\"BSD\",\"GURN\",\"DH\",\"BDYP\",\"KLKR\",\"CHT\",\"PLF\",\"GQD\",\n" +
                "            \"GOF\",\"BTPG\",\"TLX\",\"CG\",\"LLH\",\"BEQ\",\"BLY\",\"UPA\",\"HMZ\",\"KOG\",\"RIS\",\"SRP\",\"SHE\",\"DEA\",\"NSF\",\"SIU\",\"NKL\",\"MLYA\",\"HPL\",\"KKAE\",\"BAHW\",\"LOK\",\"TAK\",\"MAYP\",\"AMBG\",\"NGRI\",\n" +
                "            \"GFAE\",\"HYG\",\"BDC\",\"ADST\",\"MUG\",\"TLO\",\"KHN\",\"PDA\",\"SLG\",\"BCGM\",\"BOI\",\"DBP\",\"BGF\",\"MYM\",\"NMF\",\"RSLR\",\"PLAE\",\"SKG\",\"GRP\",\"BWN\",\"BSAE\",\"TBAE\",\"KJU\",\"DMLE\",\"KMAE\",\n" +
                "            \"JIT\",\"BGAE\",\"SOAE\",\"BHLA\",\"GPAE\",\"ABKA\",\"BGRA\",\"DTAE\",\"SMAE\",\"KLNT\",\"NDAE\",\"BFZ\",\"PSAE\",\"MTFA\",\"LKX\",\"BQY\",\"PTAE\",\"AGAE\",\"DHAE\",\"KWAE\",\"SHBA\",\"BZL\",\"DKAE\",\"GBRA\",\n" +
                "            \"JOX\",\"BPAE\",\"BRPA\",\"MBE\",\"BLAE\",\"KQU\",\"MDSE\",\"CDAE\",\"PBZ\",\"BMAE\",\"DNHL\",\"SHBC\",\"HIH\",\"GRAE\",\"JPQ\",\"JRAE\",\"NBAE\",\"MSAE\",\"CHC\",\"PRAE\",\"BBAE\",\"BHR\",\"MUU\",\"CGR\",\"CNS\",\n" +
                "            \"HGY\",\"SGBA\",\"STBB\",\"GN\",\"AKIP\",\"MAJ\",\"GGP\",\"NBRN\",\"CPHT\",\"KLYM\",\"KLYG\",\"KLYS\",\"STB\",\"FLU\",\"HBE\",\"KLNP\",\"BTKB\",\"BLYG\",\"DAKE\",\"BARN\",\"KNJ\",\"JKL\",\"BDZ\",\"THP\",\"BIJ\",\n" +
                "            \"HNB\",\"TKF\",\"NMDR\",\"MPN\",\"MTAP\",\"BSHT\",\"BBLA\",\"CQR\",\"GGV\",\"MPE\",\"KMZA\",\"HRO\",\"BSLA\",\"LBTL\",\"BGRD\",\"SXC\",\"BHKA\",\"KBGH\",\"KZPR\",\"TLG\",\"BRMH\",\"CRAE\",\"MRGM\",\"KGP\",\"JPR\",\n" +
                "            \"MPD\",\"SMCK\",\"BCK\",\"DUAN\",\"RDU\",\"HAUR\",\"KHAI\",\"BIR\",\"KZPB\",\"GMDN\",\"GKL\",\"MDN\",\"RGX\",\"RGA\",\"SMTG\",\"TMZ\",\"KSBP\",\"SSPH\",\"MSDL\",\"BRDB\",\"BYSA\",\"DZK\",\"DZKT\",\"BAAR\",\"HLZ\",\n" +
                "            \"BKNM\",\"KONA\",\"DNI\",\"JLBR\",\"MDC\",\"DMJR\",\"DJR\",\"DKB\",\"BAC\",\"PTHL\",\"MNH\",\"MHLN\",\"MJH\",\"JLI\",\"HDC\",\"AMZ\",\"JSOR\",\"RCD\",\"BLYH\",\"SHM\",\"PDPK\",\"KMRA\",\"KSHT\",\"CMDG\",\"KJRA\",\n" +
                "            \"KJRM\",\"ARNB\",\"BTRH\",\"BGNA\",\"DTG\",\"TLPH\",\"TKPH\",\"BALT\",\"KPAW\",\"VSU\",\"BSMH\",\"GNJP\",\"MYPR\",\"NDKR\",\"LSGS\",\"DSPN\",\"HEN\",\"NCN\",\"KATI\",\"STLB\",\"SJPA\",\"APRD\",\"BOPA\",\"RMRB\",\"TKRA\",\n" +
                "            \"DGHA\",\"TIT\",\"KAN\",\"GLI\",\"PAJ\",\"MNAE\",\"PAN\",\"RBH\",\"DGR\",\"OYR\",\"UDL\",\"RNG\",\"KPK\",\"ASN\",\"BQT\",\"INCI\",\"HIJ\",\"MDNR\",\"VSPR\",\"SNTR\",\"GOGT\",\"GMMG\",\"GMMR\",\"HPUR\",\"GMDP\",\n" +
                "            \"BUGM\",\"MTIP\",\"BOKA\",\"RNGR\",\"SHMR\",\"GOR\",\"SRBZ\",\"KYB\",\"GSQ\",\"BWCN\",\"SHJ\",\"SHKL\",\"INS\",\"KMRL\",\"BTRB\",\"PSF\",\"DGF\",\"DIM\",\"SONA\",\"HAM\",\"SIMR\",\"BDPR\",\"CNRF\",\"BZC\",\n" +
                "            \"BEX\",\"NOB\",\"BKNO\",\"BQA\",\"SOF\",\"NGX\",\"KCY\",\"BCF\",\"SIZ\", \"SPS\") and (name LIKE '%" + filter + "%' OR code LIKE '%" + filter + "%') ORDER BY name ASC";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(String.valueOf(c.getInt(0)));//station id
            listdata.add(c.getString(1));//domain_id
            listdata.add(c.getString(2));//station_code
            listdata.add(c.getString(3));//station name
            listdata.add(c.getString(4));//lat
            listdata.add(c.getString(5));//long
            listdata.add(c.getString(6));//sound dex

            list.add(listdata);
        }

        return list;
    }

//    public ArrayList<ArrayList<String>> GetTrainListBetweenStations(int srcId, int dstId) {
//        ArrayList<ArrayList<String>> list = new ArrayList<>();
//
//        String data = "select * from stop_times a INNER JOIN trips b ON a.trip_id=b.id LEFT JOIN routes c ON b.route_id=c.id LEFT JOIN stop_times st ON c.id = st.trip_id  where a.stop_id =" + srcId + " and st.stop_id =" + dstId + " and ( a.stop_sequence  <  st.stop_sequence)";
//        Cursor c = mDb.rawQuery(data, null);
//
//        while (c.moveToNext()) {
//            ArrayList<String> listdata = new ArrayList<>();
//
//            listdata.add(String.valueOf(c.getInt(1)));//trip_id
//            listdata.add(c.getString(3));//departure_time_src
//            listdata.add(dweString.valudseOf(c.getInt(4)));//id_src
//            listdata.add(String.valueOf(c.getInt(12)));//calendar_id
//            listdata.add(c.getString(15));//SHORT_NAME
//            listdata.add(c.getString(16));//LONG_NAME
//            listdata.add(c.getString(19));//arrival_time_dst
//            listdata.add(String.valueOf(c.getInt(21)));//id_dst

//            list.add(listdata);
//        }
//
//        return list;
//    }

    public ArrayList<ArrayList<String>> GetTrainListBetweenStations(int srcId, int dstId, String type) {
        ArrayList<ArrayList<String>> list = new ArrayList<>();

//        String data = "select * from stop_times a INNER JOIN trips b ON a.trip_id=b.id LEFT JOIN routes c ON b.route_id=c.id LEFT JOIN stop_times st ON c.id = st.trip_id  where a.stop_id =" + srcId + " and st.stop_id =" + dstId + " and ( a.stop_sequence  <  st.stop_sequence)";
        String data = "SELECT trips.id AS t_id, trips.domain_id AS tripDomainId, routes.id AS r_id, routes.domain_id AS r_domain_id, routes.short_name AS r_short_name, routes.long_name AS r_long_name, cal.id AS c_id, cal.days AS c_days, cal.start_date AS c_start_date, cal.end_date AS c_end_date, start_st.id AS sst_id, start_st.trip_id AS sst_trip_id, start_st.stop_id AS sst_stop_id, start_st.arrival_time AS sst_arrival_time, start_st.departure_time AS sst_departure_time, start_st.stop_sequence AS sst_stop_sequence, start_st.runday AS sst_runday, start_st.distance AS sst_distance, start_st.platform_number AS sst_platform_number, end_st.id AS est_id, end_st.trip_id AS est_trip_id, end_st.stop_id AS est_stop_id, end_st.arrival_time AS est_arrival_time, end_st.departure_time AS est_departure_time, end_st.stop_sequence AS est_stop_sequence, end_st.runday AS est_runday, end_st.distance AS est_distance, end_st.platform_number AS est_platform_number FROM trips INNER JOIN routes ON trips.route_id = routes.id INNER JOIN calendars AS cal ON trips.calendar_id = cal.id INNER JOIN stop_times AS start_st ON trips.id = start_st.trip_id INNER JOIN stops AS start_stop ON start_st.stop_id = start_stop.id INNER JOIN stop_times AS end_st ON trips.id = end_st.trip_id INNER JOIN stops AS end_stop ON end_st.stop_id = end_stop.id WHERE (start_st.departure_time < end_st.arrival_time OR start_st.runday < end_st.runday) AND start_st.stop_sequence < end_st.stop_sequence AND start_stop.domain_id IN ('" + srcId + "') AND end_stop.domain_id IN ('" + dstId + "') GROUP BY trips.id  ORDER BY sst_departure_time ASC";

        Cursor c = mDb.rawQuery(data, null);

        System.out.println("XxX:> SQL :::::::::::: " + data);
//        System.out.println("XxX:> C :::::::::::: " + c.getCount());

        while (c.moveToNext()) {                            // checking if the train is running
            ArrayList<String> listdata = new ArrayList<>();

            listdata.add(String.valueOf(c.getInt(c.getColumnIndex("t_id"))));               // trip_id
            listdata.add(c.getString(c.getColumnIndex("sst_departure_time")));              // departure_time_src
            listdata.add(String.valueOf(c.getInt(c.getColumnIndex("sst_stop_id"))));        // id_src
            listdata.add(String.valueOf(c.getInt(c.getColumnIndex("c_id"))));               // calendar_id
            listdata.add(c.getString(c.getColumnIndex("r_short_name")));                    // SHORT_NAME
            listdata.add(c.getString(c.getColumnIndex("r_long_name")));                     // LONG_NAME
            listdata.add(c.getString(c.getColumnIndex("est_departure_time")));              // arrival_time_dst
            listdata.add(String.valueOf(c.getInt(c.getColumnIndex("est_stop_sequence"))));  // id_dst
            listdata.add(c.getString(c.getColumnIndex("est_id")));                          // stop sequence
            listdata.add(c.getString(c.getColumnIndex("c_days")));                          // days
            listdata.add(c.getString(c.getColumnIndex("c_start_date")));                    // start date

            if (type.equalsIgnoreCase("express")) {
                list.add(listdata);
            } else {
                if (c.getInt(c.getColumnIndex("c_start_date")) > 0) {
                    list.add(listdata);
                }
            }
        }

        return list;
    }

    public ArrayList<ArrayList<String>> GetRouteDetailsKolkataLocal(int trip_id) {
        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from stop_times s LEFT JOIN stops r ON s.stop_id=r.id where s.trip_id =" + trip_id + " order by s.stop_sequence";
        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();

            listdata.add(c.getString(2));//arrival_time
            listdata.add(c.getString(3));//departure_time
            listdata.add(c.getString(11));//station_code
            listdata.add(c.getString(12));//station_name
            listdata.add(c.getString(13));//lat
            listdata.add(c.getString(14));//long
            listdata.add(String.valueOf(c.getInt(5)));//stop_sequence

            list.add(listdata);
        }

        return list;
    }

}

