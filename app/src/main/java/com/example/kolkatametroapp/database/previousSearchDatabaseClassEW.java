package com.example.kolkatametroapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class previousSearchDatabaseClassEW extends SQLiteOpenHelper {
    public static final String TAG = "databasehelper";
    public static final String TABLE_NAME = "Previous_searched_icon_EW"; //defining column and table name in database
    public static final String COL0 = "Id";
    public static final String COL1 = "srcId";
    public static final String COL2 = "src";
    public static final String COL3 = "srcCode";
    public static final String COL4 = "dstId";
    public static final String COL5 = "dst";
    public static final String COL6 = "dstCode";
    public static final String COL7 = "srcKannada";
    public static final String COL8 = "dstKannada";

    public previousSearchDatabaseClassEW(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {  //creating table if not exist
        String createTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + COL0 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL1 + " TEXT, " + COL2 + " TEXT, " + COL3 + " TEXT, " + COL4 + " TEXT, " + COL5 + " TEXT, " + COL6 + " TEXT, " + COL7 + " TEXT, " + COL8 + " TEXT) ";
        sqLiteDatabase.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void adddata(String srcId, String src, String srcCode, String dstId, String dst, String dstCode, String srcKannada, String dstKannada) {
        SQLiteDatabase db = this.getWritableDatabase();  //adding item to database
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, srcId);
        contentValues.put(COL2, src);
        contentValues.put(COL3, srcCode);
        contentValues.put(COL4, dstId);
        contentValues.put(COL5, dst);
        contentValues.put(COL6, dstCode);
        contentValues.put(COL7, srcKannada);
        contentValues.put(COL8, dstKannada);

        db.insert(TABLE_NAME, null, contentValues);
    }

    public ArrayList<ArrayList<String>> getdata() {   //getting the list of items from database
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COL0 + " DESC " + " LIMIT 5 ";
        Cursor c = db.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(c.getString(0));//id
            listdata.add(c.getString(1));//srcid
            listdata.add(c.getString(2));//src
            listdata.add(c.getString(3).replaceAll(" ", ""));//srccode
            listdata.add(c.getString(4));//dstid
            listdata.add(c.getString(5));//dst
            listdata.add(c.getString(6));//dstcode
            listdata.add(c.getString(7));//srckannada
            listdata.add(c.getString(8));//dstkannada

            list.add(listdata);
        }

        return list;
    }

    public boolean delete(String id) {   //to delete data from database
        SQLiteDatabase db = this.getWritableDatabase();
        //String query = "DELETE FROM " + TABLE_NAME + "WHERE ID = " + id;
        //Cursor c = db.rawQuery(query, null);
        return db.delete(TABLE_NAME, COL0 + "=?", new String[]{id}) > 0;
    }

    public String searchItemAlreadyExist(String srcid, String dstid) {   //for searching existing item in database
        SQLiteDatabase db = this.getReadableDatabase();
        String sourceid = '"' + srcid + '"';
        String destinationid = '"' + dstid + '"';

        String data = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL1 + " = " + sourceid + " AND " + COL4 + " = " + destinationid;

        Cursor c = db.rawQuery(data, null);

        if (c.getCount() <= 0) {
            c.close();
            return "";
        } else {
            c.moveToFirst();
            return c.getString(0);
        }
    }

}
