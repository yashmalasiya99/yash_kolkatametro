package com.example.kolkatametroapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

public class DataAdapter {
    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    public DataAdapter(Context context) {
        this.mContext = context;
        mDbHelper = new DatabaseHelper(mContext, "kolkataMetroDB.db", null, 1);
    }

    public DataAdapter createDatabase() throws SQLException {
        try {
            mDbHelper.createDataBase();
        } catch (IOException mIOException) {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public DataAdapter open() throws SQLException {
        try {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        } catch (SQLException mSQLException) {
            Log.e(TAG, "open >>" + mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public ArrayList<ArrayList<String>> getdatastationList() {
        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "SELECT * FROM Station ORDER BY StationName ASC";
        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(c.getString(0));//station id
            listdata.add(c.getString(1));//station name
            listdata.add(c.getString(2).replaceAll(" ", ""));//station code
            listdata.add(c.getString(3).replaceAll("\n", " "));//station name bengali
            listdata.add(c.getString(4));//line id
            listdata.add(c.getString(7));//landline
            listdata.add(c.getString(8));//mobile
            listdata.add(c.getString(10));//parking
            listdata.add(c.getString(14));//address
            listdata.add(c.getString(15));//entrygates
            listdata.add(c.getString(16));//exitgates

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> getdatastationListLine1() {
        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "SELECT * FROM Station WHERE LineID = 1 ORDER BY StationName ASC";
        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(c.getString(0));//station id
            listdata.add(c.getString(1));//station name
            listdata.add(c.getString(2).replaceAll(" ", ""));//station code
            listdata.add(c.getString(3).replaceAll("\n", " "));//station name bengali
            listdata.add(c.getString(4));//line id
            listdata.add(c.getString(7));//landline
            listdata.add(c.getString(8));//mobile
            listdata.add(c.getString(10));//parking
            listdata.add(c.getString(14));//address

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> getdatastationListLine2() {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "SELECT * FROM Station WHERE LineID = 2 ORDER BY StationName ASC";
        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(c.getString(0));//station id
            listdata.add(c.getString(1));//station name
            listdata.add(c.getString(2).replaceAll(" ", ""));//station code
            listdata.add(c.getString(3).replaceAll("\n", " "));//station name bengali
            listdata.add(c.getString(4));//line id
            listdata.add(c.getString(7));//landline
            listdata.add(c.getString(8));//mobile
            listdata.add(c.getString(10));//parking
            listdata.add(c.getString(14));//address

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<String> getstationNamefromStationCode(String stationCode) {
        ArrayList<String> list = new ArrayList<>();
        String station_code = '"' + stationCode + '"';

        String data = "SELECT * FROM Station where StationCode = " + station_code;
        Cursor c = mDb.rawQuery(data, null);

        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                list.add(c.getString(0));
                list.add(c.getString(1));
                list.add(c.getString(2).replaceAll(" ", ""));
                list.add(c.getString(3).replaceAll("\n", " "));
                list.add(c.getString(4));
                list.add(c.getString(5));
                list.add(c.getString(6));
                list.add(c.getString(7));
                list.add(c.getString(8));
                list.add(c.getString(9));
                list.add(c.getString(10));
                list.add(c.getString(11));
                list.add(c.getString(12));
                list.add(c.getString(13));
                list.add(c.getString(14));
                list.add(c.getString(15));
                list.add(c.getString(16));

            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getAllStationListBetween(int srcId, int dstId) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String data;

        if (((srcId >= 1 && srcId <= 26) && (dstId >= 1 && dstId <= 26)) || ((srcId >= 27 && srcId <= 34) && (dstId >= 27 && dstId <= 34))) {
            // Hardcore Value To be Update When Value is Adding in  Database
            data = "select distinct * from SequenceDetails where stationLIst like '%-" + srcId + "-%' and stationLIst like '%-" + dstId + "-%'";
        } else {
            data = "select distinct * from SequenceDetails";
        }

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(c.getString(0));
            listdata.add(c.getString(1));

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListStationBetweenStationIDsFareDetails(String fromSrc, String fromDes) {

        String source = "'" + fromSrc + "'";
        String destination = "'" + fromDes + "'";

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "SELECT * FROM FareDetails where FromStation = " + source + " AND ToStation = " + destination;

//        System.out.println("xxX: FareDetails SQL:>>- " + data);

        Cursor c = mDb.rawQuery(data, null);

//        Log.v("xxX:", "Cursor size: " + c.getCount());
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//fare id
                listdata.add(c.getString(1).replaceAll(" ", ""));//fromstation
                listdata.add(c.getString(2).replaceAll(" ", ""));//tostation
                listdata.add(c.getString(3));//fare
                listdata.add(c.getString(4));//line
                listdata.add(c.getString(5));//distance
                listdata.add(c.getString(6));//stops
//                listdata.add(c.getString(7));//remarks

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<String> getStationsForSrcToDst(int stationId) {
        ArrayList<String> list = new ArrayList<>();

        String data = "SELECT * FROM Station where StationID = " + stationId;
        Cursor c = mDb.rawQuery(data, null);

        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                list.add(c.getString(0));
                list.add(c.getString(1));
                list.add(c.getString(2).replaceAll(" ", ""));
                list.add(c.getString(3).replaceAll("\n", " "));
                list.add(c.getString(4));
                list.add(c.getString(5));
                list.add(c.getString(6));
                list.add(c.getString(7));
                list.add(c.getString(8));
                list.add(c.getString(9));
                list.add(c.getString(10));
                list.add(c.getString(11));
                list.add(c.getString(12));
                list.add(c.getString(13));
                list.add(c.getString(14));
                list.add(c.getString(15));
                list.add(c.getString(16));

            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListTimeBetSrcAndDest_mon(String fromStation, String fromDestination) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String fromsrc = '"' + fromStation + '"';
        String fromdst = '"' + fromDestination + '"';
        String data = "SELECT * FROM train_timings_MON where Source  = " + fromsrc + " AND Destination  = " + fromdst;
        Cursor c = mDb.rawQuery(data, null);

        Log.v("xxX:", "SQL: " + data);
        Log.v("xxX:", "Cursor size: " + c.getCount());
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//id
                listdata.add(c.getString(1).replaceAll(" ", ""));//src
                listdata.add(c.getString(2).replaceAll(" ", ""));//dst
                listdata.add(c.getString(3));//train_no
                listdata.add(c.getString(4));//start
                listdata.add(c.getString(5));//end
                listdata.add(c.getString(6));//frm_time
                listdata.add(c.getString(7));//to_time
                listdata.add(c.getString(8));//available
                listdata.add(c.getString(9));//active

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListTimeBetSrcAndDest_tue(String fromStation, String fromDestination) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String fromsrc = '"' + fromStation + '"';
        String fromdst = '"' + fromDestination + '"';
        String data = "SELECT * FROM train_timing_TUE where Source  = " + fromsrc + " AND Destination  = " + fromdst;
        Cursor c = mDb.rawQuery(data, null);

        Log.v("", "Cursor size: " + c.getCount());
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//id
                listdata.add(c.getString(1).replaceAll(" ", ""));//src
                listdata.add(c.getString(2).replaceAll(" ", ""));//dst
                listdata.add(c.getString(3));//train_no
                listdata.add(c.getString(4));//start
                listdata.add(c.getString(5));//end
                listdata.add(c.getString(6));//frm_time
                listdata.add(c.getString(7));//to_time
                listdata.add(c.getString(8));//available
                listdata.add(c.getString(9));//active

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListTimeBetSrcAndDest_wed(String fromStation, String fromDestination) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String fromsrc = '"' + fromStation + '"';
        String fromdst = '"' + fromDestination + '"';
        String data = "SELECT * FROM train_timing_WED where Source  = " + fromsrc + " AND Destination  = " + fromdst;
        Cursor c = mDb.rawQuery(data, null);

        Log.v("", "Cursor size: " + c.getCount());
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//id
                listdata.add(c.getString(1).replaceAll(" ", ""));//src
                listdata.add(c.getString(2).replaceAll(" ", ""));//dst
                listdata.add(c.getString(3));//train_no
                listdata.add(c.getString(4));//start
                listdata.add(c.getString(5));//end
                listdata.add(c.getString(6));//frm_time
                listdata.add(c.getString(7));//to_time
                listdata.add(c.getString(8));//available
                listdata.add(c.getString(9));//active

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListTimeBetSrcAndDest_thrus(String fromStation, String fromDestination) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String fromsrc = '"' + fromStation + '"';
        String fromdst = '"' + fromDestination + '"';
        String data = "SELECT * FROM train_timing_THRU where Source = " + fromsrc + " AND Destination = " + fromdst;
        Cursor c = mDb.rawQuery(data, null);

        Log.v("SQL", "SQL Thursday: " + data);
        Log.v("Count", "Cursor size: " + c.getCount());
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//id
                listdata.add(c.getString(1).replaceAll(" ", ""));//src
                listdata.add(c.getString(2).replaceAll(" ", ""));//dst
                listdata.add(c.getString(3));//train_no
                listdata.add(c.getString(4));//start
                listdata.add(c.getString(5));//end
                listdata.add(c.getString(6));//frm_time
                listdata.add(c.getString(7));//to_time
                listdata.add(c.getString(8));//available
                listdata.add(c.getString(9));//active

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListTimeBetSrcAndDest_fri(String fromStation, String fromDestination) {
        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String fromsrc = '"' + fromStation + '"';
        String fromdst = '"' + fromDestination + '"';
        String data = "SELECT * FROM train_timings_FRI where Source  = " + fromsrc + " AND Destination  = " + fromdst;

//        System.out.println("xXxX: FRIDAY SQL  : " + data);

        Cursor c = mDb.rawQuery(data, null);

//        System.out.println("xXxX: Cursor size: " + c.getCount());

        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//id
                listdata.add(c.getString(1).replaceAll(" ", ""));//src
                listdata.add(c.getString(2).replaceAll(" ", ""));//dst
                listdata.add(c.getString(3));//train_no
                listdata.add(c.getString(4));//start
                listdata.add(c.getString(5));//end
                listdata.add(c.getString(6));//frm_time
                listdata.add(c.getString(7));//to_time
                listdata.add(c.getString(8));//available
                listdata.add(c.getString(9));//active

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListTimeBetSrcAndDest_sat(String fromStation, String fromDestination) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String fromsrc = '"' + fromStation + '"';
        String fromdst = '"' + fromDestination + '"';
        String data = "SELECT * FROM train_timings_SAT where Source  = " + fromsrc + " AND Destination  = " + fromdst;
        Cursor c = mDb.rawQuery(data, null);

        Log.v("", "Cursor size: " + c.getCount());
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//id
                listdata.add(c.getString(1).replaceAll(" ", ""));//src
                listdata.add(c.getString(2).replaceAll(" ", ""));//dst
                listdata.add(c.getString(3));//train_no
                listdata.add(c.getString(4));//start
                listdata.add(c.getString(5));//end
                listdata.add(c.getString(6));//frm_time
                listdata.add(c.getString(7));//to_time
                listdata.add(c.getString(8));//available
                listdata.add(c.getString(9));//active

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getMetroListTimeBetSrcAndDest_sun(String fromStation, String fromDestination) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();
        String fromsrc = '"' + fromStation + '"';
        String fromdst = '"' + fromDestination + '"';
        String data = "SELECT * FROM train_timing_SUN where Source  = " + fromsrc + " AND Destination  = " + fromdst;
        Cursor c = mDb.rawQuery(data, null);

        Log.v("", "Cursor size: " + c.getCount());
        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));//id
                listdata.add(c.getString(1).replaceAll(" ", ""));//src
                listdata.add(c.getString(2).replaceAll(" ", ""));//dst
                listdata.add(c.getString(3));//train_no
                listdata.add(c.getString(4));//start
                listdata.add(c.getString(5));//end
                listdata.add(c.getString(6));//frm_time
                listdata.add(c.getString(7));//to_time
                listdata.add(c.getString(8));//available
                listdata.add(c.getString(9));//active

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getstationlListFromStationId(int stationId) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from SequenceDetails where stationLIst like '%-" + stationId + "-%'";
        System.out.println(data);

        Cursor c = mDb.rawQuery(data, null);

        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0)); //sequence id
                listdata.add(c.getString(1)); //stationlist

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> getAllStations() {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "SELECT * FROM Station";
        Cursor c = mDb.rawQuery(data, null);

        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                ArrayList<String> listdata = new ArrayList<>();
                listdata.add(c.getString(0));
                listdata.add(c.getString(1));
                listdata.add(c.getString(2).replaceAll(" ", ""));
                listdata.add(c.getString(3).replaceAll("\n", " "));
                listdata.add(c.getString(4));
                listdata.add(c.getString(5));
                listdata.add(c.getString(6));
                listdata.add(c.getString(7));
                listdata.add(c.getString(8));
                listdata.add(c.getString(9));
                listdata.add(c.getString(10));
                listdata.add(c.getString(11));
                listdata.add(c.getString(12));
                listdata.add(c.getString(13));
                listdata.add(c.getString(14));
                listdata.add(c.getString(15));
                listdata.add(c.getString(16));

                list.add(listdata);
            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

}
