package com.example.kolkatametroapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

public class DataAdapterBus {
    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DatabaseHelperBus mDbHelper;

    public DataAdapterBus(Context context) {
        this.mContext = context;
        mDbHelper = new DatabaseHelperBus(mContext, "database.db", null, 1);
    }

    public DataAdapterBus createDatabase() throws SQLException {
        try {
            mDbHelper.createDataBase();
        } catch (IOException mIOException) {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public DataAdapterBus open() throws SQLException {
        try {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        } catch (SQLException mSQLException) {
            Log.e(TAG, "open >>" + mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public ArrayList<ArrayList<String>> GetDataStationListKolkataBus() {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from bus_station";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(String.valueOf(c.getInt(0)));//station id
            listdata.add(c.getString(1));//station_code
            listdata.add(c.getString(2));//station_name_eg
            listdata.add(c.getString(3));//station_name_bn
            listdata.add(c.getString(4));//station_name_hn

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> GetDataStationListKolkataBusFilter(String filter) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from bus_station where station_name_eng LIKE '%" + filter + "%'";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(String.valueOf(c.getInt(0)));//station id
            listdata.add(c.getString(1));//station_code
            listdata.add(c.getString(2));//station_name_eg
            listdata.add(c.getString(3));//station_name_bn
            listdata.add(c.getString(4));//station_name_hn

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<String> getTypeOfStationsForSrcAndDst(int stationId) {
        ArrayList<String> list = new ArrayList<>();

        String data = "SELECT * FROM bus_station where station_id =" + stationId;
        Cursor c = mDb.rawQuery(data, null);

        c.moveToFirst();
        if (c.moveToFirst()) {
            do {
                list.add(String.valueOf(c.getInt(0)));//station id
                list.add(c.getString(1));//station_code
                list.add(c.getString(2));//station_name_eg
                list.add(c.getString(3));//station_name_bn
                list.add(c.getString(4));//station_name_hn

            }
            while (c.moveToNext());
        }
        c.close();
        return list;
    }

    public ArrayList<ArrayList<String>> GetDataBusList(int src_id, int dst_id) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from bus_route br INNER JOIN bus_detail bd ON ( br.bus_id = bd.bus_id )\n" +
                "where br.station_id =" + src_id + " or br.station_id =" + dst_id + " group by br.bus_id having count(br.bus_id) > 1";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(String.valueOf(c.getInt(4)));//bus_id
            listdata.add(c.getString(5));//bus_number
            listdata.add(String.valueOf(c.getInt(6)));//bus_type
            listdata.add(c.getString(7));//bus_operator

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> GetSrcDstRouteArray(int bus_id) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from bus_route br INNER JOIN bus_station bs ON br.station_id = bs.station_id  where bus_id =" + bus_id + " order by position";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(c.getString(5));//station_code
            listdata.add(c.getString(6));//station_name_eng
            listdata.add(c.getString(7));//station_name_bng
            listdata.add(c.getString(3));//position

            list.add(listdata);
        }

        return list;
    }

    public ArrayList<ArrayList<String>> GetSrcDstRouteArrayReverse(int bus_id) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();

        String data = "select * from bus_route br INNER JOIN bus_station bs ON br.station_id = bs.station_id  where bus_id =" + bus_id + " order by position DESC";

        Cursor c = mDb.rawQuery(data, null);

        while (c.moveToNext()) {
            ArrayList<String> listdata = new ArrayList<>();
            listdata.add(c.getString(5));//station_code
            listdata.add(c.getString(6));//station_name_eng
            listdata.add(c.getString(7));//station_name_bng
            listdata.add(c.getString(3));//position

            list.add(listdata);
        }

        return list;
    }

}
