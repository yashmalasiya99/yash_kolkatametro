package com.example.kolkatametroapp.classes;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.androidnetworking.AndroidNetworking;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class ApiApplicationClass extends Application {

    public static final String TAG = ApiApplicationClass.class.getSimpleName();

    private static Context context;


    private static ApiApplicationClass mInstance;

    public static Context getAppContext() {
        return ApiApplicationClass.context;
    }

    public static synchronized ApiApplicationClass getInstance() {
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);


    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            mInstance = this;
            ApiApplicationClass.context = getApplicationContext();

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .retryOnConnectionFailure(true)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .build();

            AndroidNetworking.initialize(getApplicationContext(), okHttpClient);


        } catch (Exception e) {

        }
    }
}