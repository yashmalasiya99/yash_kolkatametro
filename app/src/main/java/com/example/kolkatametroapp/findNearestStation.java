package com.example.kolkatametroapp;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;


public class findNearestStation extends AppCompatActivity {

    public static final String MyPREFERENCES = "LangaugePref";
    private static final int PERMISSION_LOCATION_REQUEST_CODE = 200;
    SeekBar seekBar;
    Button srch_btn;
    LinearLayout linear;
    TextView current_progress_value, kms;
    Toolbar toolbar;
    SharedPreferences preferences;
    String lang;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }
        setContentView(R.layout.nearest_station_layout);

        toolbar = findViewById(R.id.toolbar_neareststation);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();

        InterstitialAd.load(findNearestStation.this, getResources().getString(R.string.inter), adRequest1,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        mInterstitialAd = null;
                    }
                });
        seekBar = findViewById(R.id.seekbar_radius);
        kms = findViewById(R.id.kms);
        srch_btn = findViewById(R.id.search_button_km_radius);
        linear = findViewById(R.id.find_nearest_station_linear);
        current_progress_value = findViewById(R.id.current_progress_value);


        if (checkPermission()) {
            //permission already granted
        } else {
            requestPermission();
        }

        seekBar.setMax(50);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                current_progress_value.setText(String.valueOf(i));
                if (i > 1) {
                    kms.setText(R.string.station_radius_kms);
                } else {
                    kms.setText(R.string.station_radius_km);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //on click intent to nearest station list
        srch_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String range = current_progress_value.getText().toString();

                if (range.equals("0")) {
                    Snackbar snackbar = Snackbar
                            .make(linear, " Please select station radius more than 0.", Snackbar.LENGTH_SHORT);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(findNearestStation.this, R.color.white));
                    snackbar.show();
                } else {
                    Intent i = new Intent(findNearestStation.this, nearestStationList.class);
                    i.putExtra("range", range);
                    startActivity(i);
                    if (mInterstitialAd != null) {
                        mInterstitialAd.show(findNearestStation.this);
                    }
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item1, menu);

        MenuItem routeMapMenu = menu.findItem(R.id.route_map_icon);
        ImageView iv = new ImageView(this);
        iv.setBackground(getResources().getDrawable(R.drawable.location_drawable_white));
        Animation animation = new AlphaAnimation((float) 2, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        iv.startAnimation(animation);
        routeMapMenu.setActionView(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(findNearestStation.this, routeMapWebview.class);
                startActivity(intent);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(findNearestStation.this, ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(findNearestStation.this, ACCESS_COARSE_LOCATION);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, PERMISSION_LOCATION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 200) {
            if (grantResults.length > 0) {
                boolean locationAcceptedFine = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean locationAcceptedCoarse = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                if (locationAcceptedFine && locationAcceptedCoarse) {
                    //permission accepted
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setMessage("You cannot use this functionality without allowing device location");
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.setPositiveButton("Allow",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    requestPermission();
                                }
                            });

                    alertDialogBuilder.setNegativeButton("Go back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();
                }

            }
        }

    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
