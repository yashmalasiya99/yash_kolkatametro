package com.example.kolkatametroapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.previousSearchedItemAdapterKolkataLocal;
import com.example.kolkatametroapp.database.previous_search_database_local;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.material.snackbar.Snackbar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class kolkata_local_find_route_class extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    Toolbar toolbar;
    CardView src_card, dst_card;
    TextView src_txt, dst_txt, saved_schedules;
    int LAUNCH_SRC = 1;
    int LAUNCH_DST = 2;
    Button srch_btn;
    ImageView interchange_src_dst;
    TextView textView_time;
    String get_src_txt, get_dst_txt;
    String srcStationId, dstStationId;
    String srcStationCode, dstStationCode;
    CoordinatorLayout coordinateLayout;
    RecyclerView recyclerView;
    ArrayList<ArrayList<String>> lists;
    previous_search_database_local databaseClass;
    previousSearchedItemAdapterKolkataLocal adapter;
    LinearLayout linearLayout_date_picker;
    String lang;
    String time = "";
    String deporarrival = "";
    //    DataAdapterTrains dbAdapter;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kolkata_local_find_route);

        toolbar = findViewById(R.id.toolbar_local_find_route);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.kolkata_local);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        AdRequest adRequest1 = new AdRequest.Builder().build();
        InterstitialAd.load(kolkata_local_find_route_class.this, getResources().getString(R.string.inter), adRequest1,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        mInterstitialAd = null;
                    }
                });

        src_card = findViewById(R.id.crd_src_local);
        dst_card = findViewById(R.id.crd_dst_local);
        src_txt = findViewById(R.id.txt_src_local);
        dst_txt = findViewById(R.id.txt_dst_local);
        srch_btn = findViewById(R.id.btn_srch_local);
        interchange_src_dst = findViewById(R.id.img_overlap_local);
        coordinateLayout = findViewById(R.id.relative_layout_kolkata_local);
        recyclerView = findViewById(R.id.recycler_view_local);
        saved_schedules = findViewById(R.id.saved_schedules_local);
        linearLayout_date_picker = findViewById(R.id.linear_layout_time_picker);
        textView_time = findViewById(R.id.text_view_time_local);


        databaseClass = new previous_search_database_local(kolkata_local_find_route_class.this);

//        dbAdapter = new DataAdapterTrains(kolkata_local_find_route_class.this);
//        dbAdapter.createDatabase();
//        dbAdapter.open();
        linearLayout_date_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(kolkata_local_find_route_class.this);
                AlertDialog alertDialog = builder.create();
                View view_dialog = getLayoutInflater().inflate(R.layout.dialog_time_picker_local, null);






                TextView set_text = view_dialog.findViewById(R.id.text_view_set_dialog_time_picker);
                TextView reset_text = view_dialog.findViewById(R.id.text_view_reset_dialog_time_picker);

                TimePicker picker = (TimePicker) view_dialog.findViewById(R.id.time_Picker_dialog);
                picker.setIs24HourView(true);

                TextView text_cancel = view_dialog.findViewById(R.id.text_view_cancel_dialog_time_picker);


                text_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                set_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                            deporarrival = "1";
                            DateFormat df = new SimpleDateFormat("HH:mm");
                            DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
                            Date date = null;

                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    date = df.parse(picker.getCurrentHour() + ":" + picker.getMinute());
                                    time = outputformat.format(date);
                                    Log.d("time",time+deporarrival);
                                    textView_time.setText(time);
                                    alertDialog.dismiss();
                                }

                            } catch (ParseException e) {
                                Log.d("error 69", e.getMessage());
                            }




                    }});
                alertDialog.setView(view_dialog);
                alertDialog.show();
                reset_text.setOnClickListener(new View.OnClickListener()

                    {
                        @Override
                        public void onClick (View view){
                        Calendar c = Calendar.getInstance();
                        picker.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
                        picker.setCurrentMinute(c.get(Calendar.MINUTE));
                    }
                    });


                }
            });

        src_card.setOnClickListener(new View.OnClickListener()

            {  //intent to srcsearch
                @Override
                public void onClick (View view){
                Intent i = new Intent(kolkata_local_find_route_class.this, kolkataSrchListLocal.class);
                startActivityForResult(i, LAUNCH_SRC);
            }
            });

        dst_card.setOnClickListener(new View.OnClickListener()

            { //intent to source search
                @Override
                public void onClick (View view){
                Intent i = new Intent(kolkata_local_find_route_class.this, kolkataSrchListLocal.class);
                startActivityForResult(i, LAUNCH_DST);
            }
            });

        interchange_src_dst.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View view){
                get_src_txt = src_txt.getText().toString();
                get_dst_txt = dst_txt.getText().toString();

                if (get_src_txt.equals("")) {  //if source is null
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, " Select valid source", Snackbar.LENGTH_SHORT)
                            .setAction("SELECT", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i = new Intent(kolkata_local_find_route_class.this, kolkataSrchListLocal.class);
                                    startActivityForResult(i, LAUNCH_SRC);
                                }
                            });
                    snackbar.setActionTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.red));
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.white));
                    snackbar.show();
                } else if (get_dst_txt.equals("")) { //if destination is null
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, " Select valid destination", Snackbar.LENGTH_SHORT)
                            .setAction("SELECT", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i = new Intent(kolkata_local_find_route_class.this, kolkataSrchListLocal.class);
                                    startActivityForResult(i, LAUNCH_DST);
                                }
                            });
                    snackbar.setActionTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.red));
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.white));
                    snackbar.show();
                } else {
                    String srcid = srcStationId;
                    String dstid = dstStationId;
                    String src_code = srcStationCode;
                    String dst_code = dstStationCode;

                    src_txt.setText(get_dst_txt);
                    dst_txt.setText(get_src_txt);
                    srcStationId = dstid;
                    dstStationId = srcid;
                    srcStationCode = dst_code;
                    dstStationCode = src_code;
                }
            }
            });

        srch_btn.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View view){
                get_src_txt = src_txt.getText().toString();
                get_dst_txt = dst_txt.getText().toString();

                if (get_src_txt.equals("")) {  //if source is null
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, " Select valid source", Snackbar.LENGTH_SHORT)
                            .setAction("SELECT", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i = new Intent(kolkata_local_find_route_class.this, kolkataSrchListLocal.class);
                                    startActivityForResult(i, LAUNCH_SRC);
                                }
                            });
                    snackbar.setActionTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.red));
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.white));
                    snackbar.show();
                } else if (get_dst_txt.equals("")) { //if destination is null
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, " Select valid destination", Snackbar.LENGTH_SHORT)
                            .setAction("SELECT", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i = new Intent(kolkata_local_find_route_class.this, kolkataSrchListLocal.class);
                                    startActivityForResult(i, LAUNCH_DST);
                                }
                            });
                    snackbar.setActionTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.red));
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.white));
                    snackbar.show();
                } else if (get_src_txt.equals(get_dst_txt) || get_dst_txt.equals(get_src_txt)) {
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, "Source and Destination can't be same.", Snackbar.LENGTH_SHORT);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(kolkata_local_find_route_class.this, R.color.white));
                    snackbar.show();
                } else {
                    String valueOfSearchedItemExistOrNot = databaseClass.searchItemAlreadyExist(srcStationId, dstStationId);
                    if (!valueOfSearchedItemExistOrNot.equals("")) {  //if searching item is already present in database (check)
                        databaseClass.delete(valueOfSearchedItemExistOrNot); //delete the item from database
                    }
                    databaseClass.adddata(srcStationId, get_src_txt, srcStationCode, dstStationId, get_dst_txt, dstStationCode, "", "");

                    Intent i = new Intent(kolkata_local_find_route_class.this, searchedKolkataLocalList.class); //intent the values to search metro lists
                    i.putExtra("src", get_src_txt);
                    i.putExtra("srcid", srcStationId);
                    i.putExtra("src_code", srcStationCode);
                    i.putExtra("dst", get_dst_txt);
                    i.putExtra("dstid", dstStationId);
                    i.putExtra("dst_code", dstStationCode);
                    i.putExtra("type", "local");
                    i.putExtra("deparv", deporarrival);
                    i.putExtra("time", time);


                    startActivity(i);
                    if (mInterstitialAd != null) {
                        mInterstitialAd.show(kolkata_local_find_route_class.this);
                    }
                }
            }
            });

        if(savedInstanceState !=null)

            {

                String source = savedInstanceState.getString("srcName");
                String destination = savedInstanceState.getString("dstName");
                String sourceId = savedInstanceState.getString("srcId");
                String destId = savedInstanceState.getString("dstId");
                String src_code = savedInstanceState.getString("srcCode");
                String dst_code = savedInstanceState.getString("dstCode");

                src_txt.setText(source);
                dst_txt.setText(destination);
                srcStationId = sourceId;
                dstStationId = destId;
                srcStationCode = src_code;
                dstStationCode = dst_code;
            }
        }

        @Override
        public void onActivityResult ( int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == LAUNCH_SRC) {
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");
//                srcStationId = data.getStringExtra("stationId");
                    srcStationId = data.getStringExtra("domainId");
                    srcStationCode = data.getStringExtra("station_code");
                    src_txt.setText(result);
//                System.out.println("XxX:> srcresuk" + result);
//                System.out.println("XxX:> srcid" + srcStationId);
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            }
            if (requestCode == LAUNCH_DST) {
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");
//                dstStationId = data.getStringExtra("stationId");
                    dstStationId = data.getStringExtra("domainId");
                    dstStationCode = data.getStringExtra("station_code");
                    dst_txt.setText(result);
//                System.out.println("desresuk" + result);
//                System.out.println("srcid" + dstStationId);
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Write your code if there's no result
                }
            }
        }

        @Override
        public void onResume () {
            super.onResume();

            lists = databaseClass.getdata();
            //if list size is not 0 then show recent search else remove visibility
            if (lists.size() != 0) {
                saved_schedules.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.VISIBLE);

                adapter = new previousSearchedItemAdapterKolkataLocal(lists, lang, kolkata_local_find_route_class.this);
                recyclerView.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(kolkata_local_find_route_class.this);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);

            } else {
                saved_schedules.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            }
        }

        public void remove_saved_visiblity () {
            saved_schedules.setVisibility(View.GONE);
        }

        @Override
        public void onSaveInstanceState (Bundle savedInstanceState){

            // Save UI state changes to the savedInstanceState.
            // This bundle will be passed to onCreate if the process is
            // killed and restarted.
            get_src_txt = src_txt.getText().toString();
            get_dst_txt = dst_txt.getText().toString();

            savedInstanceState.putString("srcId", srcStationId);
            savedInstanceState.putString("dstId", dstStationId);
            savedInstanceState.putString("srcName", get_src_txt);
            savedInstanceState.putString("dstName", get_dst_txt);
            savedInstanceState.putString("srcCode", srcStationCode);
            savedInstanceState.putString("dstCode", dstStationCode);

            // etc.

            super.onSaveInstanceState(savedInstanceState);
        }

        public void setLocale (String lang){ //call this in onCreate()
            Locale myLocale = new Locale(lang);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            switch (item.getItemId()) {
                case android.R.id.home:
                    this.finish();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
    }
