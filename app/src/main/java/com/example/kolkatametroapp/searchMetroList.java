package com.example.kolkatametroapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.time_fragment_recycler_adapter;
import com.example.kolkatametroapp.database.DataAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class searchMetroList extends AppCompatActivity {
    public static final int ITEMS_PER_AD = 5;
    public static final String MyPREFERENCES = "LangaugePref";
    Toolbar toolbar;
    TextView src_display_txtview, dst_display_txtview, km_value_txtview, rs_value_txtview, min_value_txtview, station_value_txtview, changes_value_txtview;
    ImageView img_back;
    String src_from_main, dst_from_main, src_id_from_main, dst_id_from_main, src_code_from_main, dst_code_from_main;
    DataAdapter dbAdapter;
    int INTERCHANGE;
    int src_id, dst_id;
    int src_pos, dst_pos;
    int pos_dst, pos_src;
    int days, hours, min;
    String[] sequencelist;
    String SeqID;
    String LocalTime;
    String time_from, time_to;
    String Date;
    Date date1, date2;
    String Reverse = "0";
    ArrayList<ArrayList<String>> srcDestTimeArrayList = new ArrayList<>();
    ArrayList<ArrayList<String>> srcDestTimeArrayList_twelve_hr_format = new ArrayList<>();
    ArrayList<ArrayList<String>> srcDestTimeArrayList_twelve_hr_format_now_onwards = new ArrayList<>();
    ArrayList<Integer> sequenceList = new ArrayList<>();
    ArrayList<Integer> srcToDestSequenceList = new ArrayList<>();

    RadioGroup radioGroup;
    RadioButton radioNowOnwards, radioFullDay;
    RecyclerView recyclerView;
    time_fragment_recycler_adapter Adapter;
    LinearLayout no_trains_available;
    SharedPreferences preferences;
    String lang, type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        try {






            lang = preferences.getString("langauge", "");
            if (lang.equals("")) {
                setLocale("en");
            } else {
                setLocale(lang);
            }

            setContentView(R.layout.searched_metro_list_layout);

            toolbar = findViewById(R.id.searched_item_toolbar);
            setSupportActionBar(toolbar);

            AdView bannerView = findViewById(R.id.adView);
            bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            AdRequest adRequest = new AdRequest.Builder().build();
            bannerView.loadAd(adRequest);

            src_from_main = getIntent().getStringExtra("src");
            src_id_from_main = getIntent().getStringExtra("srcid");
            src_code_from_main = getIntent().getStringExtra("src_code");
            dst_from_main = getIntent().getStringExtra("dst");
            dst_id_from_main = getIntent().getStringExtra("dstid");
            dst_code_from_main = getIntent().getStringExtra("dst_code");
            type = getIntent().getStringExtra("type");

            src_display_txtview = findViewById(R.id.searched_src_display);
            dst_display_txtview = findViewById(R.id.searched_dst_display);
            km_value_txtview = findViewById(R.id.km_value);
            rs_value_txtview = findViewById(R.id.rs_value);
            min_value_txtview = findViewById(R.id.min_value);
            station_value_txtview = findViewById(R.id.stations_value);
            changes_value_txtview = findViewById(R.id.changes_values);
            img_back = findViewById(R.id.img_back);
            recyclerView = findViewById(R.id.recycler_view_time_fragment);
            no_trains_available = findViewById(R.id.no_trains_available_layout);

//            Button button = null;
//            button.setText("DDDDD");

            radioNowOnwards = findViewById(R.id.radioNowOnwards);
            radioFullDay = findViewById(R.id.radioFullDay);
            radioGroup = findViewById(R.id.radiotime);

            src_display_txtview.setText(src_from_main);
            dst_display_txtview.setText(dst_from_main);

            src_id = Integer.valueOf(src_id_from_main);
            dst_id = Integer.valueOf(dst_id_from_main);

//        System.out.println("xXxX: src: " + src_from_main);
//        System.out.println("xXxX: src CODE: " + src_code_from_main);
//        System.out.println("xXxX: dst code: " + dst_code_from_main);
//        System.out.println("xXxX: srcid: " + src_id_from_main);
//        System.out.println("xXxX: dst: " + dst_from_main);
//        System.out.println("xXxX: dstid: " + dst_id_from_main);

            dbAdapter = new DataAdapter(searchMetroList.this);
            dbAdapter.createDatabase();
            dbAdapter.open();

            ArrayList<ArrayList<String>> betweendatalist = dbAdapter.getAllStationListBetween(src_id, dst_id);
            System.out.println("ADDINGG");
            System.out.println(betweendatalist);
//        System.out.println("xxX: betweendatalist size :::::::::::: " + betweendatalist.toString());
//        System.out.println("xXxX: betweendatalist :::::::::::: " + betweendatalist.get(0).get(1));

            //condition for sequence list
            if (betweendatalist.size() <= 1) {

                System.out.println("ADDINGG");
                // returning only one row
                INTERCHANGE = 0;
                SeqID = betweendatalist.get(0).get(0);
                sequencelist = betweendatalist.get(0).get(1).split("-");


                // Convert String[] to ArrayList Directly
                for (String split : sequencelist) {
                    if (!split.equals("")) {
                        sequenceList.add(Integer.valueOf(split));
                    }
                }

                src_pos = sequenceList.indexOf(src_id);
                dst_pos = sequenceList.indexOf(dst_id);

                if (src_pos > dst_pos) {
                    Collections.reverse(sequenceList);
                    Reverse = "1";
                }

                pos_src = sequenceList.indexOf(src_id);
                pos_dst = sequenceList.indexOf(dst_id);

                for (int k = pos_src; k <= pos_dst; k++) {
                    srcToDestSequenceList.add(sequenceList.get(k));
                }
            } else {
                // returning 2 rows
                INTERCHANGE = 1;
            }

//        System.out.println("xXxX: srctodstlist" + srcToDestSequenceList.size());

            //src to dest fare details
            ArrayList<ArrayList<String>> srcToDesFareDetails = dbAdapter.getMetroListStationBetweenStationIDsFareDetails(src_code_from_main, dst_code_from_main);
//        System.out.println("xxX: srcToDesFareDetails :: " + srcToDesFareDetails.toString());

            img_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            SimpleDateFormat sdfo = new SimpleDateFormat("hh:mm a");

            // for getting current time
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            Date currentLocalTime = cal.getTime();
            DateFormat date = new SimpleDateFormat("hh:mm a");
            date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));

            LocalTime = date.format(currentLocalTime);

            // for getting current date
            Calendar calendar = Calendar.getInstance();
            Date currdate = calendar.getTime();
            DateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
            Date = dateFor.format(currdate);

            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
            Date dt1 = null;
            try {
                dt1 = format1.parse(Date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // convert date to weekday
            DateFormat format2 = new SimpleDateFormat("EEEE");
            String finalDay = format2.format(dt1);
//        System.out.println(" final day : " + finalDay);

            if (finalDay.equals("Monday")) {
                srcDestTimeArrayList = dbAdapter.getMetroListTimeBetSrcAndDest_mon(src_code_from_main, dst_code_from_main);
            } else if (finalDay.equals("Tuesday")) {
                srcDestTimeArrayList = dbAdapter.getMetroListTimeBetSrcAndDest_tue(src_code_from_main, dst_code_from_main);
            } else if (finalDay.equals("Wednesday")) {
                srcDestTimeArrayList = dbAdapter.getMetroListTimeBetSrcAndDest_wed(src_code_from_main, dst_code_from_main);
            } else if (finalDay.equals("Thursday")) {
                srcDestTimeArrayList = dbAdapter.getMetroListTimeBetSrcAndDest_thrus(src_code_from_main, dst_code_from_main);
            } else if (finalDay.equals("Friday")) {
                srcDestTimeArrayList = dbAdapter.getMetroListTimeBetSrcAndDest_fri(src_code_from_main, dst_code_from_main);
            } else if (finalDay.equals("Saturday")) {
                srcDestTimeArrayList = dbAdapter.getMetroListTimeBetSrcAndDest_sat(src_code_from_main, dst_code_from_main);
            } else {
                srcDestTimeArrayList = dbAdapter.getMetroListTimeBetSrcAndDest_sun(src_code_from_main, dst_code_from_main);
            }

            dbAdapter.close();

//        System.out.println("xxX: srcDestTimeArrayList size  : " + srcDestTimeArrayList.size());

            //converting 24hr format to 12hr format
            for (int j = 0; j < srcDestTimeArrayList.size(); j++) {
                try {
                    if (srcDestTimeArrayList.get(j).get(6) == null || srcDestTimeArrayList.get(j).get(6).trim().isEmpty()) {
                        time_from = "-";
                    } else {
                        SimpleDateFormat sdf_frm = new SimpleDateFormat("HH:mm");
                        Date dateObj_frm = sdf_frm.parse(srcDestTimeArrayList.get(j).get(6));
                        time_from = new SimpleDateFormat("hh:mm aa").format(dateObj_frm);
                    }

                    if (srcDestTimeArrayList.get(j).get(7) == null || srcDestTimeArrayList.get(j).get(7).trim().isEmpty()) {
                        time_to = "-";
                    } else {
                        SimpleDateFormat sdf_to = new SimpleDateFormat("HH:mm");
                        Date dateObj_to = sdf_to.parse(srcDestTimeArrayList.get(j).get(7));
                        time_to = new SimpleDateFormat("hh:mm aa").format(dateObj_to);
                    }

                    ArrayList<String> twelve_hr_frmt_time_array = new ArrayList<>();
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(0));
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(1));
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(2));
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(3));
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(4));
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(5));
                    twelve_hr_frmt_time_array.add(time_from);
                    twelve_hr_frmt_time_array.add(time_to);
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(8));
                    twelve_hr_frmt_time_array.add(srcDestTimeArrayList.get(j).get(9));

                    srcDestTimeArrayList_twelve_hr_format.add(twelve_hr_frmt_time_array);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

//        System.out.println("xXxX: all size before ads : " + srcDestTimeArrayList_twelve_hr_format.size());

            // showing now onwards time
            for (int j = 0; j < srcDestTimeArrayList_twelve_hr_format.size(); j++) {
                try {
                    Date currentTime = sdfo.parse(LocalTime);
                    Date listTime = sdfo.parse(srcDestTimeArrayList_twelve_hr_format.get(j).get(6));

                    if (listTime.after(currentTime)) {
                        ArrayList<String> twelve_hr_frmt_time_array_now_onwards = new ArrayList<>();
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(0));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(1));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(2));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(3));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(4));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(5));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(6));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(7));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(8));
                        twelve_hr_frmt_time_array_now_onwards.add(srcDestTimeArrayList_twelve_hr_format.get(j).get(9));

                        srcDestTimeArrayList_twelve_hr_format_now_onwards.add(twelve_hr_frmt_time_array_now_onwards);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

//        System.out.println("xXxX: srcDestTimeArrayList_twelve_hr_format_now_onwards: before ads : " + srcDestTimeArrayList_twelve_hr_format_now_onwards.size());

            for (int i = ITEMS_PER_AD; i <= srcDestTimeArrayList_twelve_hr_format.size(); i += ITEMS_PER_AD) {
                ArrayList<String> twelve_hr_frmt_time_array_for_ads = new ArrayList<>();
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);

                srcDestTimeArrayList_twelve_hr_format.add(i, twelve_hr_frmt_time_array_for_ads);
            }

//        System.out.println("xXxX: all size after ads : " + srcDestTimeArrayList_twelve_hr_format.size());

            for (int i = ITEMS_PER_AD; i <= srcDestTimeArrayList_twelve_hr_format_now_onwards.size(); i += ITEMS_PER_AD) {
                ArrayList<String> twelve_hr_frmt_time_array_now_onwards_for_ads = new ArrayList<>();
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);
                twelve_hr_frmt_time_array_now_onwards_for_ads.add(null);

                srcDestTimeArrayList_twelve_hr_format_now_onwards.add(i, twelve_hr_frmt_time_array_now_onwards_for_ads);
            }

//        System.out.println("xXxX: now size after ads : " + srcDestTimeArrayList_twelve_hr_format_now_onwards.toString());

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioNowOnwards.isChecked()) {
                        if (srcDestTimeArrayList_twelve_hr_format_now_onwards.size() == 0) {
                            no_trains_available.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            no_trains_available.setVisibility(View.GONE);
                            Adapter = new time_fragment_recycler_adapter(srcDestTimeArrayList_twelve_hr_format_now_onwards, lang, src_id_from_main, dst_id_from_main,
                                    src_code_from_main, dst_code_from_main, srcToDestSequenceList, INTERCHANGE, searchMetroList.this);
                            recyclerView.setHasFixedSize(true);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(searchMetroList.this);
                            recyclerView.setLayoutManager(layoutManager);

                            recyclerView.setAdapter(Adapter);
                        }
                    }

                    if (radioFullDay.isChecked()) {
                        if (srcDestTimeArrayList_twelve_hr_format.size() == 0) {
                            no_trains_available.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                            no_trains_available.setVisibility(View.GONE);
                            Adapter = new time_fragment_recycler_adapter(srcDestTimeArrayList_twelve_hr_format, lang, src_id_from_main, dst_id_from_main,
                                    src_code_from_main, dst_code_from_main, srcToDestSequenceList, INTERCHANGE, searchMetroList.this);
                            recyclerView.setHasFixedSize(true);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(searchMetroList.this);
                            recyclerView.setLayoutManager(layoutManager);

                            recyclerView.setAdapter(Adapter);
                        }
                    }
                }
            });

            if (srcDestTimeArrayList_twelve_hr_format_now_onwards.size() == 0) {
                no_trains_available.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                no_trains_available.setVisibility(View.GONE);
                Adapter = new time_fragment_recycler_adapter(srcDestTimeArrayList_twelve_hr_format_now_onwards, lang, src_id_from_main, dst_id_from_main,
                        src_code_from_main, dst_code_from_main, srcToDestSequenceList, INTERCHANGE, searchMetroList.this);
                recyclerView.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(searchMetroList.this);
                recyclerView.setLayoutManager(layoutManager);

                recyclerView.setAdapter(Adapter);
            }
//        System.out.println(" time from : " + time_from);
//        System.out.println(" time to : " + time_to);

            if (srcDestTimeArrayList_twelve_hr_format.size() == 0) {
                min = 0;
            } else {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
                try {
                    date1 = simpleDateFormat.parse(time_from);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    date2 = simpleDateFormat.parse(time_to);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                try {
                    long difference = date2.getTime() - date1.getTime();
                    days = (int) (difference / (1000 * 60 * 60 * 24));
                    hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
                    min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                    if (hours > 0) {
                        min = 60 + min;
                    }
                } catch (Exception ex) {
                    min = 0;
                }

//            System.out.println("hr days " + days);
//            System.out.println("hr hours " + hours);
//            System.out.println("hr min " + min);
            }

            if (srcToDesFareDetails.size() == 0) {
                km_value_txtview.setText("0");
                rs_value_txtview.setText("0");
                min_value_txtview.setText("0");
                station_value_txtview.setText("0");
                changes_value_txtview.setText("0");
            } else {
                //setting up fare details from source to destination
                km_value_txtview.setText(String.format("%.2f", Double.valueOf(srcToDesFareDetails.get(0).get(5))));
                rs_value_txtview.setText(srcToDesFareDetails.get(0).get(3));
                min_value_txtview.setText(String.valueOf(min));
                station_value_txtview.setText(srcToDesFareDetails.get(0).get(6));
                changes_value_txtview.setText(String.valueOf(INTERCHANGE));
            }


        }
        catch (Exception exception){

            src_from_main = getIntent().getStringExtra("src");
            src_id_from_main = getIntent().getStringExtra("srcid");
            src_code_from_main = getIntent().getStringExtra("src_code");
            dst_from_main = getIntent().getStringExtra("dst");
            dst_id_from_main = getIntent().getStringExtra("dstid");
            dst_code_from_main = getIntent().getStringExtra("dst_code");
            type = getIntent().getStringExtra("type");

            if(src_code_from_main!=null&&src_id_from_main!=null&&dst_from_main!=null&&dst_id_from_main!=null&&dst_code_from_main!=null&&type!=null){
                CrashesHandler crashesHandler = new CrashesHandler();
                crashesHandler.logging_to_firebase(searchMetroList.this,exception,"SEARCH METRO LIST 435 "+"  "+src_code_from_main+src_id_from_main+"  "+src_code_from_main+"  "+dst_from_main+"  "+dst_id_from_main+"  "+dst_code_from_main+"  "+type,"searchMetroList",src_from_main,dst_from_main);

            }
            else {
                CrashesHandler crashesHandler = new CrashesHandler();
                crashesHandler.logging_to_firebase(searchMetroList.this,exception,"SEARCH METRO LIST 435 SOME THING IS NULL ","searchMetroList",src_from_main,dst_from_main);
            }


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item1, menu);

        MenuItem routeMapMenu = menu.findItem(R.id.route_map_icon);
        ImageView iv = new ImageView(this);
        iv.setBackground(getResources().getDrawable(R.drawable.location_drawable_white));
        Animation animation = new AlphaAnimation((float) 2, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        iv.startAnimation(animation);
        routeMapMenu.setActionView(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(searchMetroList.this, routeMapWebview.class);
                startActivity(intent);
            }
        });
        return true;
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}
