package com.example.kolkatametroapp.asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Decompress extends AsyncTask<String, String, String> {
    public static final String MyPREFERENCES = "LangaugePref";
    ProgressDialog progressDialog;
    Activity activity;
    String FileN = null;
    SharedPreferences preferences;
    String ServerVersion;
    String DB_PATH = "";

    public Decompress(Activity activity, String ServerVersion) {
        this.activity = activity;
        this.ServerVersion = ServerVersion;
        preferences = this.activity.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Shows Progress Bar Dialog and then call doInBackground method
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... f_url) {
        try {
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                DB_PATH = activity.getApplicationInfo().dataDir + "/kolkata/";
            } else {
                DB_PATH = activity.getApplicationInfo().dataDir + "/kolkata/";
            }
            File kolkataDir = new File(DB_PATH);
            kolkataDir.mkdirs();
            FileN = "DataDownloader_" + UUID.randomUUID().toString() + ".zip";
            downloadZipFile(f_url[0], DB_PATH + FileN);
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }
        return null;
    }

    //while downloading file
    protected void onProgressUpdate(String... progress) {
        // Set progress percentage
        progressDialog.setProgress(Integer.parseInt(progress[0]));
    }

    // Once Music File is downloaded
    @Override
    protected void onPostExecute(String file_url) {
        Toast.makeText(activity, "Timetable updated", Toast.LENGTH_LONG).show();
    }

    public void downloadZipFile(String urlStr, String destinationFilePath) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlStr);

            connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDefaultUseCaches(false);
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.d("downloadZipFile", "Server ResponseCode=" + connection.getResponseCode() + " ResponseMessage=" + connection.getResponseMessage());
            }

            // download the file
            input = connection.getInputStream();

            Log.d("downloadZipFile", "destinationFilePath=" + destinationFilePath);

            new File(destinationFilePath).createNewFile();
            output = new FileOutputStream(destinationFilePath);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            System.out.println("xXxX: downloadZipFile Exception " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (output != null) output.close();
                if (input != null) input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (connection != null) connection.disconnect();
        }

        File f = new File(destinationFilePath);

        Log.d("downloadZipFile", "f.getParentFile().getPath()=" + f.getParentFile().getPath());
        Log.d("downloadZipFile", "f.getName()=" + f.getName().replace(".zip", ""));

        boolean unpack = unpackZip(destinationFilePath);
        if (unpack) {
//            System.out.println("xXxX: file unzipped");
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("DBVersion", ServerVersion);
            editor.commit();
            File f1 = new File(destinationFilePath);
            boolean deleted = f1.delete();
            if (deleted) {
//                System.out.println("xXxX: zip deleted");
            } else {
//                System.out.println("xXxX: zip not deleted");
            }
        } else {
//            System.out.println("xXxX: file not unzipped");
        }
    }

    public boolean unpackZip(String filePath) {
        InputStream is;
        ZipInputStream zis;
        try {
            String filename;

            is = new FileInputStream(filePath);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                filename = ze.getName();

                if (ze.isDirectory()) {
                    File fmd = new File(activity.getApplicationInfo().dataDir + "/databases/" + filename);
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(activity.getApplicationInfo().dataDir + "/databases/" + filename);

                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
