package com.example.kolkatametroapp;

public class Constants {



    public static String Firebase_device_error = "device_error";
    public static String Firebase_type = "Type";

    public static String Firebase_description = "Description";
    public static String Firebase_lang = "Language";
    public static String Firebase_location = "Location";

    public static String Firebase_error = "error";
    public static String Firebase_station_a = "station_A";

    public static String Firebase_station_b = "station_b";
}
