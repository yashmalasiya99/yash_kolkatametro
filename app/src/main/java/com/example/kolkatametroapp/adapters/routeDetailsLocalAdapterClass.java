package com.example.kolkatametroapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;

import java.util.ArrayList;

public class routeDetailsLocalAdapterClass extends RecyclerView.Adapter<routeDetailsLocalAdapterClass.viewholder> {

    ArrayList<ArrayList<String>> list;
    Context context;
    String lang;
    String source, destination;
    int rt_id_src, rt_id_dst;
    String currStn;

    public routeDetailsLocalAdapterClass(ArrayList<ArrayList<String>> list, String lang, String Source, String destination, int rt_id_src, int rt_id_dst, Context context, String currStn) {
        this.list = list;
        this.context = context;
        this.lang = lang;
        this.source = Source;
        this.destination = destination;
        this.rt_id_src = rt_id_src;
        this.rt_id_dst = rt_id_dst;
        this.currStn = currStn;
    }

    @NonNull
    @Override
    public routeDetailsLocalAdapterClass.viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.route_deatils_adapter_local_layout, parent, false);
        return new viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull routeDetailsLocalAdapterClass.viewholder holder, int position) {
        if (Integer.valueOf(list.get(position).get(6)) <= rt_id_dst && Integer.valueOf(list.get(position).get(6)) >= rt_id_src) {
            holder.arrival_time.setText(list.get(position).get(0));
            holder.arrival_time.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.arrival_time.setTypeface(null, Typeface.BOLD);
            holder.departure_time.setText(list.get(position).get(1));
            holder.departure_time.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.departure_time.setTypeface(null, Typeface.BOLD);
            holder.station_name.setText(list.get(position).get(3));
            holder.station_name.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.station_name.setTypeface(null, Typeface.BOLD);
        } else {
            holder.arrival_time.setText(list.get(position).get(0));
            holder.arrival_time.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));
            holder.arrival_time.setTypeface(null, Typeface.NORMAL);
            holder.departure_time.setText(list.get(position).get(1));
            holder.departure_time.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));
            holder.departure_time.setTypeface(null, Typeface.NORMAL);
            holder.station_name.setText(list.get(position).get(3));
            holder.station_name.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));
            holder.station_name.setTypeface(null, Typeface.NORMAL);
        }

        if (position == 0) {
            if (list.get(position).get(3).equals(source)) {
                holder.stepper_icon.setImageResource(R.drawable.route_start1_src);
            } else if (list.get(position).get(3).equals(destination)) {
                holder.stepper_icon.setImageResource(R.drawable.route_start1_dst);
            } else {
                if (Integer.valueOf(list.get(position).get(6)) < rt_id_dst && Integer.valueOf(list.get(position).get(6)) > rt_id_src) {
                    holder.stepper_icon.setImageResource(R.drawable.route_start1);
                } else {
                    holder.stepper_icon.setImageResource(R.drawable.route_start1_light_blue);
                }
            }
        } else if (position == list.size() - 1) {
            if (list.get(position).get(3).equals(source)) {
                holder.stepper_icon.setImageResource(R.drawable.route_end1_src);
            } else if (list.get(position).get(3).equals(destination)) {
                holder.stepper_icon.setImageResource(R.drawable.route_end1_dst);
            } else {
                if (Integer.valueOf(list.get(position).get(6)) < rt_id_dst && Integer.valueOf(list.get(position).get(6)) > rt_id_src) {
                    holder.stepper_icon.setImageResource(R.drawable.route_end1);
                } else {
                    holder.stepper_icon.setImageResource(R.drawable.route_end1_light_blue);
                }
            }
        } else {
            if (list.get(position).get(3).equals(source)) {
                holder.stepper_icon.setImageResource(R.drawable.route_mid1_src);
            } else if (list.get(position).get(3).equals(destination)) {
                holder.stepper_icon.setImageResource(R.drawable.route_mid1_dst);
            } else {
                if (Integer.valueOf(list.get(position).get(6)) < rt_id_dst && Integer.valueOf(list.get(position).get(6)) > rt_id_src) {
                    holder.stepper_icon.setImageResource(R.drawable.route_mid1);
                } else {
                    holder.stepper_icon.setImageResource(R.drawable.route_mid1_light_blue);
                }
            }
        }

        if (list.get(position).get(2).trim().equalsIgnoreCase(currStn)) {
            holder.current_station_pointer.setVisibility(View.VISIBLE);
        } else {
            holder.current_station_pointer.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setCurrentStation(String curStn) {
        this.currStn = curStn;

        notifyDataSetChanged();
    }

    public static class viewholder extends RecyclerView.ViewHolder {
        public TextView station_name, arrival_time, departure_time;
        public ImageView stepper_icon, current_station_pointer;

        public viewholder(View view) {
            super(view);
            station_name = (TextView) view.findViewById(R.id.stepper_station_txt_local_route_details);
            stepper_icon = (ImageView) view.findViewById(R.id.stepper_img_local_route_details);
            current_station_pointer = (ImageView) view.findViewById(R.id.current_station_pointer);
            arrival_time = (TextView) view.findViewById(R.id.arrival_local_route_details);
            departure_time = (TextView) view.findViewById(R.id.departure_local_route_details);
        }
    }
}