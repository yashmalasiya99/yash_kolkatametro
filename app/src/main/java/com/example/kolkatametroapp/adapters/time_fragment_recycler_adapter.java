package com.example.kolkatametroapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;
import com.example.kolkatametroapp.database.DataAdapter;
import com.example.kolkatametroapp.routeDetails;
import com.example.kolkatametroapp.searchMetroList;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;

import java.util.ArrayList;

public class time_fragment_recycler_adapter extends RecyclerView.Adapter<time_fragment_recycler_adapter.viewholder> {

    // A menu item view type.
    private static final int TYPE_CONTENT = 0;
    // The banner ad view type.
    private static final int TYPE_AD = 1;
    ArrayList<ArrayList<String>> list;
    Context context;
    DataAdapter dbAdapter;
    ArrayList<String> srcStationData;
    ArrayList<String> dstStationData;
    String lang;
    String src_id_from_main, dst_id_from_main, src_code_from_main, dst_code_from_main;
    ArrayList<Integer> srcToDestSequenceList;
    int INTERCHANGE;

    public time_fragment_recycler_adapter(ArrayList<ArrayList<String>> list, String lang, String src_id_from_main, String dst_id_from_main,
                                          String src_code_from_main, String dst_code_from_main, ArrayList<Integer> srcToDestSequenceList,
                                          int INTERCHANGE, Context context) {
        this.list = list;
        this.context = context;
        this.lang = lang;
        this.src_id_from_main = src_id_from_main;
        this.dst_id_from_main = dst_id_from_main;
        this.src_code_from_main = src_code_from_main;
        this.dst_code_from_main = dst_code_from_main;
        this.srcToDestSequenceList = srcToDestSequenceList;
        this.INTERCHANGE = INTERCHANGE;

        dbAdapter = new DataAdapter(context);
        dbAdapter.createDatabase();
        dbAdapter.open();
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_fragment_recycler_layout, parent, false);
            return new viewholder(itemview);
        } else {
            View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_view_layout, parent, false);
            return new viewholder(itemview);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, @SuppressLint("RecyclerView") int position) {
        if (getItemViewType(position) == TYPE_CONTENT) {
            // show recycler content
            holder.train_id.setText(list.get(position).get(3));
            if (list.get(position).get(4).equals("KDSW") && list.get(position).get(5).equals("KKVS")) {
                holder.start_end_txt.setText(context.getResources().getString(R.string.dak_kavi));
            } else if (list.get(position).get(4).equals("KDMI") && list.get(position).get(5).equals("KKVS")) {
                holder.start_end_txt.setText(context.getResources().getString(R.string.dum_kavi));
            } else if (list.get(position).get(4).equals("KKVS") && list.get(position).get(5).equals("KDSW")) {
                holder.start_end_txt.setText(context.getResources().getString(R.string.kavi_dak));
            } else if (list.get(position).get(4).equals("KKVS") && list.get(position).get(5).equals("KDMI")) {
                holder.start_end_txt.setText(context.getResources().getString(R.string.kavi_dum));
            } else if (list.get(position).get(4).equals("SVSA") && list.get(position).get(5).equals("PBSA")) {
                holder.start_end_txt.setText(context.getResources().getString(R.string.saltlake_phoolbagan));
            } else {
                holder.start_end_txt.setText(context.getResources().getString(R.string.phoolbagan_saltlake));
            }
            holder.from_time.setText(list.get(position).get(6));
            holder.to_time.setText(list.get(position).get(7));

            srcStationData = dbAdapter.getstationNamefromStationCode(list.get(position).get(1));
            dstStationData = dbAdapter.getstationNamefromStationCode(list.get(position).get(2));

            if (lang.equals("en")) {
                holder.from_station.setText(srcStationData.get(1));
            } else if (lang.equals("bn")) {
                holder.from_station.setText(srcStationData.get(3));
            } else {
                holder.from_station.setText(srcStationData.get(1));
            }
            if (lang.equals("en")) {
                holder.to_station.setText(dstStationData.get(1));
            } else if (lang.equals("bn")) {
                holder.to_station.setText(dstStationData.get(3));
            } else {
                holder.to_station.setText(dstStationData.get(1));
            }

            holder.train_timing_crd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    try {


                    Intent i = new Intent(context, routeDetails.class);
                    i.putExtra("src_id", src_id_from_main);
                    i.putExtra("dst_id", dst_id_from_main);
                    i.putExtra("src_code", src_code_from_main);
                    i.putExtra("dst_code", dst_code_from_main);
                    i.putIntegerArrayListExtra("SequenceList", srcToDestSequenceList);
                    i.putExtra("change", INTERCHANGE);
                    i.putExtra("train_id", list.get(position).get(3));
                    i.putExtra("train_name", holder.start_end_txt.getText().toString());
                    context.startActivity(i); }
                    catch (Exception e){
                        System.out.println(e);
                    }
                }
            });
        } else if (getItemViewType(position) == TYPE_AD) {
            //show ads content
            final AdLoader adLoader = new AdLoader.Builder(context, "ca-app-pub-8402324029883658/5703983282")
                    .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                        @Override
                        public void onNativeAdLoaded(NativeAd NativeAd) {
                            // Show the ad.
                            NativeTemplateStyle styles = new
                                    NativeTemplateStyle.Builder().build();

                            TemplateView template = holder.Adtemplate;
                            template.setStyles(styles);
                            template.setNativeAd(NativeAd);
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            // Handle the failure by logging, altering the UI, and so on.
                            holder.Adtemplate.setVisibility(View.GONE);
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
        } else {
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_CONTENT;
        } else if (position % ((searchMetroList) context).ITEMS_PER_AD == 0) {
            return TYPE_AD;
        } else {
            return TYPE_CONTENT;
        }
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView train_id, start_end_txt, from_time, from_station, to_time, to_station;
        public CardView train_timing_crd;
        public TemplateView Adtemplate;

        public viewholder(View view) {
            super(view);
            train_id = (TextView) view.findViewById(R.id.train_id);
            start_end_txt = (TextView) view.findViewById(R.id.start_end_txt);
            from_time = (TextView) view.findViewById(R.id.from_time);
            from_station = (TextView) view.findViewById(R.id.from_station);
            to_time = (TextView) view.findViewById(R.id.to_time);
            to_station = (TextView) view.findViewById(R.id.to_station);
            train_timing_crd = (CardView) view.findViewById(R.id.train_timing_crd);
            Adtemplate = view.findViewById(R.id.my_template);
        }
    }
}