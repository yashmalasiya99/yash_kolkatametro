package com.example.kolkatametroapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;

import java.util.ArrayList;

public class route_fragment_stepper_adapter_class extends RecyclerView.Adapter<route_fragment_stepper_adapter_class.viewholder> {
    ArrayList<ArrayList<String>> stationlistarray;
    int metroLine;
    Context context;
    String lang;

    public route_fragment_stepper_adapter_class(ArrayList<ArrayList<String>> stationArray, int metroLine, String lang, Context context) {
        this.stationlistarray = stationArray;
        this.metroLine = metroLine;
        this.context = context;
        this.lang = lang;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_route_stepper_adapter_layout, parent, false);
        return new viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        if (metroLine == 1) {
            if (lang.equals("en")) {
                holder.station_name.setText(stationlistarray.get(position).get(1));
            } else if (lang.equals("bn")) {
                holder.station_name.setText(stationlistarray.get(position).get(3));
            } else {
                holder.station_name.setText(stationlistarray.get(position).get(1));
            }

            if (position == 0) {
                holder.stepper_icon.setImageResource(R.drawable.route_start1_src);
            } else if (position == stationlistarray.size() - 1) {
                holder.stepper_icon.setImageResource(R.drawable.route_end1_dst);
            } else {
                holder.stepper_icon.setImageResource(R.drawable.route_mid1);
            }
        } else {
            if (lang.equals("en")) {
                holder.station_name.setText(stationlistarray.get(position).get(1));
            } else if (lang.equals("bn")) {
                holder.station_name.setText(stationlistarray.get(position).get(3));
            } else {
                holder.station_name.setText(stationlistarray.get(position).get(1));
            }

            if (position == 0) {
                holder.stepper_icon.setImageResource(R.drawable.blue_start);
            } else if (position == stationlistarray.size() - 1) {
                holder.stepper_icon.setImageResource(R.drawable.blue_end);
            } else {
                holder.stepper_icon.setImageResource(R.drawable.route_mid1_blue);
            }
        }
    }

    @Override
    public int getItemCount() {
        return stationlistarray.size();
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView station_name;
        public ImageView stepper_icon;

        public viewholder(View view) {
            super(view);
            station_name = (TextView) view.findViewById(R.id.stepper_station_txt);
            stepper_icon = (ImageView) view.findViewById(R.id.stepper_img);
        }
    }
}