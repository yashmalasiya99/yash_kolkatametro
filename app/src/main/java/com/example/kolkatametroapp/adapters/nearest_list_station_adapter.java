package com.example.kolkatametroapp.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;

import java.util.ArrayList;

public class nearest_list_station_adapter extends RecyclerView.Adapter<nearest_list_station_adapter.viewholder> {

    ArrayList<ArrayList<String>> list;
    Context context;
    Double range;

    public nearest_list_station_adapter(ArrayList<ArrayList<String>> list, Double range, Context context) {
        this.list = list;
        this.context = context;
        this.range = range;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.nearest_station_list_recycler_layout, parent, false);
        return new viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        if (Double.valueOf(list.get(position).get(3)) > 0.0 && Double.valueOf(list.get(position).get(3)) <= range) {
            holder.stationName.setText(list.get(position).get(0));
            holder.stationName.setTypeface(Typeface.DEFAULT_BOLD);
            holder.stationName.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.km.setText(list.get(position).get(3) + " KMS");
            holder.km.setTypeface(Typeface.DEFAULT_BOLD);
            holder.km.setTextColor(ContextCompat.getColor(context, R.color.black));
        } else {
            holder.stationName.setText(list.get(position).get(0));
            holder.stationName.setTypeface(Typeface.DEFAULT);
            holder.stationName.setTextColor(ContextCompat.getColor(context, R.color.grey));
            holder.km.setText(list.get(position).get(3) + " KMS");
            holder.km.setTypeface(Typeface.DEFAULT);
            holder.km.setTextColor(ContextCompat.getColor(context, R.color.grey));

        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView stationName, km;

        public viewholder(View view) {
            super(view);
            stationName = (TextView) view.findViewById(R.id.station_name_txt_nearest);
            km = (TextView) view.findViewById(R.id.KM_display_txt);
        }
    }
}