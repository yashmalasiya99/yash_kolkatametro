package com.example.kolkatametroapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;

import java.util.ArrayList;

public class src_list_adapter_class_ew extends RecyclerView.Adapter<src_list_adapter_class_ew.viewholder> {
    ArrayList<ArrayList<String>> filterlist;
    ArrayList<ArrayList<String>> list;
    Context context;
    int count;
    String lang;

    public src_list_adapter_class_ew(ArrayList<ArrayList<String>> list, String lang, Context context) {

        this.list = list;
        this.context = context;
        this.filterlist = new ArrayList<>();
        this.filterlist.addAll(list);
        this.count = list.size();
        this.lang = lang;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.src_list_recycler_layout_ew, parent, false);
        return new viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        if (lang.equals("en")) {
            holder.src_name.setText(list.get(position).get(1));
        } else if (lang.equals("bn")) {
            holder.src_name.setText(list.get(position).get(3));
        } else {
            holder.src_name.setText(list.get(position).get(1));
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filter(CharSequence charText) {
        String filterString = charText.toString().toLowerCase();
        list.clear();
        if (charText.length() == 0) {
            list.addAll(filterlist);
        } else {
            String filterableString;
            for (int i = 0; i < count; i++) {
                filterableString = filterlist.get(i).get(1);
                if (filterableString.toLowerCase().contains(filterString)) {
                    ArrayList<String> listdata = new ArrayList<>();
                    listdata.add(filterlist.get(i).get(0));
                    listdata.add(filterableString);
                    listdata.add(filterlist.get(i).get(2));
                    listdata.add(filterlist.get(i).get(3));

                    list.add(listdata);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView src_name;

        public viewholder(View view) {
            super(view);
            src_name = (TextView) view.findViewById(R.id.txt_list_srch_est_wst);
        }
    }
}
