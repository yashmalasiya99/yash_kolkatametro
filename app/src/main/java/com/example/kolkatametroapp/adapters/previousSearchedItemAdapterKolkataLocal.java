package com.example.kolkatametroapp.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;
import com.example.kolkatametroapp.database.previous_search_database_local;
import com.example.kolkatametroapp.kolkata_local_find_route_class;
import com.example.kolkatametroapp.searchedKolkataLocalList;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import java.util.ArrayList;

public class previousSearchedItemAdapterKolkataLocal extends RecyclerView.Adapter<previousSearchedItemAdapterKolkataLocal.viewholder> {

    ArrayList<ArrayList<String>> list;
    Activity context;
    previous_search_database_local databaseClass;
    String src, dst, srcId, dstId, srcKannada, dstKannada, src_code, dst_code;
    String lang;
    private InterstitialAd mInterstitialAd;

    public previousSearchedItemAdapterKolkataLocal(ArrayList<ArrayList<String>> list, String lang, Activity context) {
        this.list = list;
        this.context = context;
        this.lang = lang;

        databaseClass = new previous_search_database_local(context);
    }

    @NonNull
    @Override
    public previousSearchedItemAdapterKolkataLocal.viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.previous_search_item_adapter_kolkata_local, parent, false);
        AdRequest adRequest1 = new AdRequest.Builder().build();

        InterstitialAd.load(context, context.getResources().getString(R.string.inter), adRequest1,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        mInterstitialAd = null;
                    }
                });

        return new previousSearchedItemAdapterKolkataLocal.viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull previousSearchedItemAdapterKolkataLocal.viewholder holder, int position) {

        holder.img_src.setImageResource(R.drawable.route_start1_blue);
//        if (lang.equals("en")) {
//            holder.src_txt.setText(list.get(position).get(2));//src
//        } else if (lang.equals("bn")) {
//            holder.src_txt.setText(list.get(position).get(7));//srckannada
//        } else {
//            holder.src_txt.setText(list.get(position).get(2));//src
//        }
        holder.src_txt.setText(list.get(position).get(2)); //srcName
        holder.img_dst.setImageResource(R.drawable.route_end1_blue);
//        if (lang.equals("en")) {
//            holder.dst_txt.setText(list.get(position).get(5));//dst
//        } else if (lang.equals("bn")) {
//            holder.dst_txt.setText(list.get(position).get(8));//dstkannada
//        } else {
//            holder.dst_txt.setText(list.get(position).get(5));//dst
//        }
        holder.dst_txt.setText(list.get(position).get(5));//dstName

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = list.get(position).get(0);
                boolean deleted = databaseClass.delete(id);
                if (deleted) {
                    list.clear();
                    list = databaseClass.getdata();
                    if (list.size() == 0) {
                        ((kolkata_local_find_route_class) context).remove_saved_visiblity();
                    }
                    previousSearchedItemAdapterKolkataLocal.this.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "item not deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                srcId = list.get(position).get(1);
                src = list.get(position).get(2);
                src_code = list.get(position).get(3);
                dstId = list.get(position).get(4);
                dst = list.get(position).get(5);
                dst_code = list.get(position).get(6);
                srcKannada = list.get(position).get(7);
                dstKannada = list.get(position).get(8);
                String valueOfSearchedItemExistOrNot = databaseClass.searchItemAlreadyExist(srcId, dstId);
                if (!valueOfSearchedItemExistOrNot.equals("")) {  //if searching item is already present in database (check)
                    databaseClass.delete(valueOfSearchedItemExistOrNot); //delete the item from database
                }
                databaseClass.adddata(srcId, src, src_code, dstId, dst, dst_code, srcKannada, dstKannada);
                Intent i = new Intent(context, searchedKolkataLocalList.class);
//                if (lang.equals("en")) {
//                    i.putExtra("src", src);
//                } else if (lang.equals("bn")) {
//                    i.putExtra("src", srcKannada);
//                } else {
//                    i.putExtra("src", src);
//                }
                i.putExtra("src", src);
                i.putExtra("srcid", srcId);
                i.putExtra("src_code", src_code);
//                if (lang.equals("en")) {
//                    i.putExtra("dst", dst);
//                } else if (lang.equals("bn")) {
//                    i.putExtra("dst", dstKannada);
//                } else {
//                    i.putExtra("dst", dst);
//                }
                i.putExtra("dst", dst);
                i.putExtra("dstid", dstId);
                i.putExtra("dst_code", dst_code);
                i.putExtra("type", "local");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

                if (mInterstitialAd != null) {
                    mInterstitialAd.show(context);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView src_txt, dst_txt;
        public ImageView delete, img_src, img_dst;
        CardView card;

        public viewholder(View view) {
            super(view);
            src_txt = view.findViewById(R.id.src_station_txt_previous_search_local);
            dst_txt = view.findViewById(R.id.dst_station_txt_previous_search_local);
            img_src = view.findViewById(R.id.src_stepper_icon_previous_search_local);
            img_dst = view.findViewById(R.id.dst_stepper_icon_previous_search_local);
            delete = view.findViewById(R.id.image_delete_previous_search_local);
            card = view.findViewById(R.id.card_click_previous_search_local);

        }
    }
}
