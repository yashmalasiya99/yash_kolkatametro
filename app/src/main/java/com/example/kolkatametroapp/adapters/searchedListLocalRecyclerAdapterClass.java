package com.example.kolkatametroapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;
import com.example.kolkatametroapp.routeDetailsKolkataLocalClass;
import com.example.kolkatametroapp.searchedKolkataLocalList;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class searchedListLocalRecyclerAdapterClass extends RecyclerView.Adapter<searchedListLocalRecyclerAdapterClass.viewholder> {

    // A menu item view type.
    private static final int TYPE_CONTENT = 0;
    // The banner ad view type.
    private static final int TYPE_AD = 1;
    ArrayList<ArrayList<String>> list;
    Context context;
    String lang;
    String src_txt;
    String dst_txt;
    Date date1, date2;
    int days, hours, min;

    public searchedListLocalRecyclerAdapterClass(ArrayList<ArrayList<String>> list, String lang, String src, String dst, Context context) {
        this.list = list;
        this.context = context;
        this.lang = lang;
        this.src_txt = src;
        this.dst_txt = dst;
    }

    @NonNull
    @Override
    public searchedListLocalRecyclerAdapterClass.viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.searched_list_local_recycler_layout, parent, false);
            return new searchedListLocalRecyclerAdapterClass.viewholder(itemview);
        } else {
            View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_view_layout, parent, false);
            return new searchedListLocalRecyclerAdapterClass.viewholder(itemview);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull searchedListLocalRecyclerAdapterClass.viewholder holder, @SuppressLint("RecyclerView") int position) {
        if (getItemViewType(position) == TYPE_CONTENT) {
            // show recycler content
            holder.train_id.setText(list.get(position).get(4));
            holder.start_end_txt.setText(list.get(position).get(5));
            holder.from_time.setText(list.get(position).get(1));
            holder.to_time.setText(list.get(position).get(6));
            holder.from_station.setText(src_txt);
            holder.to_station.setText(dst_txt);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
            try {
                date1 = simpleDateFormat.parse(list.get(position).get(1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                date2 = simpleDateFormat.parse(list.get(position).get(6));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long difference = date2.getTime() - date1.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) (difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60);
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

            if (hours < 0) {
                hours = hours + 23;
            }
            if (min < 0) {
                min = 60 + min;
            }

            if (hours == 0) {
                holder.difference_time.setText(String.valueOf(min) + " min");
            } else if (min == 0) {
                holder.difference_time.setText(String.valueOf(hours) + " hr");
            } else {
                holder.difference_time.setText(String.valueOf(hours) + " hr " + String.valueOf(min) + " min");
            }

            String running_days = list.get(position).get(9);

//            System.out.println(position + " xXxX: running_days: " + running_days);

            if (running_days == null || running_days.trim().isEmpty()) {
                holder.week_mon.setTextColor(context.getResources().getColor(R.color.grey));
                holder.week_tue.setTextColor(context.getResources().getColor(R.color.grey));
                holder.week_wed.setTextColor(context.getResources().getColor(R.color.grey));
                holder.week_thu.setTextColor(context.getResources().getColor(R.color.grey));
                holder.week_fri.setTextColor(context.getResources().getColor(R.color.grey));
                holder.week_sat.setTextColor(context.getResources().getColor(R.color.grey));
                holder.week_sun.setTextColor(context.getResources().getColor(R.color.grey));
            } else {
                String rdb = Integer.toBinaryString(Integer.parseInt(running_days));

                if (rdb.trim().length() > 7) {
                    holder.week_mon.setTextColor(context.getResources().getColor(R.color.grey));
                    holder.week_tue.setTextColor(context.getResources().getColor(R.color.grey));
                    holder.week_wed.setTextColor(context.getResources().getColor(R.color.grey));
                    holder.week_thu.setTextColor(context.getResources().getColor(R.color.grey));
                    holder.week_fri.setTextColor(context.getResources().getColor(R.color.grey));
                    holder.week_sat.setTextColor(context.getResources().getColor(R.color.grey));
                    holder.week_sun.setTextColor(context.getResources().getColor(R.color.grey));
                } else {
                    if (rdb.trim().length() < 7) {
                        int remaining = 7 - rdb.trim().length();

                        String new_rdb = "";

                        for (int i = 0; i < remaining; i++) {
                            new_rdb = new_rdb + "0";
                        }

                        rdb = new_rdb + rdb;
                    }

                    String[] runnindDays = rdb.split("");

                    if (runnindDays[0].equals("1")) {
                        holder.week_mon.setTextColor(context.getResources().getColor(R.color.purple_700));
                    } else {
                        holder.week_mon.setTextColor(context.getResources().getColor(R.color.grey));
                    }
                    if (runnindDays[1].equals("1")) {
                        holder.week_tue.setTextColor(context.getResources().getColor(R.color.purple_700));
                    } else {
                        holder.week_tue.setTextColor(context.getResources().getColor(R.color.grey));
                    }
                    if (runnindDays[2].equals("1")) {
                        holder.week_wed.setTextColor(context.getResources().getColor(R.color.purple_700));
                    } else {
                        holder.week_wed.setTextColor(context.getResources().getColor(R.color.grey));
                    }
                    if (runnindDays[3].equals("1")) {
                        holder.week_thu.setTextColor(context.getResources().getColor(R.color.purple_700));
                    } else {
                        holder.week_thu.setTextColor(context.getResources().getColor(R.color.grey));
                    }
                    if (runnindDays[4].equals("1")) {
                        holder.week_fri.setTextColor(context.getResources().getColor(R.color.purple_700));
                    } else {
                        holder.week_fri.setTextColor(context.getResources().getColor(R.color.grey));
                    }
                    if (runnindDays[5].equals("1")) {
                        holder.week_sat.setTextColor(context.getResources().getColor(R.color.purple_700));
                    } else {
                        holder.week_sat.setTextColor(context.getResources().getColor(R.color.grey));
                    }
                    if (runnindDays[6].equals("1")) {
                        holder.week_sun.setTextColor(context.getResources().getColor(R.color.purple_700));
                    } else {
                        holder.week_sun.setTextColor(context.getResources().getColor(R.color.grey));
                    }
                }
            }

            holder.train_timing_crd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, routeDetailsKolkataLocalClass.class);
                    i.putExtra("short_name", list.get(position).get(4));
                    i.putExtra("long_name", list.get(position).get(5));
                    i.putExtra("trip_id", list.get(position).get(0));
                    i.putExtra("cal_id", list.get(position).get(3));
                    i.putExtra("src", src_txt);
                    i.putExtra("dst", dst_txt);
                    context.startActivity(i);
                }
            });

        } else if (getItemViewType(position) == TYPE_AD) {
            //show ads content
            final AdLoader adLoader = new AdLoader.Builder(context, "ca-app-pub-8402324029883658/5703983282")
                    .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                        @Override
                        public void onNativeAdLoaded(NativeAd NativeAd) {
                            // Show the ad.
                            NativeTemplateStyle styles = new
                                    NativeTemplateStyle.Builder().build();

                            TemplateView template = holder.Adtemplate;
                            template.setStyles(styles);
                            template.setNativeAd(NativeAd);
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            // Handle the failure by logging, altering the UI, and so on.
                            holder.Adtemplate.setVisibility(View.GONE);

                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
        } else {
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_CONTENT;
        } else if (position % ((searchedKolkataLocalList) context).ITEMS_PER_AD == 0) {
            return TYPE_AD;
        } else {
            return TYPE_CONTENT;
        }
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView train_id, start_end_txt, from_time, from_station, to_time, to_station, difference_time;
        public TextView week_mon, week_tue, week_wed, week_thu, week_fri, week_sat, week_sun;
        public CardView train_timing_crd;
        public TemplateView Adtemplate;

        public viewholder(View view) {
            super(view);
            train_id = view.findViewById(R.id.train_id_trains_local);
            start_end_txt = view.findViewById(R.id.start_end_txt_trains_local);
            from_time = view.findViewById(R.id.from_time_trains_local);
            from_station = view.findViewById(R.id.from_station_trains_local);
            to_time = view.findViewById(R.id.to_time_trains_local);
            to_station = view.findViewById(R.id.to_station_trains_local);
            difference_time = view.findViewById(R.id.diff_time_trains_local);

            week_mon = view.findViewById(R.id.week_mon);
            week_tue = view.findViewById(R.id.week_tue);
            week_wed = view.findViewById(R.id.week_wed);
            week_thu = view.findViewById(R.id.week_thu);
            week_fri = view.findViewById(R.id.week_fri);
            week_sat = view.findViewById(R.id.week_sat);
            week_sun = view.findViewById(R.id.week_sun);

            train_timing_crd = (CardView) view.findViewById(R.id.train_timing_crd_trains_local);

            Adtemplate = view.findViewById(R.id.my_template);
        }
    }
}
