package com.example.kolkatametroapp.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;
import com.example.kolkatametroapp.stationListInfo;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import java.util.ArrayList;

public class stationListAdapterClass extends RecyclerView.Adapter<stationListAdapterClass.viewholder> {
    ArrayList<ArrayList<String>> filterlist;
    ArrayList<ArrayList<String>> list;
    Activity activity;
    int count;
    String lang;
    private InterstitialAd mInterstitialAd;

    public stationListAdapterClass(ArrayList<ArrayList<String>> list, String lang, Activity activity) {
        this.list = list;
        this.activity = activity;
        this.filterlist = new ArrayList<>();
        this.filterlist.addAll(list);
        this.count = list.size();
        this.lang = lang;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.station_list_recycler_layout, parent, false);
        AdRequest adRequest1 = new AdRequest.Builder().build();

        InterstitialAd.load(activity, activity.getResources().getString(R.string.inter), adRequest1,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        mInterstitialAd = null;
                    }
                });
        return new viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        if (lang.equals("en")) {
            holder.src_name.setText(list.get(position).get(1));
        } else if (lang.equals("bn")) {
            holder.src_name.setText(list.get(position).get(3));
        } else {
            holder.src_name.setText(list.get(position).get(1));
        }

        if (list.get(position).get(4).equals("1")) {
            holder.line_image.setImageResource(R.drawable.purple_line);
        } else {
            holder.line_image.setImageResource(R.drawable.blue_line);
        }

        holder.items_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, stationListInfo.class);
                i.putExtra("stationid", list.get(position).get(0));
                i.putExtra("stationName", list.get(position).get(1));
                i.putExtra("stationCode", list.get(position).get(2));
                i.putExtra("bengali", list.get(position).get(3));
                i.putExtra("lineId", list.get(position).get(4));
                i.putExtra("landline", list.get(position).get(5));
                i.putExtra("mobile", list.get(position).get(6));
                i.putExtra("parking", list.get(position).get(7));
                i.putExtra("address", list.get(position).get(8));
                i.putExtra("entry", list.get(position).get(9));
                i.putExtra("exit", list.get(position).get(10));
                activity.startActivity(i);
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(activity);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filter(CharSequence charText) {
        String filterString = charText.toString().toLowerCase();
        list.clear();
        if (charText.length() == 0) {
            list.addAll(filterlist);
        } else {
            String filterableString;
            for (int i = 0; i < count; i++) {
                filterableString = filterlist.get(i).get(1);
                if (filterableString.toLowerCase().contains(filterString)) {
                    ArrayList<String> listdata = new ArrayList<>();
                    listdata.add(filterlist.get(i).get(0));//station id
                    listdata.add(filterableString);// station name
                    listdata.add(filterlist.get(i).get(2)); // station code
                    listdata.add(filterlist.get(i).get(3)); // station name bengali
                    listdata.add(filterlist.get(i).get(4));//line id
                    listdata.add(filterlist.get(i).get(5));//landline
                    listdata.add(filterlist.get(i).get(6));//mobile
                    listdata.add(filterlist.get(i).get(7));//parking
                    listdata.add(filterlist.get(i).get(8));//address
                    listdata.add(filterlist.get(i).get(9));//entrygates
                    listdata.add(filterlist.get(i).get(10));//exitgates

                    list.add(listdata);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView src_name;
        public CardView items_card;
        public ImageView line_image;

        public viewholder(View view) {
            super(view);
            src_name = (TextView) view.findViewById(R.id.txt_item);
            items_card = (CardView) view.findViewById(R.id.crd_items);
            line_image = (ImageView) view.findViewById(R.id.lineImage);
        }
    }
}