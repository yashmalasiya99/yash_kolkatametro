package com.example.kolkatametroapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;
import com.example.kolkatametroapp.database.DataAdapterBus;
import com.example.kolkatametroapp.routeDetailsKolkataBusClass;
import com.example.kolkatametroapp.searchedKolkataBusList;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;

import java.util.ArrayList;

public class searchedListBusRecyclerAdapterClass extends RecyclerView.Adapter<searchedListBusRecyclerAdapterClass.viewholder> {

    // A menu item view type.
    private static final int TYPE_CONTENT = 0;
    // The banner ad view type.
    private static final int TYPE_AD = 1;
    ArrayList<ArrayList<String>> list;
    Context context;
    String lang;
    String src_txt;
    String dst_txt;
    DataAdapterBus dbAdapter;
    int src_id, dst_id;
    ArrayList<ArrayList<String>> srcDstTextArray;

    public searchedListBusRecyclerAdapterClass(ArrayList<ArrayList<String>> list, String lang, String src, String dst, int src_id, int dst_id, Context context) {

        this.list = list;
        this.context = context;
        this.lang = lang;
        this.src_txt = src;
        this.dst_txt = dst;
        this.src_id = src_id;
        this.dst_id = dst_id;

        dbAdapter = new DataAdapterBus(context);
        dbAdapter.createDatabase();
        dbAdapter.open();
    }

    @NonNull
    @Override
    public searchedListBusRecyclerAdapterClass.viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_CONTENT) {
            View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.searched_kolkata_bus_recycler_adapter_layout, parent, false);
            return new searchedListBusRecyclerAdapterClass.viewholder(itemview);
        } else {
            View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_view_layout, parent, false);
            return new searchedListBusRecyclerAdapterClass.viewholder(itemview);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull searchedListBusRecyclerAdapterClass.viewholder holder, int position) {

        if (getItemViewType(position) == TYPE_CONTENT) {
            // show recycler content
            holder.train_id.setText(list.get(position).get(1));

            if (src_id < dst_id) {
                srcDstTextArray = dbAdapter.GetSrcDstRouteArray(Integer.valueOf(list.get(position).get(0)));
                if (lang.equals("en")) {
                    holder.start_end_txt.setText(srcDstTextArray.get(0).get(1) + " - " + srcDstTextArray.get(srcDstTextArray.size() - 1).get(1));
                } else if (lang.equals("bn")) {
                    holder.start_end_txt.setText(srcDstTextArray.get(0).get(2) + " - " + srcDstTextArray.get(srcDstTextArray.size() - 1).get(2));
                } else {
                    holder.start_end_txt.setText(srcDstTextArray.get(0).get(1) + " - " + srcDstTextArray.get(srcDstTextArray.size() - 1).get(1));
                }
            } else {
                srcDstTextArray = dbAdapter.GetSrcDstRouteArrayReverse(Integer.valueOf(list.get(position).get(0)));
                if (lang.equals("en")) {
                    holder.start_end_txt.setText(srcDstTextArray.get(0).get(1) + " - " + srcDstTextArray.get(srcDstTextArray.size() - 1).get(1));
                } else if (lang.equals("bn")) {
                    holder.start_end_txt.setText(srcDstTextArray.get(0).get(2) + " - " + srcDstTextArray.get(srcDstTextArray.size() - 1).get(2));
                } else {
                    holder.start_end_txt.setText(srcDstTextArray.get(0).get(1) + " - " + srcDstTextArray.get(srcDstTextArray.size() - 1).get(1));
                }
            }
            holder.from_station.setText(src_txt);
            holder.to_station.setText(dst_txt);

            if (list.get(position).get(2).equals("0")) {
                holder.bus_type.setText(context.getResources().getString(R.string.non_ac));
            } else {
                holder.bus_type.setText(context.getResources().getString(R.string.ac));
            }
            holder.bus_operator.setText(list.get(position).get(3));

            holder.train_timing_crd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, routeDetailsKolkataBusClass.class);
                    i.putExtra("short_name", list.get(position).get(1));
                    i.putExtra("long_name", holder.start_end_txt.getText().toString());
                    i.putExtra("bus_id", list.get(position).get(0));
                    i.putExtra("src", src_txt);
                    i.putExtra("dst", dst_txt);
                    i.putExtra("src_id", src_id);
                    i.putExtra("dst_id", dst_id);
                    context.startActivity(i);
                }
            });

        } else if (getItemViewType(position) == TYPE_AD) {
            //show ads content
            final AdLoader adLoader = new AdLoader.Builder(context, "ca-app-pub-8402324029883658/5703983282")
                    .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                        @Override
                        public void onNativeAdLoaded(NativeAd NativeAd) {
                            // Show the ad.
                            NativeTemplateStyle styles = new
                                    NativeTemplateStyle.Builder().build();

                            TemplateView template = holder.Adtemplate;
                            template.setStyles(styles);
                            template.setNativeAd(NativeAd);
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {
                            // Handle the failure by logging, altering the UI, and so on.
                            holder.Adtemplate.setVisibility(View.GONE);

                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());
        } else {

        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_CONTENT;
        } else if (position % ((searchedKolkataBusList) context).ITEMS_PER_AD == 0) {
            return TYPE_AD;
        } else {
            return TYPE_CONTENT;
        }
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView train_id, start_end_txt, from_station, to_station, bus_type, bus_operator;
        public CardView train_timing_crd;
        public TemplateView Adtemplate;

        public viewholder(View view) {
            super(view);
            train_id = (TextView) view.findViewById(R.id.train_id_trains_bus);
            start_end_txt = (TextView) view.findViewById(R.id.start_end_txt_trains_bus);
            from_station = (TextView) view.findViewById(R.id.from_station_kolkata_bus);
            to_station = (TextView) view.findViewById(R.id.to_station_kolkata_bus);
            train_timing_crd = (CardView) view.findViewById(R.id.train_timing_crd_trains_bus);
            bus_type = (TextView) view.findViewById(R.id.bus_type);
            bus_operator = (TextView) view.findViewById(R.id.bus_operator);
            Adtemplate = view.findViewById(R.id.my_template);
        }
    }
}