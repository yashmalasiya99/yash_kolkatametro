package com.example.kolkatametroapp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;

import java.util.ArrayList;

public class routeDetailBusAdapterClass extends RecyclerView.Adapter<routeDetailBusAdapterClass.viewholder> {

    ArrayList<ArrayList<String>> list;
    Context context;
    String lang;
    String source, destination;
    int rt_id_src, rt_id_dst;

    public routeDetailBusAdapterClass(ArrayList<ArrayList<String>> list, String lang, String Source, String destination, int rt_id_src, int rt_id_dst, Context context) {

        this.list = list;
        this.context = context;
        this.lang = lang;
        this.source = Source;
        this.destination = destination;
        this.rt_id_src = rt_id_src;
        this.rt_id_dst = rt_id_dst;
    }

    @NonNull
    @Override
    public routeDetailBusAdapterClass.viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.route_detail_bus_recycler_layout, parent, false);
        return new routeDetailBusAdapterClass.viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull routeDetailBusAdapterClass.viewholder holder, int position) {

        if ((Integer.valueOf(list.get(position).get(3)) <= rt_id_dst && Integer.valueOf(list.get(position).get(3)) >= rt_id_src) || (Integer.valueOf(list.get(position).get(3)) <= rt_id_src && Integer.valueOf(list.get(position).get(3)) >= rt_id_dst)) {
            if (lang.equals("en")) {
                holder.station_name.setText(list.get(position).get(1));
            } else if (lang.equals("bn")) {
                holder.station_name.setText(list.get(position).get(2));
            } else {
                holder.station_name.setText(list.get(position).get(1));
            }
            holder.station_name.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.station_name.setTypeface(null, Typeface.BOLD);
        } else {
            if (lang.equals("en")) {
                holder.station_name.setText(list.get(position).get(1));
            } else if (lang.equals("bn")) {
                holder.station_name.setText(list.get(position).get(2));
            } else {
                holder.station_name.setText(list.get(position).get(1));
            }
            holder.station_name.setTextColor(ContextCompat.getColor(context, R.color.dark_grey));
            holder.station_name.setTypeface(null, Typeface.NORMAL);
        }
        if (position == 0) {
            if (list.get(position).get(1).equals(source) || list.get(position).get(2).equals(source)) {
                holder.stepper_icon.setImageResource(R.drawable.route_start1_src);
            } else if (list.get(position).get(1).equals(destination) || list.get(position).get(2).equals(destination)) {
                holder.stepper_icon.setImageResource(R.drawable.route_start1_dst);
            } else {
                if ((Integer.valueOf(list.get(position).get(3)) < rt_id_dst && Integer.valueOf(list.get(position).get(3)) > rt_id_src) || (Integer.valueOf(list.get(position).get(3)) <= rt_id_src && Integer.valueOf(list.get(position).get(3)) >= rt_id_dst)) {
                    holder.stepper_icon.setImageResource(R.drawable.route_start1);
                } else {
                    holder.stepper_icon.setImageResource(R.drawable.route_start1_light_blue);
                }
            }
        } else if (position == list.size() - 1) {
            if (list.get(position).get(1).equals(source) || list.get(position).get(2).equals(source)) {
                holder.stepper_icon.setImageResource(R.drawable.route_end1_src);
            } else if (list.get(position).get(1).equals(destination) || list.get(position).get(2).equals(destination)) {
                holder.stepper_icon.setImageResource(R.drawable.route_end1_dst);
            } else {
                if ((Integer.valueOf(list.get(position).get(3)) < rt_id_dst && Integer.valueOf(list.get(position).get(3)) > rt_id_src) || (Integer.valueOf(list.get(position).get(3)) <= rt_id_src && Integer.valueOf(list.get(position).get(3)) >= rt_id_dst)) {
                    holder.stepper_icon.setImageResource(R.drawable.route_end1);
                } else {
                    holder.stepper_icon.setImageResource(R.drawable.route_end1_light_blue);
                }
            }
        } else {
            if (list.get(position).get(1).equals(source) || list.get(position).get(2).equals(source)) {
                holder.stepper_icon.setImageResource(R.drawable.route_mid1_src);
            } else if (list.get(position).get(1).equals(destination) || list.get(position).get(2).equals(destination)) {
                holder.stepper_icon.setImageResource(R.drawable.route_mid1_dst);
            } else {
                if ((Integer.valueOf(list.get(position).get(3)) < rt_id_dst && Integer.valueOf(list.get(position).get(3)) > rt_id_src) || (Integer.valueOf(list.get(position).get(3)) <= rt_id_src && Integer.valueOf(list.get(position).get(3)) >= rt_id_dst)) {
                    holder.stepper_icon.setImageResource(R.drawable.route_mid1);
                } else {
                    holder.stepper_icon.setImageResource(R.drawable.route_mid1_light_blue);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView station_name;
        public ImageView stepper_icon;

        public viewholder(View view) {
            super(view);
            station_name = (TextView) view.findViewById(R.id.stepper_station_txt_bus_route_details);
            stepper_icon = (ImageView) view.findViewById(R.id.stepper_img_bus_route_details);
        }
    }
}
