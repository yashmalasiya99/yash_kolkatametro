package com.example.kolkatametroapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.R;
import com.example.kolkatametroapp.database.DataAdapterTrains;

import java.util.ArrayList;

public class stationListExpressAdapterClass extends RecyclerView.Adapter<stationListExpressAdapterClass.viewholder> {
    ArrayList<ArrayList<String>> filterlist;
    ArrayList<ArrayList<String>> list;
    Context context;
    int count;
    DataAdapterTrains dbAdapter;
    String lang;

    public stationListExpressAdapterClass(ArrayList<ArrayList<String>> list, String lang, Context context) {
        this.list = list;
        this.context = context;
        this.filterlist = new ArrayList<>();
        this.filterlist.addAll(list);
        this.count = list.size();
        this.lang = lang;

        dbAdapter = new DataAdapterTrains(context);
        dbAdapter.createDatabase();
        dbAdapter.open();
    }

    @NonNull
    @Override
    public stationListExpressAdapterClass.viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.station_list_local_recycler_layout, parent, false);
        return new stationListExpressAdapterClass.viewholder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull stationListExpressAdapterClass.viewholder holder, int position) {
//        if (lang.equals("en")) {
//            holder.src_name.setText(list.get(position).get(1));
//        } else if (lang.equals("bn")) {
//            holder.src_name.setText(list.get(position).get(3));
//        } else {
//            holder.src_name.setText(list.get(position).get(1));
//        }
        holder.src_code.setText(list.get(position).get(2));
        holder.src_name.setText(list.get(position).get(3));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filter(CharSequence charText) {
        String filterString = charText.toString().toLowerCase();
        list.clear();
        if (charText.length() == 0) {
            list.addAll(filterlist);
        } else {
            ArrayList<ArrayList<String>> filterList = dbAdapter.GetAllStationsList(filterString);
            list.addAll(filterList);
        }
        notifyDataSetChanged();
    }

    public class viewholder extends RecyclerView.ViewHolder {
        public TextView src_name, src_code;

        public viewholder(View view) {
            super(view);
            src_code = (TextView) view.findViewById(R.id.txt_list_srch_kolkata_local_code);
            src_name = (TextView) view.findViewById(R.id.txt_list_srch_kolkata_local);
        }
    }
}
