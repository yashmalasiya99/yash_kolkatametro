package com.example.kolkatametroapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.nearest_list_station_adapter;
import com.example.kolkatametroapp.database.DataAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class nearestStationList extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    Toolbar toolbar;
    DataAdapter dbAdapter;
    ArrayList<ArrayList<String>> stations;
    double distance;
    double latitudeDefault;
    double longitudeDefault;
    RecyclerView recyclerView;
    ArrayList<String> listdata;
    ArrayList<ArrayList<String>> list;
    ArrayList<ArrayList<String>> sortedFilterList;
    String range;
    int IndexOfSmallestKm;
    ArrayList<Double> kilometers;
    Double Range;
    nearest_list_station_adapter adapter;
    SharedPreferences preferences;
    String lang;

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_EVEN);
        return bd.doubleValue();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }

        setContentView(R.layout.nearest_station_list_layout);

        toolbar = findViewById(R.id.toolbar_nearStationList);
        setSupportActionBar(toolbar);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getLocation();

        range = getIntent().getStringExtra("range");

        Range = Double.valueOf(range);
        System.out.println("range" + Range);

        dbAdapter = new DataAdapter(nearestStationList.this);
        dbAdapter.createDatabase();
        dbAdapter.open();

        recyclerView = findViewById(R.id.station_list_nearest_recycler);

        list = new ArrayList<>();
        kilometers = new ArrayList<>();
        sortedFilterList = new ArrayList<>();

        stations = dbAdapter.getAllStations();

        dbAdapter.close();

        for (int i = 0; i < stations.size(); i++) {
            listdata = new ArrayList<>();
            distance = getKmFromLatLong(latitudeDefault, longitudeDefault, Double.valueOf(stations.get(i).get(5)), Double.valueOf(stations.get(i).get(6)));
            distance = round(distance, 1);
            if (lang.equals("en")) {
                listdata.add(stations.get(i).get(1));
            } else if (lang.equals("bn")) {
                listdata.add(stations.get(i).get(3));
            } else {
                listdata.add(stations.get(i).get(1));
            }
            listdata.add(stations.get(i).get(5));
            listdata.add(stations.get(i).get(6));
            listdata.add(String.valueOf(distance));

            list.add(listdata);
        }

        for (int i = 0; i < list.size(); i++) {
            kilometers.add(Double.valueOf(list.get(i).get(3)));
        }

        Collections.sort(kilometers);

        for (int i = 0; i < kilometers.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (Double.valueOf(list.get(j).get(3)).equals(kilometers.get(i))) {
                    IndexOfSmallestKm = j;
                    break;
                }
            }
            ArrayList<String> sortedFilterdata = new ArrayList<>();
            sortedFilterdata.add(list.get(IndexOfSmallestKm).get(0));
            sortedFilterdata.add(list.get(IndexOfSmallestKm).get(1));
            sortedFilterdata.add(list.get(IndexOfSmallestKm).get(2));
            sortedFilterdata.add(list.get(IndexOfSmallestKm).get(3));

            sortedFilterList.add(sortedFilterdata);

            list.remove(IndexOfSmallestKm);
        }

        adapter = new nearest_list_station_adapter(sortedFilterList, Range, nearestStationList.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(nearestStationList.this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

    }

    public double getKmFromLatLong(double lat1, double lng1, double lat2, double lng2) {
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lng1);
        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lng2);
        double distanceInMeters = loc1.distanceTo(loc2);
        return distanceInMeters / 1000;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getLocation() {
        Double latitude = 0.0, longitude;
        String message = "";
        LocationManager mlocManager = null;
        LocationListener mlocListener;
        mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mlocListener = new MyLocationListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
        if (mlocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            latitude = MyLocationListener.latitude;
            longitude = MyLocationListener.longitude;
            message = message + "https://www.google.com/maps/dir/@" + latitude + "," + longitude;
            latitudeDefault = latitude;
            longitudeDefault = longitude;
            if (latitude == 0.0) {
                Toast.makeText(getApplicationContext(), "Currently gps has not found your location....", Toast.LENGTH_LONG).show();
            }

        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(nearestStationList.this);

            // Setting Dialog Title
            alertDialog.setTitle("GPS is settings");

            // Setting Dialog Message
            alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
            alertDialog.setCancelable(false);
            // On pressing Settings button
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                    finish();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item1, menu);

        MenuItem routeMapMenu = menu.findItem(R.id.route_map_icon);
        ImageView iv = new ImageView(this);
        iv.setBackground(getResources().getDrawable(R.drawable.location_drawable_white));
        Animation animation = new AlphaAnimation((float) 2, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        iv.startAnimation(animation);
        routeMapMenu.setActionView(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(nearestStationList.this, routeMapWebview.class);
                startActivity(intent);
            }
        });
        return true;
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}
