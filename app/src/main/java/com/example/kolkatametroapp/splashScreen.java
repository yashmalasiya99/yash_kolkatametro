package com.example.kolkatametroapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

import androidx.annotation.Nullable;

import com.example.kolkatametroapp.utility.FileUtils;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Locale;

public class splashScreen extends Activity {
    public static final String MyPREFERENCES = "LangaugePref";
    SharedPreferences preferences;
    String lang, ServerVersion = "1.0";
    ProgressDialog progressDialog;
    String FileN = null;
    String DB_PATH = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        lang = preferences.getString("langauge", "");

        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }

        setContentView(R.layout.splash_screen_layout);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        FileUtils.checkIfFileExistsInDBFolder(splashScreen.this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                goToNextPage();
            }
        }, 1000);

//        AndroidNetworking.get("https://appspunditinfotech.com/kolkata/update.json")
//                .addHeaders("Cache-Control", "no-cache")
//                .setPriority(Priority.HIGH)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
////                        System.out.println("xXxX: response : " + response.toString());
//                        try {
//                            String serverversion = response.getString("train_verison");
////                            System.out.println("xXxX: serverversion  : " + serverversion + " // " + preferences.getString("DBVersion", "0"));
//                            if (Float.parseFloat(serverversion) > Float.parseFloat(preferences.getString("DBVersion", "0"))) {
//                                ServerVersion = serverversion;
//
////                                Decompress_ decompress = new Decompress_();
////                                decompress.execute("https://appspunditinfotech.com/kolkata/kolkata.zip");
//
//                                new Decompress_().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
//                            } else if (Float.parseFloat(serverversion) < Float.parseFloat(preferences.getString("DBVersion", "0"))) {
//                                goToNextPage();
//                            } else {
//                                goToNextPage();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
////                            System.out.println("xXxX: catch : " + e.toString());
//                            goToNextPage();
//                        }
//                    }
//
//                    @Override
//                    public void onError(ANError anError) {
////                        System.out.println("xXxX: anError : " + anError.toString());
//                        goToNextPage();
//                    }
//                });
//
    }

    private void goToNextPage() {
        Intent i = new Intent(splashScreen.this, MainDashboard.class);
        startActivity(i);
        finish();
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

//    public class Decompress_ extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            progressDialog = new ProgressDialog(splashScreen.this);
//
//            progressDialog.setMessage("Please wait, updating timetable.");
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... str) {
//            try {
//                DB_PATH = getApplicationInfo().dataDir + "/kolkata/";
//                File kolkataDir = new File(DB_PATH);
//                kolkataDir.mkdirs();
//                FileN = "DataDownloader_" + UUID.randomUUID().toString() + ".zip";
////                downloadZipFile("https://appspunditinfotech.com/kolkata/kolkata.zip", DB_PATH + FileN);
//                downloadZipFile("https://accufeedback.com/trains/kolkata/kolkata.zip", DB_PATH + FileN);
//            } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            progressDialog.cancel();
//            progressDialog.dismiss();
//
//            Toast.makeText(splashScreen.this, "Timetable updated", Toast.LENGTH_LONG).show();
//
//            goToNextPage();
//        }
//    }
//
//    public void downloadZipFile(String urlStr, String destinationFilePath) {
//        InputStream input = null;
//        OutputStream output = null;
//        HttpURLConnection connection = null;
//        try {
//            URL url = new URL(urlStr);
//
//            connection = (HttpURLConnection) url.openConnection();
//            connection.setUseCaches(false);
//            connection.setDefaultUseCaches(false);
//            connection.connect();
//
//            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                Log.d("downloadZipFile", "Server ResponseCode=" + connection.getResponseCode() + " ResponseMessage=" + connection.getResponseMessage());
//            }
//
//            // download the file
//            input = connection.getInputStream();
//
//            new File(destinationFilePath).createNewFile();
//            output = new FileOutputStream(destinationFilePath);
//
//            byte data[] = new byte[4096];
//            int count;
//            while ((count = input.read(data)) != -1) {
//                output.write(data, 0, count);
//            }
//        } catch (Exception e) {
//            System.out.println("xXxX: downloadZipFile Exception " + e.getMessage());
//            e.printStackTrace();
//        } finally {
//            try {
//                if (output != null) output.close();
//                if (input != null) input.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            if (connection != null) connection.disconnect();
//        }
//
////        Log.d("downloadZipFile", "f.getParentFile().getPath()=" + f.getParentFile().getPath());
////        Log.d("downloadZipFile", "f.getName()=" + f.getName().replace(".zip", ""));
//
//        boolean unpack = unpackZip(destinationFilePath);
//        if (unpack) {
////            System.out.println("xXxX: file unzipped");
//            SharedPreferences.Editor editor = preferences.edit();
//            editor.putString("DBVersion", ServerVersion);
//            editor.commit();
//            File f1 = new File(destinationFilePath);
//            boolean deleted = f1.delete();
//            if (deleted) {
////                System.out.println("xXxX: zip deleted");
//            } else {
////                System.out.println("xXxX: zip not deleted");
//            }
//        } else {
////            System.out.println("xXxX: file not unzipped");
//        }
//    }
//
//    public boolean unpackZip(String filePath) {
//        InputStream is;
//        ZipInputStream zis;
//        try {
//            String filename;
//
//            is = new FileInputStream(filePath);
//            zis = new ZipInputStream(new BufferedInputStream(is));
//            ZipEntry ze;
//            byte[] buffer = new byte[1024];
//            int count;
//
//            while ((ze = zis.getNextEntry()) != null) {
//                filename = ze.getName();
//
//                if (ze.isDirectory()) {
//                    File fmd = new File(getApplicationInfo().dataDir + "/databases/" + filename);
//                    fmd.mkdirs();
//                    continue;
//                }
//
//                FileOutputStream fout = new FileOutputStream(getApplicationInfo().dataDir + "/databases/" + filename);
//
//                while ((count = zis.read(buffer)) != -1) {
//                    fout.write(buffer, 0, count);
//                }
//
//                fout.close();
//                zis.closeEntry();
//            }
//
//            zis.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false;
//        }
//
//        return true;
//    }
}
