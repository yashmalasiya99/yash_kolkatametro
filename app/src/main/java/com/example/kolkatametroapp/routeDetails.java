package com.example.kolkatametroapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.route_fragment_stepper_adapter_class;
import com.example.kolkatametroapp.classes.RecyclerItemClickListener;
import com.example.kolkatametroapp.database.DataAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import java.util.ArrayList;
import java.util.Locale;

public class routeDetails extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    ArrayList<Integer> sequenceList;
    Button report_time_table_issue;
    String srcId, dstId, srcCode, dstCode;
    int src_id, dst_id;
    int change;
    Toolbar toolbar;
    RecyclerView recyclerView;
    int metro_line;
    TextView short_name_view, long_name_view;
    DataAdapter dbAdapter;
    String train_id, train_name;
    ArrayList<ArrayList<String>> stationArray;
    route_fragment_stepper_adapter_class adapter;
    SharedPreferences preferences;
    String lang;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }
        setContentView(R.layout.route_details_layout);

        toolbar = findViewById(R.id.toolbar_route_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        AdRequest adRequest1 = new AdRequest.Builder().build();

        InterstitialAd.load(routeDetails.this, getResources().getString(R.string.inter), adRequest1,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        mInterstitialAd = null;
                    }
                });

        srcId = getIntent().getStringExtra("src_id");
        dstId = getIntent().getStringExtra("dst_id");
        srcCode = getIntent().getStringExtra("src_code");
        dstCode = getIntent().getStringExtra("dst_code");
        sequenceList = getIntent().getIntegerArrayListExtra("SequenceList");
        change = getIntent().getIntExtra("change", 0);
        train_id = getIntent().getStringExtra("train_id");
        train_name = getIntent().getStringExtra("train_name");

        stationArray = new ArrayList<>();

        recyclerView = findViewById(R.id.route_stepper_recycler_view);
        report_time_table_issue = findViewById(R.id.report_time_table_issue_route);
        short_name_view = findViewById(R.id.short_name_route_details);
        long_name_view = findViewById(R.id.long_name_route_details);

        src_id = Integer.valueOf(srcId);
        dst_id = Integer.valueOf(dstId);

        short_name_view.setText(train_id);
        long_name_view.setText(train_name);

        dbAdapter = new DataAdapter(routeDetails.this);
        dbAdapter.createDatabase();
        dbAdapter.open();

        System.out.println("ZZZZZZZZZZZZZZZZ");


        // get stations for all the sequence list
        for (int k = 0; k < sequenceList.size(); k++) {
            ArrayList<String> srcToDestStationArray = dbAdapter.getStationsForSrcToDst(sequenceList.get(k));
            stationArray.add(srcToDestStationArray);
        }

        dbAdapter.close();

        try {

            if (!sequenceList.isEmpty() || sequenceList != null||sequenceList.get(0)!=null) {


                //condition for line where 1 denotes purple 2 denotes blue.
                if (((sequenceList.get(0) >= 1 && sequenceList.get(sequenceList.size() - 1) <= 26)) || ((sequenceList.get(0) <= 26 && sequenceList.get(sequenceList.size() - 1) >= 1))) {
                    metro_line = 1;
                }
                if (sequenceList == null || sequenceList.isEmpty() || sequenceList.get(0) == null) {
                    metro_line = 2;
                }
            }
            else {
                metro_line = 2;
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        adapter = new route_fragment_stepper_adapter_class(stationArray, metro_line, lang, routeDetails.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(routeDetails.this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        //recycler view on click listener for particular position and intent to station info.
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(routeDetails.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent i = new Intent(routeDetails.this, stationListInfo.class);
                i.putExtra("stationid", stationArray.get(position).get(0));
                i.putExtra("stationName", stationArray.get(position).get(1));
                i.putExtra("stationCode", stationArray.get(position).get(2));
                i.putExtra("bengali", stationArray.get(position).get(3));
                i.putExtra("lineId", stationArray.get(position).get(4));
                i.putExtra("landline", stationArray.get(position).get(7));
                i.putExtra("mobile", stationArray.get(position).get(8));
                i.putExtra("parking", stationArray.get(position).get(10));
                i.putExtra("address", stationArray.get(position).get(14));
                i.putExtra("entry", stationArray.get(position).get(15));
                i.putExtra("exit", stationArray.get(position).get(16));
                startActivity(i);
            }
        }));

        //reporting time table issue
        report_time_table_issue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = "Report Timetable Issue";
                String bodyText = "From " + stationArray.get(0).get(1) + " to " + stationArray.get(stationArray.size() - 1).get(1);
                String mailto = "mailto:appspundit2014@gmail.com" +
                        "?cc=" + "" +
                        "&subject=" + Uri.encode(subject) +
                        "&body=" + Uri.encode(bodyText);
                Intent EmailIntent = new Intent(Intent.ACTION_SENDTO);
                Uri data = Uri.parse(mailto);
                EmailIntent.setData(data);
                try {
                    startActivity(EmailIntent);
                } catch (ActivityNotFoundException e) {
                    //TODO: Handle case where no email app is available
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item1, menu);

        MenuItem routeMapMenu = menu.findItem(R.id.route_map_icon);
        ImageView iv = new ImageView(this);
        iv.setBackground(getResources().getDrawable(R.drawable.location_drawable_white));
        Animation animation = new AlphaAnimation((float) 2, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        iv.startAnimation(animation);
        routeMapMenu.setActionView(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(routeDetails.this, routeMapWebview.class);
                startActivity(intent);
            }
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd != null) {
            mInterstitialAd.show(routeDetails.this);
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(routeDetails.this);
                }
                this.finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
