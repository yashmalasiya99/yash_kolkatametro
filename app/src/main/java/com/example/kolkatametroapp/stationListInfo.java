package com.example.kolkatametroapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.example.kolkatametroapp.database.DataAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Locale;

public class stationListInfo extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    Toolbar toolbar;
    TextView Address, Line, Parking, Landline, Mobile, Station, tostation, fromstation, fromstationkm, tostationkm, entrystation, exitstation;
    Button show_in_map;
    String stationid, stationName, line, landline, mobile, parking, address, kannada, entrygates, exitgates, stationCode;
    int stationID;
    String[] sequencelist;
    ArrayList<Integer> sequenceList;
    String zerofrom, zeroto;
    CardView from_to_card;
    ArrayList<ArrayList<String>> stationListFromStationDuration;
    ArrayList<ArrayList<String>> stationListToStationDuration;
    DataAdapter dbAdapter;
    SharedPreferences preferences;
    String lang;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }
        setContentView(R.layout.station_list_info_layout);

        toolbar = findViewById(R.id.toolbar_station_info);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);

        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);


        Address = findViewById(R.id.address);
        Line = findViewById(R.id.line);
        Parking = findViewById(R.id.parking);
        Landline = findViewById(R.id.landline);
        Mobile = findViewById(R.id.mobile);
        Station = findViewById(R.id.station);
        show_in_map = findViewById(R.id.btn_view_in_map);
        tostation = findViewById(R.id.to);
        tostationkm = findViewById(R.id.todistance);
        fromstation = findViewById(R.id.from);
        fromstationkm = findViewById(R.id.fromdistance);
        from_to_card = findViewById(R.id.from_to_cardview);
        entrystation = findViewById(R.id.Entry);
        exitstation = findViewById(R.id.Exit);


        stationid = getIntent().getStringExtra("stationid");
        stationName = getIntent().getStringExtra("stationName");
        stationCode = getIntent().getStringExtra("stationCode");
        kannada = getIntent().getStringExtra("bengali");
        line = getIntent().getStringExtra("lineId");
        landline = getIntent().getStringExtra("landline");
        mobile = getIntent().getStringExtra("mobile");
        parking = getIntent().getStringExtra("parking");
        address = getIntent().getStringExtra("address");
        entrygates = getIntent().getStringExtra("entry");
        exitgates = getIntent().getStringExtra("exit");

        if (lang.equals("en")) {
            getSupportActionBar().setTitle(stationName);
        } else if (lang.equals("kn")) {
            getSupportActionBar().setTitle(kannada);
        } else {
            getSupportActionBar().setTitle(stationName);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sequenceList = new ArrayList<>();

        Address.setText(address);
        if (lang.equals("en")) {
            Station.setText(stationName);
        } else if (lang.equals("bn")) {
            Station.setText(kannada);
        } else {
            Station.setText(stationName);
        }

        // condition for line
        if (line.equals("1")) {
            Line.setText(getResources().getString(R.string.purple));
            Line.setTextColor(ContextCompat.getColor(stationListInfo.this, R.color.purple));
        } else if (line.equals("2")) {
            Line.setText(getResources().getString(R.string.blue));
            Line.setTextColor(ContextCompat.getColor(stationListInfo.this, R.color.light_blue));
        }

        // condition for parking
        if (parking == null) {
            Parking.setText(getResources().getString(R.string.no_data));
        } else {
            Parking.setText(parking);
        }

        // condition for landline
        if (landline == null) {
            Landline.setText(getResources().getString(R.string.no_data));
        } else {
            Landline.setText(landline);
        }

        // condition for mobile
        if (mobile == null) {
            Mobile.setText(getResources().getString(R.string.no_data));
        } else {
            Mobile.setText(mobile);
        }

        stationID = Integer.parseInt(stationid);

        dbAdapter = new DataAdapter(stationListInfo.this);
        dbAdapter.createDatabase();
        dbAdapter.open();

        ArrayList<ArrayList<String>> stationListSeq = dbAdapter.getstationlListFromStationId(stationID);
        sequencelist = stationListSeq.get(0).get(1).split("-");
        for (String split : sequencelist) {
            if (!split.equals("")) {
                sequenceList.add(Integer.valueOf(split));
            }
        }
        ArrayList<String> stationListFirstStation = dbAdapter.getStationsForSrcToDst(sequenceList.get(0));
        ArrayList<String> stationListLastStation = dbAdapter.getStationsForSrcToDst(sequenceList.get(sequenceList.size() - 1));

        if (lang.equals("en")) {
            fromstation.setText(stationListFirstStation.get(1));
        } else if (lang.equals("bn")) {
            fromstation.setText(stationListFirstStation.get(3));
        } else {
            fromstation.setText(stationListFirstStation.get(1));
        }

        if (lang.equals("en")) {
            tostation.setText(stationListLastStation.get(1));
        } else if (lang.equals("bn")) {
            tostation.setText(stationListLastStation.get(3));
        } else {
            tostation.setText(stationListLastStation.get(1));
        }

        if (stationCode.equals(stationListFirstStation.get(2))) {
            zerofrom = "0";
            fromstationkm.append(zerofrom + " " + getResources().getString(R.string.station_radius_km));
        }
        else {
//            System.out.println("xxX: getMetroListStationBetweenStationIDsFareDetails:>>- " + stationCode + ", " + stationListFirstStation.get(2));

            stationListFromStationDuration = dbAdapter.getMetroListStationBetweenStationIDsFareDetails(stationCode, stationListFirstStation.get(2));

//            System.out.println("xxX: stationListFromStationDuration:>>- " + stationListFromStationDuration.toString());

            try {
                fromstationkm.append(stationListFromStationDuration.get(0).get(5) + " " + getResources().getString(R.string.station_radius_km));
            } catch (Exception ex) {
                fromstationkm.append("");
            }
        }

        if (stationCode.equals(stationListLastStation.get(2))) {
            zeroto = "0";
            tostationkm.append(zeroto + " " + getResources().getString(R.string.station_radius_km));
        } else {
            stationListToStationDuration = dbAdapter.getMetroListStationBetweenStationIDsFareDetails(stationCode, stationListLastStation.get(2));
            tostationkm.append(stationListToStationDuration.get(0).get(5) + " " + getResources().getString(R.string.station_radius_km));
        }

        if (entrygates == null || entrygates.equals("")) {
            entrystation.setText(getResources().getString(R.string.no_data));
        } else {
            entrystation.append("Gate No " + entrygates);
        }

        if (exitgates == null || exitgates.equals("")) {
            exitstation.setText(getResources().getString(R.string.no_data));
        } else {
            exitstation.append("Gate No " + exitgates);
        }

        show_in_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(stationListInfo.this, routeMapWebview.class);
                startActivity(i);
            }
        });

        dbAdapter.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item1, menu);

        MenuItem routeMapMenu = menu.findItem(R.id.route_map_icon);
        ImageView iv = new ImageView(this);
        iv.setBackground(getResources().getDrawable(R.drawable.location_drawable_white));
        Animation animation = new AlphaAnimation((float) 2, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        iv.startAnimation(animation);
        routeMapMenu.setActionView(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(stationListInfo.this, routeMapWebview.class);
                startActivity(intent);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
