package com.example.kolkatametroapp;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.kolkatametroapp.utility.FileUtils;
import com.example.kolkatametroapp.utility.HttpHandler;
import com.example.kolkatametroapp.viewpager.find_route_viewpager;
import com.google.android.material.navigation.NavigationView;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sanojpunchihewa.updatemanager.UpdateManager;
import com.sanojpunchihewa.updatemanager.UpdateManagerConstant;

import org.json.JSONObject;

import java.io.File;
import java.util.Locale;
import java.util.UUID;

public class MainDashboard extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    UpdateManager mUpdateManager;
    DrawerLayout drawer;
    NavigationView navView;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    SharedPreferences preferences;
    String lang;
    View view;
    ReviewManager reviewManager;
    Task<ReviewInfo> request;
    ReviewInfo reviewInfo = null;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        lang = preferences.getString("langauge", "");

        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }
        setContentView(R.layout.main_dashboard_layout);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        ViewGroup viewGroup = findViewById(android.R.id.content);
        view = LayoutInflater.from(this).inflate(R.layout.change_lang_dialog_layout, viewGroup, false);

        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navView = findViewById(R.id.nav_view);

        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            defaulthomefragment();
            navView.setCheckedItem(R.id.hommie);
        }
        //Rate the app google api
        reviewManager = ReviewManagerFactory.create(MainDashboard.this);
        request = reviewManager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // We can get the ReviewInfo object
                reviewInfo = task.getResult();
            }
        });
        //Update the app google api library

        try {
            mUpdateManager = UpdateManager.Builder(this).mode(UpdateManagerConstant.IMMEDIATE);
            mUpdateManager.start();
        } catch (Exception ex) {
            Toast.makeText(this, "A new version is available. Please update your app !!", Toast.LENGTH_SHORT).show();
        }

        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.hommie://home
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentcontainer,
                                new homeMainFragment()).commit();
                        break;

                    case R.id.find_route://find route
                        Intent i = new Intent(MainDashboard.this, find_route_viewpager.class);
                        startActivity(i);
                        break;

                    case R.id.route_map://route map
                        Intent intent = new Intent(MainDashboard.this, routeMapWebview.class);
                        startActivity(intent);
                        break;

                    case R.id.vastral_gam_apparal_park://stations
                        Intent stationIntent = new Intent(MainDashboard.this, stationList.class);
                        startActivity(stationIntent);
                        break;

                    case R.id.request_feature://request features
                        String subject = "Request Feature";
                        String bodyText = "";
                        String mailto = "mailto:appspundit2014@gmail.com" +
                                "?cc=" + "" +
                                "&subject=" + Uri.encode(subject) +
                                "&body=" + Uri.encode(bodyText);
                        Intent EmailIntent = new Intent(Intent.ACTION_SENDTO);
                        Uri data = Uri.parse(mailto);
                        EmailIntent.setData(data);
                        try {
                            startActivity(EmailIntent);
                        } catch (ActivityNotFoundException e) {
                            //TODO: Handle case where no email app is available
                            Toast.makeText(MainDashboard.this, "No app available to send email.", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.find_nearest_station_nav://find nearest stations
                        Intent neareststationIntent = new Intent(MainDashboard.this, findNearestStation.class);
                        startActivity(neareststationIntent);
                        break;

                    case R.id.change_lang://lang change
                        lang = preferences.getString("langauge", "");

                        RadioButton rbEng = view.findViewById(R.id.english);
                        RadioButton rbKann = view.findViewById(R.id.kannada);

                        if (lang.equals("en")) {
                            rbEng.setChecked(true);
                        } else if (lang.equals("bn")) {
                            rbKann.setChecked(true);
                        } else {
                            rbEng.setChecked(true);
                        }

                        homeMainFragment fragment = (homeMainFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentcontainer);
                        fragment.showDialog(view);

                        break;

                    case R.id.about_us://about us
                        Intent PlayStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/dev?id=4744559207594823968"));
                        startActivity(PlayStoreIntent);
                        break;

                    case R.id.share://share
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Kolkata Local App");
                        String app_url = "Kolkata local app is one stop search destination to plan your trips around city with local train, Metro, Bus Trips Planner Download the app from this link  " + " https://play.google.com/store/apps/details?id=" + getPackageName();
                        shareIntent.putExtra(Intent.EXTRA_TEXT, app_url);
                        startActivity(Intent.createChooser(shareIntent, "Share via"));
                        break;

                    case R.id.rate_us://rate us
                        Intent rateusintent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                        startActivity(rateusintent);
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        new DownloadFilesTask().execute("");
    }

    public void defaulthomefragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentcontainer,
                new homeMainFragment()).commit();
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item, menu);

        MenuItem routeMapMenu = menu.findItem(R.id.route_map_menu);
        ImageView iv = new ImageView(this);
        iv.setBackground(getResources().getDrawable(R.drawable.location_drawable_white));
        Animation animation = new AlphaAnimation((float) 2, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        iv.startAnimation(animation);
        routeMapMenu.setActionView(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainDashboard.this, routeMapWebview.class);
                startActivity(intent);

            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_language_menu:
                lang = preferences.getString("langauge", "");

                RadioButton rbEng = view.findViewById(R.id.english);
                RadioButton rbKann = view.findViewById(R.id.kannada);

                if (lang.equals("en")) {
                    rbEng.setChecked(true);
                } else if (lang.equals("bn")) {
                    rbKann.setChecked(true);
                } else {
                    rbEng.setChecked(true);
                }

                homeMainFragment fragment = (homeMainFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentcontainer);
                fragment.showDialog(view);

                break;
        }
        return true;
    }

    /* private void ShowRatedilogmanual() {
         AlertDialog.Builder builder = new AlertDialog.Builder(MainDashboard.this);

         builder.setIcon(R.mipmap.icon);
         builder.setTitle("Rate the app");
         builder.setMessage(Html.fromHtml("If you like Kolkata App please rate us!", null, null));

         builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialogInterface, int i) {
                 SharedPreferences.Editor editor = prefs.edit();
                 editor.putBoolean("rate", false);
                 editor.commit();
                 Uri uri = Uri.parse("market://details?id=" + getPackageName());
                 startActivity(new Intent(Intent.ACTION_VIEW, uri));
             }
         });
         builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialogInterface, int i) {
                 finish();
             }
         });

         builder.setCancelable(true);

         AlertDialog dialog = builder.create();
         if (MainDashboard.this != null && !MainDashboard.this.isFinishing()) {
             dialog.show();
         }
     }
 */
    @Override
    public void onBackPressed() {
        if (preferences.getBoolean("rate", true)) {
            showrateDialog();
        } else {
            showexitdilog();

        }

    }

    private void showrateDialog() {
        try {
            // System.out.println("Rate called Activity finish:::1");
            if (reviewInfo == null) {
                showexitdilog();
            } else {
                Task<Void> flow = reviewManager.launchReviewFlow(MainDashboard.this, reviewInfo);
                flow.addOnCompleteListener(task1 -> {
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                    // System.out.println("Rate called Activity finish:::2");
                    finish();
                });
            }
        } catch (Exception e) {
            showexitdilog();
        }
    }

    private void showexitdilog() {
        AlertDialog.Builder ab = new AlertDialog.Builder(MainDashboard.this);
        ab.setTitle("ExitApp");
        ab.setIcon(R.mipmap.icon);
        ab.setMessage("Are you sure you want to exit this app?");
        ab.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //if you want to finish just current activity
                finish();
            }
        });
        ab.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        ab.show();
    }

    private void showRestartDilog() {
        AlertDialog.Builder ab = new AlertDialog.Builder(MainDashboard.this);
        ab.setTitle("TimeTable Updated");
        ab.setIcon(R.mipmap.icon);
        ab.setMessage("Timetable has been updated. We recommend you to restart the app.");
        ab.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                Intent intent = new Intent(MainDashboard.this, splashScreen.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        ab.setNegativeButton("Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        try {
            ab.show();
        } catch (WindowManager.BadTokenException e) {
        } catch (Exception e) {
        }
    }

    private class DownloadFilesTask extends AsyncTask<String, Integer, String> {
        boolean showExitDialog = false;

        protected String doInBackground(String... urls) {
            try {
                String jsonStr = HttpHandler.get(MainDashboard.this, "https://appspunditinfotech.com/kolkata/update.json");
                JSONObject jsonObject = new JSONObject(jsonStr);
                String train_version = jsonObject.getString("train_verison");
                System.out.println("Train Version "+train_version);


//                String stations_version = jsonObject.getString("stations_version");

//                System.out.println("x-x: jsonObject:::" + jsonObject.toString());
//                System.out.println("x-x: DBVersion:::" + preferences.getString("DBVersion", "0"));

                File dataDir = FileUtils.getDataDir(MainDashboard.this);
//                System.out.println("x-x: file path:::::" + dataDir.getAbsolutePath());
                if (Float.parseFloat(train_version) > Float.parseFloat(preferences.getString("DBVersion", "35"))) {
                    String FileN = "DataDownloader_" + UUID.randomUUID().toString() + ".zip";
                    boolean zipstrainstatus = FileUtils.downloadZipFile("https://accufeedback.com/trains/kolkata/kolkata.zip", dataDir.getAbsolutePath() + "/" + FileN);
//                    System.out.println("x-x: dataDir: " + dataDir.getAbsolutePath() + " // " + FileN);
                    if (zipstrainstatus) {

//                        System.out.println("x-x: zipstrainstatus");
                        boolean st = FileUtils.unpackZip(dataDir.getAbsolutePath() + "/" + FileN);
//                        System.out.println("x-x: file successfully uploaded path: " + dataDir.getAbsolutePath() + "/" + FileN);
                        if (st) {
                            preferences.edit().putString("DBVersion", train_version).commit();
                            showExitDialog = true;
                        } else {
                            showExitDialog = false;
                        }
                    } else {
                        showExitDialog = false;
                    }
                }

                else {
                    showExitDialog = false;
                }
            } catch (Exception e) {
                showExitDialog = false;
                System.out.println("x-x: database zip fetching error:::" + e.toString());
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            if (showExitDialog) {
                try {
                    showRestartDilog();
                } catch (Exception ex) {
                }
            }
        }
    }
}
