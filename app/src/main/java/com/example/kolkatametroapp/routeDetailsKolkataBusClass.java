package com.example.kolkatametroapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.routeDetailBusAdapterClass;
import com.example.kolkatametroapp.database.DataAdapterBus;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import java.util.ArrayList;
import java.util.Locale;

public class routeDetailsKolkataBusClass extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    SharedPreferences preferences;
    String lang;
    RecyclerView recyclerView;
    Button report_time_table_issue;
    Toolbar toolbar;
    routeDetailBusAdapterClass adapter;
    int bus_id_int;
    int rt_id_src, rt_id_dst;
    ArrayList<ArrayList<String>> final_route_details_local_array;
    DataAdapterBus dbAdapter;
    ArrayList<ArrayList<String>> routeStationListArray;
    int src_station_id, dst_station_id;
    String short_name, long_name, bus_id, cal_id, source, destination;
    TextView long_name_txt;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }

        setContentView(R.layout.route_details_kolkata_bus_layout);

        toolbar = findViewById(R.id.toolbar_route_details_bus);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        AdRequest adRequest1 = new AdRequest.Builder().build();

        InterstitialAd.load(routeDetailsKolkataBusClass.this, getResources().getString(R.string.inter), adRequest1,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        mInterstitialAd = null;
                    }
                });

        short_name = getIntent().getStringExtra("short_name");
        long_name = getIntent().getStringExtra("long_name");
        bus_id = getIntent().getStringExtra("bus_id");
        source = getIntent().getStringExtra("src");
        destination = getIntent().getStringExtra("dst");
        src_station_id = getIntent().getIntExtra("src_id", 0);
        dst_station_id = getIntent().getIntExtra("dst_id", 0);

        getSupportActionBar().setTitle("Bus no. " + short_name);

        System.out.println("short " + short_name);
        System.out.println("long " + long_name);
        System.out.println("trip_id " + bus_id);
        System.out.println("cal_id " + cal_id);

        recyclerView = findViewById(R.id.route_stepper_recycler_view_route_details_bus);
        report_time_table_issue = findViewById(R.id.report_time_table_issue_route_route_details_bus);
        long_name_txt = findViewById(R.id.long_name_route_details_bus);

        long_name_txt.setText(long_name);

        bus_id_int = Integer.valueOf(bus_id);

        final_route_details_local_array = new ArrayList<>();

        dbAdapter = new DataAdapterBus(routeDetailsKolkataBusClass.this);
        dbAdapter.createDatabase();
        dbAdapter.open();

        if (src_station_id < dst_station_id) {
            routeStationListArray = dbAdapter.GetSrcDstRouteArray(bus_id_int);
        } else {
            routeStationListArray = dbAdapter.GetSrcDstRouteArrayReverse(bus_id_int);
        }

//        System.out.println("route array size : " +routeStationListArray.size());

        dbAdapter.close();

        for (int i = 0; i < routeStationListArray.size(); i++) {
            if (routeStationListArray.get(i).get(1).equals(source) || routeStationListArray.get(i).get(2).equals(source)) {
                rt_id_src = Integer.valueOf(routeStationListArray.get(i).get(3));
            }
            if (routeStationListArray.get(i).get(1).equals(destination) || routeStationListArray.get(i).get(2).equals(destination)) {
                rt_id_dst = Integer.valueOf(routeStationListArray.get(i).get(3));
            }
        }

        adapter = new routeDetailBusAdapterClass(routeStationListArray, lang, source, destination, rt_id_src, rt_id_dst, routeDetailsKolkataBusClass.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(routeDetailsKolkataBusClass.this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        report_time_table_issue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String subject = "Report Timetable Issue";
                String bodyText = " Bus no " + short_name + " and Bus route is " + long_name;
                String mailto = "mailto:appspundit2014@gmail.com" +
                        "?cc=" + "" +
                        "&subject=" + Uri.encode(subject) +
                        "&body=" + Uri.encode(bodyText);
                Intent EmailIntent = new Intent(Intent.ACTION_SENDTO);
                Uri data = Uri.parse(mailto);
                EmailIntent.setData(data);
                try {
                    startActivity(EmailIntent);
                } catch (ActivityNotFoundException e) {
                    //TODO: Handle case where no email app is available
                }
            }
        });
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd != null) {
            mInterstitialAd.show(routeDetailsKolkataBusClass.this);
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(routeDetailsKolkataBusClass.this);
                }
                this.finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}