package com.example.kolkatametroapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.stationListAdapterClass;
import com.example.kolkatametroapp.database.DataAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Locale;

public class stationList extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    Toolbar toolbar;
    RecyclerView recyclerView;
    EditText search_edit_text;
    stationListAdapterClass adapter;
    DataAdapter dbAdapter;
    SharedPreferences preferences;
    String lang;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }
        setContentView(R.layout.station_list_layout);

        toolbar = findViewById(R.id.toolbar_station);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        recyclerView = findViewById(R.id.recycler_view_station);
        search_edit_text = findViewById(R.id.search_frame);

        dbAdapter = new DataAdapter(stationList.this);
        dbAdapter.createDatabase();
        dbAdapter.open();
        ArrayList<ArrayList<String>> list = dbAdapter.getdatastationList();
//        System.out.println("actual list is :::::::::::: " + list.size());

        dbAdapter.close();

        adapter = new stationListAdapterClass(list, lang, stationList.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(stationList.this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);

        // filter the content on text change on edit text
        search_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.filter(charSequence);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        //for clicking drawable in the search edit text
        search_edit_text.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (search_edit_text.getRight() - search_edit_text.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        search_edit_text.setText("");

                        return true;
                    }
                }
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item1, menu);

        MenuItem routeMapMenu = menu.findItem(R.id.route_map_icon);
        ImageView iv = new ImageView(this);
        iv.setBackground(getResources().getDrawable(R.drawable.location_drawable_white));
        Animation animation = new AlphaAnimation((float) 2, 0); // Change alpha from fully visible to invisible
        animation.setDuration(500); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation
        iv.startAnimation(animation);
        routeMapMenu.setActionView(iv);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(stationList.this, routeMapWebview.class);
                startActivity(intent);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}
