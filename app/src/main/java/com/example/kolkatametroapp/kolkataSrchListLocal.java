package com.example.kolkatametroapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.stationListLocalAdapterClass;
import com.example.kolkatametroapp.classes.RecyclerItemClickListener;
import com.example.kolkatametroapp.database.DataAdapterTrains;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Locale;

public class kolkataSrchListLocal extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    Toolbar toolbar;
    RecyclerView recyclerView;
    stationListLocalAdapterClass adapter;
    ImageView back;
    ProgressBar progressBar;
    EditText search_txt;
    DataAdapterTrains dbAdapter;
    SharedPreferences preferences;
    String lang;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        lang = preferences.getString("langauge", "");
        if (lang.equals("")) {
            setLocale("en");
        } else {
            setLocale(lang);
        }
        setContentView(R.layout.kolkata_srch_list_local);

        toolbar = findViewById(R.id.search_local_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        AdView bannerView = findViewById(R.id.adView);
        bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerView.loadAd(adRequest);

        recyclerView = findViewById(R.id.recycler_view_search_local);
        progressBar = findViewById(R.id.progressBar_search_local);
        search_txt = findViewById(R.id.search_txt_kolkata_local);
        back = findViewById(R.id.toolbar_img_back_search_local);

        dbAdapter = new DataAdapterTrains(kolkataSrchListLocal.this);
        dbAdapter.createDatabase();
        dbAdapter.open();

        ArrayList<ArrayList<String>> list = dbAdapter.GetDataStationList();
//        System.out.println("XxX:> GetDataStationList :::::::::::: " + list.toString());

        dbAdapter.close();

        adapter = new stationListLocalAdapterClass(list, lang, kolkataSrchListLocal.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(kolkataSrchListLocal.this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(kolkataSrchListLocal.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent returnIntent = new Intent();
//                if (lang.equals("en")) {
//                    returnIntent.putExtra("result", list.get(position).get(1));
//                } else if (lang.equals("bn")) {
//                    returnIntent.putExtra("result", list.get(position).get(3));
//                } else {
//                    returnIntent.putExtra("result", list.get(position).get(1));
//                }
                returnIntent.putExtra("result", list.get(position).get(3));
                returnIntent.putExtra("station_code", list.get(position).get(2));
                returnIntent.putExtra("stationId", list.get(position).get(0));
                returnIntent.putExtra("domainId", list.get(position).get(1));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        }));


        search_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        search_txt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (search_txt.getRight() - search_txt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        search_txt.setText("");

                        return true;
                    }
                }
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void setLocale(String lang) { //call this in onCreate()
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}



