package com.example.kolkatametroapp;

import android.content.Context;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;


class CrashesHandler {

    public void logging_to_firebase(Context c,Exception e,String des ,String activity_data,String station_a ,String station_b) {

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(c);

        Bundle bundle = new Bundle();
        bundle.putString(Constants.Firebase_type, e.getStackTrace()[0].getClassName());
        bundle.putString(Constants.Firebase_location,activity_data+" line:"+e.getStackTrace()[0].getLineNumber() +" class name :"+e.getStackTrace()[0].getClassName()+" Cause"+e.getLocalizedMessage() );
        bundle.putString(Constants.Firebase_error, e.getMessage());
        bundle.putString(Constants.Firebase_description,des);
        bundle.putString(Constants.Firebase_station_a,station_a);
        bundle.putString(Constants.Firebase_station_b,station_b);
        mFirebaseAnalytics.logEvent(Constants.Firebase_device_error, bundle);


    }
}


