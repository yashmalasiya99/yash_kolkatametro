package com.example.kolkatametroapp;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kolkatametroapp.adapters.searchedListLocalRecyclerAdapterClass;
import com.example.kolkatametroapp.database.DataAdapterTrains;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class searchedKolkataLocalList extends AppCompatActivity {
    public static final String MyPREFERENCES = "LangaugePref";
    public static final int ITEMS_PER_AD = 5;
    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView src_display_txtview, dst_display_txtview;
    ImageView img_back;
    String src_from_main, dst_from_main, src_id_from_main, dst_id_from_main, src_code_from_main, dst_code_from_main, type;
    SharedPreferences preferences;
    int src_id, dst_id;
    ArrayList<ArrayList<String>> searched_array_list_after_converting_to_twelve_hr;
    searchedListLocalRecyclerAdapterClass Adapter;
    String lang;
    String time_from, time_to;
    DataAdapterTrains dbAdapter;
    LinearLayout no_trains_available;
    TextView today_text;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {


            preferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
            lang = preferences.getString("langauge", "");
            if (lang.equals("")) {
                setLocale("en");
            } else {
                setLocale(lang);
            }

            setContentView(R.layout.searched_kolkata_local_layout);

            toolbar = findViewById(R.id.toolbar_searched_list_local);
            setSupportActionBar(toolbar);

            AdView bannerView = findViewById(R.id.adView);
            bannerView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            AdRequest adRequest = new AdRequest.Builder().build();
            bannerView.loadAd(adRequest);

            src_from_main = getIntent().getStringExtra("src");
            src_id_from_main = getIntent().getStringExtra("srcid");
            src_code_from_main = getIntent().getStringExtra("src_code");
            dst_from_main = getIntent().getStringExtra("dst");
            dst_id_from_main = getIntent().getStringExtra("dstid");
            dst_code_from_main = getIntent().getStringExtra("dst_code");
            type = getIntent().getStringExtra("type");

            src_display_txtview = findViewById(R.id.searched_src_display_searched_list_local);
            dst_display_txtview = findViewById(R.id.searched_dst_display_searched_list_local);
            img_back = findViewById(R.id.img_back_searched_list_local);
            recyclerView = findViewById(R.id.recycler_view_searched_list_local);
            no_trains_available = findViewById(R.id.no_trains_available_layout_searched_list_local);

            src_display_txtview.setText(src_from_main);
            dst_display_txtview.setText(dst_from_main);
            today_text = findViewById(R.id.today_text);

            searched_array_list_after_converting_to_twelve_hr = new ArrayList<>();

            src_id = Integer.valueOf(src_id_from_main);
            dst_id = Integer.valueOf(dst_id_from_main);
            today_text.setText(getResources().getString(R.string.all_dates_dialog_picker));

            today_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(searchedKolkataLocalList.this);
                    alertDialog = builder.create();
                    View view_dailog = getLayoutInflater().inflate(R.layout.dialog_day_picker, null);

                    TextView textView_today = view_dailog.findViewById(R.id.dailog_text_day_picker_today);
                    TextView textView_all = view_dailog.findViewById(R.id.dailog_text_day_picker_all_dates);
                    TextView textView_tomorow = view_dailog.findViewById(R.id.dailog_text_day_picker_tommorow);
                    TextView textView_cancel = view_dailog.findViewById(R.id.text_dialog_cancel);
                    TextView choose_from_calendra = view_dailog.findViewById(R.id.dailog_text_day_picker_choose_from_calender);
                    textView_today.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Date c = Calendar.getInstance().getTime();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("E");
                            SimpleDateFormat dateFormat_1 = new SimpleDateFormat("dd-MMM");
                            String current_day = dateFormat.format(c);
                            String show_date = dateFormat_1.format(c);

                            today_text.setText(show_date + "-" + getResources().getString(R.string.today_dialog_picker));
                            filter_by_day(current_day);


                        }
                    });
                    textView_tomorow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.add(Calendar.DAY_OF_MONTH, 1);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("E");
                            String current_day = dateFormat.format(calendar.getTime());


                            SimpleDateFormat dateFormat_1 = new SimpleDateFormat("dd-MMM");

                            String show_date = dateFormat_1.format(calendar.getTime());

                            filter_by_day(current_day);
                            today_text.setText(show_date + "-" + getResources().getString(R.string.tommorow_dialog_picker));

//                        Calendar cal1 = Calendar.getInstance();
//                        cal1.add(Calendar.DATE, index_week);
//                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
//                        date_first = simpleDateFormat.format(cal1.getTime());

                        }
                    });
                    textView_all.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            today_text.setText(getResources().getString(R.string.all_dates_dialog_picker));
                            filter_by_day();


                        }
                    });

                    try {
                        choose_from_calendra.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();
                                Calendar c = Calendar.getInstance();
                                int mYear = c.get(Calendar.YEAR);
                                int mMonth = c.get(Calendar.MONTH);
                                int mDay = c.get(Calendar.DAY_OF_MONTH);
                                DatePickerDialog dialog = new DatePickerDialog(searchedKolkataLocalList.this,R.style.datepicker,
                                        new DatePickerDialog.OnDateSetListener() {
                                            @Override
                                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                                SimpleDateFormat simpledateformat = new SimpleDateFormat("E");
                                                Date date = new Date(i,i1, i2);
                                                String dayOfWeek = simpledateformat.format(date);
                                                SimpleDateFormat dateFormat_1 = new SimpleDateFormat("dd-MMM");
                                                String show_date = dateFormat_1.format(date);
                                                filter_by_day(dayOfWeek);
                                                today_text.setText(show_date);

                                            }
                                        }, mYear, mMonth, mDay);
                                dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                                dialog.show();
                                dialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.purple_500));
                                dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.purple_500));

                            }
                        });

                    }
                    catch (Exception e){
                        Log.d("errpr",e.getMessage());
                    }
                    textView_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog.setView(view_dailog);
                    alertDialog.show();
                }
            });

//        System.out.println("XxX:> src =x- " + src_from_main);
//        System.out.println("XxX:> src CODE =x- " + src_code_from_main);
//        System.out.println("XxX:> dst code =x- " + dst_code_from_main);
//        System.out.println("XxX:> srcid =x- " + src_id_from_main);
//        System.out.println("XxX:> dst =x- " + dst_from_main);
//        System.out.println("XxX:> dstid =x- " + dst_id_from_main);

            dbAdapter = new DataAdapterTrains(searchedKolkataLocalList.this);
            dbAdapter.createDatabase();
            dbAdapter.open();

            ArrayList<ArrayList<String>> list = dbAdapter.GetTrainListBetweenStations(src_id, dst_id, type);
//        System.out.println("XxX:> list :::::::::::: " + list.size());

            dbAdapter.close();

            for (int j = 0; j < list.size(); j++) {
                try {
                    SimpleDateFormat sdf_frm = new SimpleDateFormat("HH:mm");
                    Date dateObj_frm = sdf_frm.parse(list.get(j).get(1));
                    time_from = new SimpleDateFormat("hh:mm aa").format(dateObj_frm);

                    SimpleDateFormat sdf_to = new SimpleDateFormat("HH:mm");
                    Date dateObj_to = sdf_to.parse(list.get(j).get(6));
                    time_to = new SimpleDateFormat("hh:mm aa").format(dateObj_to);

                    ArrayList<String> twelve_hr_frmt_time_array = new ArrayList<>();
                    twelve_hr_frmt_time_array.add(list.get(j).get(0));
                    twelve_hr_frmt_time_array.add(time_from);
                    twelve_hr_frmt_time_array.add(list.get(j).get(2));
                    twelve_hr_frmt_time_array.add(list.get(j).get(3));
                    twelve_hr_frmt_time_array.add(list.get(j).get(4));
                    twelve_hr_frmt_time_array.add(list.get(j).get(5));
                    twelve_hr_frmt_time_array.add(time_to);
                    twelve_hr_frmt_time_array.add(list.get(j).get(7));
                    twelve_hr_frmt_time_array.add(list.get(j).get(8));
                    twelve_hr_frmt_time_array.add(list.get(j).get(9));
                    twelve_hr_frmt_time_array.add(list.get(j).get(10));

                    searched_array_list_after_converting_to_twelve_hr.add(twelve_hr_frmt_time_array);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            for (int i = ITEMS_PER_AD; i <= searched_array_list_after_converting_to_twelve_hr.size(); i += ITEMS_PER_AD) {
                ArrayList<String> twelve_hr_frmt_time_array_for_ads = new ArrayList<>();
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);

                searched_array_list_after_converting_to_twelve_hr.add(i, twelve_hr_frmt_time_array_for_ads);
            }

//        System.out.println(" actual size after ads is :::::::::::: " + searched_array_list_after_converting_to_twelve_hr.toString());

            if (searched_array_list_after_converting_to_twelve_hr.size() == 0) {
                no_trains_available.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {


                recyclerView.setVisibility(View.VISIBLE);
                no_trains_available.setVisibility(View.GONE);
                Adapter = new searchedListLocalRecyclerAdapterClass(
                        searched_array_list_after_converting_to_twelve_hr,
                        lang,
                        src_from_main,
                        dst_from_main, searchedKolkataLocalList.this);
                recyclerView.setHasFixedSize(true);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(searchedKolkataLocalList.this);
                recyclerView.setLayoutManager(layoutManager);

                recyclerView.setAdapter(Adapter);
                Log.d("calling",getIntent().getStringExtra("time")+getIntent().getStringExtra("deparv"));

                if (getIntent().getStringExtra("deparv") != null||getIntent().getStringExtra("time") != null) {
                    auto_index_set(getIntent().getStringExtra("deparv"), getIntent().getStringExtra("time"));
                } else {





                    Log.d("calling","else");
                    auto_index_set("", "");
                }

            }

            img_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        } catch (Exception e) {
            Log.d("error", e.getMessage());
        }
    }

    private void filter_by_day() {
        try {


            recyclerView.setVisibility(View.VISIBLE);
            no_trains_available.setVisibility(View.GONE);
            Adapter = new searchedListLocalRecyclerAdapterClass(
                    searched_array_list_after_converting_to_twelve_hr,
                    lang,
                    src_from_main,
                    dst_from_main, searchedKolkataLocalList.this);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(searchedKolkataLocalList.this);
            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setAdapter(Adapter);
            alertDialog.dismiss();
            if (searched_array_list_after_converting_to_twelve_hr.size() == 0) {
                no_trains_available.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.d("error", e.getMessage());

        }

    }

    private void filter_by_day(String current_day) {
        try {


            Log.d("current_dat", current_day);

            ArrayList<ArrayList<String>> filtred_ArrayList = new ArrayList<>();

            for (int i = 0; i < searched_array_list_after_converting_to_twelve_hr.size(); i++) {

                if (searched_array_list_after_converting_to_twelve_hr.get(i).get(9) != null) {
                    String running_days = searched_array_list_after_converting_to_twelve_hr.get(i).get(9);

//            System.out.println(position + " xXxX: running_days: " + running_days);

                    if (running_days == null || running_days.trim().isEmpty()) {

                    } else {
                        String rdb = Integer.toBinaryString(Integer.parseInt(running_days));
                        if (rdb.trim().length() < 7) {
                            int remaining = 7 - rdb.trim().length();

                            String new_rdb = "";

                            for (int p = 0; p < remaining; i++) {
                                new_rdb = new_rdb + "0";
                            }

                            rdb = new_rdb + rdb;
                        }
                        String[] runnindDays = rdb.split("");

                        if (runnindDays[0].equals("1")) {
                            if (current_day.equals("Mon")) {
                                filtred_ArrayList.add(searched_array_list_after_converting_to_twelve_hr.get(i));
                            }
                            // running mon
                        }
                        if (runnindDays[1].equals("1")) {
                            if (current_day.equals("Tue")) {
                                filtred_ArrayList.add(searched_array_list_after_converting_to_twelve_hr.get(i));
                            }
                        }
                        if (runnindDays[2].equals("1")) {
                            if (current_day.equals("Wed")) {
                                filtred_ArrayList.add(searched_array_list_after_converting_to_twelve_hr.get(i));
                            }
                        }
                        if (runnindDays[3].equals("1")) {
                            if (current_day.equals("Thu")) {
                                filtred_ArrayList.add(searched_array_list_after_converting_to_twelve_hr.get(i));
                            }
                        }
                        if (runnindDays[4].equals("1")) {
                            if (current_day.equals("Fri")) {
                                filtred_ArrayList.add(searched_array_list_after_converting_to_twelve_hr.get(i));
                            }
                        }
                        if (runnindDays[5].equals("1")) {
                            if (current_day.equals("Sat")) {
                                filtred_ArrayList.add(searched_array_list_after_converting_to_twelve_hr.get(i));
                            }
                        }
                        if (runnindDays[6].equals("1")) {
                            if (current_day.equals("Sun")) {
                                filtred_ArrayList.add(searched_array_list_after_converting_to_twelve_hr.get(i));
                            }
                        }

                    }


                }


            }

            for (int i = ITEMS_PER_AD; i <= filtred_ArrayList.size(); i += ITEMS_PER_AD) {
                ArrayList<String> twelve_hr_frmt_time_array_for_ads = new ArrayList<>();
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);
                twelve_hr_frmt_time_array_for_ads.add(null);

                filtred_ArrayList.add(i, twelve_hr_frmt_time_array_for_ads);
            }

            recyclerView.setVisibility(View.VISIBLE);
            no_trains_available.setVisibility(View.GONE);
            Adapter = new searchedListLocalRecyclerAdapterClass(
                    filtred_ArrayList,
                    lang,
                    src_from_main,
                    dst_from_main, searchedKolkataLocalList.this);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(searchedKolkataLocalList.this);
            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setAdapter(Adapter);
            alertDialog.dismiss();

            if (filtred_ArrayList.size() == 0) {
                no_trains_available.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.d("error", e.getMessage());
        }


    }

    private void auto_index_set(String deparv, String time) {
        try {


            if (deparv.equals("")&&time.equals("")) {
                int j = 0;
                int m = 0;

                DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
                Date date = null;

                try {


                    date = Calendar.getInstance().getTime();
                    String flag = outputformat.format(date);
                    date = outputformat.parse(flag);



                    Log.d("size", String.valueOf(searched_array_list_after_converting_to_twelve_hr.size()));
                    for (int i = 0; i < searched_array_list_after_converting_to_twelve_hr.size(); i++) {

                        if (searched_array_list_after_converting_to_twelve_hr.get(i).get(1) != null) {
                            Date datey = outputformat.parse(searched_array_list_after_converting_to_twelve_hr.get(i).get(1));
                            Log.d("time", searched_array_list_after_converting_to_twelve_hr.get(i).get(1) + time);
                            if (!datey.before(date)) {
                                Log.d("break", "break");
                                if (j == 0) {
                                    m = i;
                                    j = 1;
                                }


                            }

                        }

                    }


                    recyclerView.scrollToPosition(m);
                    Log.d("position", String.valueOf(m));


                } catch (ParseException e) {
                    Log.d("error 69", e.getMessage());
                }


            }

            if (deparv.equals("1")) {
                int j = 0;
                int m = 0;
                DateFormat df = new SimpleDateFormat("HH:mm");
                DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
                Date date = null;

                try {
                    date = outputformat.parse(time);

                    Log.d("size", String.valueOf(searched_array_list_after_converting_to_twelve_hr.size()));
                    for (int i = 0; i < searched_array_list_after_converting_to_twelve_hr.size(); i++) {

                        if (searched_array_list_after_converting_to_twelve_hr.get(i).get(1) != null) {
                            Date datey = outputformat.parse(searched_array_list_after_converting_to_twelve_hr.get(i).get(1));
                            Log.d("time", searched_array_list_after_converting_to_twelve_hr.get(i).get(1) + time);
                            if (!datey.before(date)) {
                                Log.d("break", "break");
                                if (j == 0) {
                                    m = i;
                                    j = 1;
                                }


                            }

                        }

                    }


                    recyclerView.scrollToPosition(m);
                    Log.d("position", String.valueOf(m));


                } catch (ParseException e) {
                    Log.d("error 69", e.getMessage());
                }


            }
//        if (deparv.equals("2")) {
//            int j = 0;
//            int m = 0;
//            DateFormat df = new SimpleDateFormat("HH:mm");
//            DateFormat outputformat = new SimpleDateFormat("hh:mm aa");
//            Date date = null;
//
//            try {
//                date = outputformat.parse(time);
//
//                Log.d("size", String.valueOf(searched_array_list_after_converting_to_twelve_hr.size()));
//                for (int i = 0; i < searched_array_list_after_converting_to_twelve_hr.size(); i++) {
//
//                    if (searched_array_list_after_converting_to_twelve_hr.get(i).get(6) != null) {
//                        Date datey = outputformat.parse(searched_array_list_after_converting_to_twelve_hr.get(i).get(6));
//                        Log.d("time", searched_array_list_after_converting_to_twelve_hr.get(i).get(6) + time);
//                        if (!date.before(datey)) {
//                            Log.d("break", "break");
//                            if (j == 0) {
//                                m = i;
//                                j = 1;
//                            }
//
//
//                        }
//
//                    }
//
//                }
//
//
//                recyclerView.scrollToPosition(m);
//                Log.d("position", String.valueOf(m));
//
//
//            } catch (ParseException e) {
//                Log.d("error 69", e.getMessage());
//            }
//
//
//        }
        } catch (Exception e) {
            Log.d("error", e.getMessage());
        }

    }

    public void setLocale(String lang) { //call this in onCreate()
        try {


            Locale myLocale = new Locale(lang);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        } catch (Exception e) {
            Log.d("error", e.getMessage());
        }
    }
}
