-keepattributes Signature
-keepattributes *Annotation*
-keepattributes InnerClasses,EnclosingMethod

# Basic ProGuard rules for Firebase Android SDK 2.0.0+
-keep class com.google.firebase.** { *; }
-keep class com.google.android.gms.ads.** { *; }
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}




-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-keepnames class com.google.android.play.core.review.ReviewInfo
-keepnames class com.daasuu.bl.** { *; }

-keepnames class com.google.android.gms.ads.** { *; }
-keepnames class com.google.firebase.** { *; }

-dontwarn okio.**